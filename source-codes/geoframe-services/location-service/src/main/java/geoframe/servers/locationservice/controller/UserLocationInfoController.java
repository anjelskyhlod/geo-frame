package geoframe.servers.locationservice.controller;

import geoframe.servers.locationservice.connection.Event;
import geoframe.servers.locationservice.connection.EventResponse;
import geoframe.servers.locationservice.connection.EventType;
import geoframe.servers.locationservice.connection.JsonHelper;
import geoframe.servers.locationservice.kafka.SimpleProducer;
import geoframe.servers.locationservice.model.GameObject;
import geoframe.servers.locationservice.model.UserLocationInfo;
import geoframe.servers.locationservice.model.UserLocationKey;
import geoframe.servers.locationservice.security.TokenHelper;
import geoframe.servers.locationservice.service.UserLocationinfoService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * @author Lucie Kucharova
 * Handles events that ocuure in context of
 * loading player's game objects in mobile app
 */

@Component
public class UserLocationInfoController {

    @Autowired
    UserLocationinfoService userLocationinfoService;

    @Autowired
    SimpleProducer producer;

    @Autowired
    JsonHelper jsonHelper;

    @Autowired
    TokenHelper tokenHelper;

    /**
     * Checks if player is connecting for the first time,
     * eventually clears not needed previous data, saves player
     * object as current info about his game and location
     * and notifies game object service about objects needed.
     *
     * @param event
     */
    public void setUserPosition(Event event) {
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");
        data.put("userId", event.getUserId());

        UserLocationInfo userLocationInfo = new UserLocationInfo(data);

        UserLocationInfo existing = userLocationinfoService.findById(userLocationInfo.getId());
        if (userLocationInfo.isFirstTimeChecking() && existing != null) {
            existing.setObjectIds(new ArrayList<>());
            userLocationinfoService.update(existing);
        }
        if (existing == null) {
            userLocationinfoService.saveNewUserLocationInfo(userLocationInfo);
        } else {
            // not needed now, but if it is necessary to update user position, here.
        }

        JSONObject dataToService = userLocationInfo.toJson();
        if (existing != null) dataToService.put("existing", existing.getObjectIds());
        JSONObject msg = new JSONObject();
        msg.put("data", dataToService);
        msg.put("eventType", "GET_OBJECTS_BY_LOCATION");
        producer.sendToService("game_object_service", msg);
    }

    /**
     * Filters and stores game objects and send to user
     * those wich are not loaded for him yet.
     *
     * @param event
     * @return Game objects which was not loaded for
     * user yet
     */
    public EventResponse storeObjectsAndSendToUser(Event event) {
        EventResponse response = createEventResponse(EventType.SEND_OBJECTS_BY_LOCATION);
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");

        Long gameId = jsonHelper.getLong(data, "gameId");
        String userId = jsonHelper.getString(data, "userId");

        UserLocationInfo info = userLocationinfoService.findById(new UserLocationKey(userId, gameId));
        if (info == null) return null;

        JSONArray gameIdsJsonArr = (JSONArray) data.get("gameObjects");
        List<String> gameIds = createFromJSONArray(gameIdsJsonArr);
        List<String> alreadyStoredIds = info.getObjectIds();
        for (ListIterator<String> iter = gameIds.listIterator(); iter.hasNext(); ) {
            String element = iter.next();
            if (alreadyStoredIds.contains(element)) {
                gameIdsJsonArr.remove(element);
            } else {
                info.getObjectIds().add(element);
            }
        }
        userLocationinfoService.update(info);
        response.addJsonArrayData("gameObjects", gameIdsJsonArr);
        response.setTopic(userId);
        return response;
    }

    /**
     * Deletes stored data for specified player
     *
     * @param event
     */
    public void deleteUserData(Event event) {
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");
        String username = tokenHelper
                .getUsernameFromToken(event.getSecurityContext().getAuthentication().getToken());
        Long gameId = Long.parseLong(data.get("gameId").toString());

        userLocationinfoService.deleteById(new UserLocationKey(username, gameId));
    }

    /**
     * Checks if newly set object is in visible
     * area of any of the players. If it is so,
     * sends the object to player
     *
     * @param event
     * @return If objects is in visible range,
     * returns object
     */
    public EventResponse saveUpdateCheck(Event event) {
        EventResponse response = null;
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");
        JSONObject jGameObject = jsonHelper.getJsonObject(data, "gameObject");

        GameObject gameObject = new GameObject(jGameObject);
        List<UserLocationInfo> infos = userLocationinfoService.fingAllByGameId(gameObject.getGameId());
        for (UserLocationInfo info : infos) {
            if (distFrom(info.getLattitude(), info.getLongitude(), gameObject.getLocation().getLattitude(),
                    gameObject.getLocation().getLongitude()) < info.getVisibleRange()) {
                response = createEventResponse(EventType.SAVED_GAME_OBJECT);
                response.setTopic(info.getId().getUserId());
                response.addData("gameObject", gameObject.toJson());
                info.getObjectIds().add(gameObject.getId().toString());
                userLocationinfoService.update(info);
                producer.sendResponseMessage(response.getTopic(), response.toJson());
            }
        }
        return null;
    }

    /**
     * Checks if updated object is in loaded
     * objects of any of the players. If it is so,
     * sends the object to player
     *
     * @param event
     * @return If objects is in visible range,
     * returns object
     */
    public EventResponse updateUpdateCheck(Event event) {
        EventResponse response = null;
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");
        JSONObject jGameObject = jsonHelper.getJsonObject(data, "gameObject");

        GameObject gameObject = new GameObject(jGameObject);
        List<UserLocationInfo> infos = userLocationinfoService.fingAllByGameId(gameObject.getGameId());
        for (UserLocationInfo info : infos) {
            if(info.getObjectIds().contains(gameObject.getId().toString())) {
                response = createEventResponse(EventType.UPDATED_GAME_OBJECT);
                response.setTopic(info.getId().getUserId());
                response.addData("gameObject", gameObject.toJson());
                info.getObjectIds().add(gameObject.getId().toString());
                producer.sendResponseMessage(response.getTopic(), response.toJson());
            }
        }
        return null;
    }

    /**
     * Checks if any of the player has loaded
     * given object. If player has stored this object,
     * sends him id to erase it
     * @param event
     * @return
     */
    public EventResponse deleteObject(Event event) {
        EventResponse response = null;
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");
        Long gameObjectId = jsonHelper.getLong(data, "gameObjectId");
        Long gameId = jsonHelper.getLong(data, "gameId");

        List<UserLocationInfo> infos = userLocationinfoService.fingAllByGameId(gameId);
        for (UserLocationInfo info : infos) {
            if(info.getObjectIds().contains(gameObjectId.toString())) {
                response = createEventResponse(EventType.DELETED_GAME_OBJECT);
                response.setTopic(info.getId().getUserId());
                response.addData("gameObjectId", gameObjectId);
                info.getObjectIds().add(gameObjectId.toString());
                producer.sendResponseMessage(response.getTopic(), response.toJson());
            }
        }
        return null;
    }


//    ############################
//    ###### Helper methods ######
//    ############################

    private EventResponse createEventResponse(EventType eventType) {
        EventResponse response = new EventResponse();
        response.setEventType(eventType);
        return response;
    }

    private List<String> createFromJSONArray(JSONArray jsonArray) {
        List<String> stringList = new ArrayList<>();
        Iterator<Object> iterator = jsonArray.iterator();
        while (iterator.hasNext()) {
            JSONObject obj = null;
            try {
                obj = (JSONObject) (iterator.next());
            } catch (Exception e) {
                e.printStackTrace();
            }
            stringList.add(obj.get("id").toString());
        }
        return stringList;
    }


    /**
     * Calculates distance between two GPS points
     *
     * @param lat1
     * @param lng1
     * @param lat2
     * @param lng2
     * @return Distance in meters
     */
    private static double distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 3958.75; // miles (or 6371.0 kilometers)
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;
        return dist * 1000;
    }
}
