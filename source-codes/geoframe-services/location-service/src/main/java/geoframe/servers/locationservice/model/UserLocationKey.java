package geoframe.servers.locationservice.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author Lucie Kucharova
 * Composed key for @{@link UserLocationInfo}
 */

@Embeddable
public class UserLocationKey implements Serializable {
    @Column(name = "userId")
    private String userId;
    @Column(name = "gameId")
    private Long gameId;

    public UserLocationKey(String userId, Long gameId) {
        this.userId = userId;
        this.gameId = gameId;
    }

    public UserLocationKey() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }
}
