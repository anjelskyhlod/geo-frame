package geoframe.servers.locationservice;

import geoframe.servers.locationservice.kafka.SimpleConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author Lucie Kucharova
 * Main application class. This application handles position from mobile
 * application and tries to keep it's data usage to minimum.
 */

@SpringBootApplication
public class Application {

	@Autowired
	ApplicationContext applicationContext;

	@Bean
	public TaskExecutor taskExecutor() {
		return new SimpleAsyncTaskExecutor();
	}

	@Bean
	public CommandLineRunner schedulingRunner(TaskExecutor executor) {
		return new CommandLineRunner() {
			public void run(String... args) throws Exception {
				SimpleConsumer consumer = applicationContext.getBean(SimpleConsumer.class);
				executor.execute(consumer);
			}
		};
	}

	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}


