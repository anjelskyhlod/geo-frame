package geoframe.servers.locationservice.connection;

import geoframe.servers.locationservice.security.auth.JwtSecurityContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author Lucie Kucharova
 * Utility used for parsing incoming message from broker to it's object representation (Event)
 */

public class EventBuilder {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    private JsonHelper jsonHelper;

    public EventBuilder() {
        jsonHelper = new JsonHelper();
    }

    public Event createEvent(String key, String value, JwtSecurityContext ctx) {
        Event event = null;
        JSONObject jsonValue = jsonHelper.createJsonFromString(value);

        EventType type = createEventType(jsonValue.get("eventType").toString());
        if (type != null && key != null) {
            event = new Event(key, type, jsonValue, ctx);
        } else {
            LOGGER.error("CreateEvent failed");
        }
        return event;
    }

    private EventType createEventType(String value) {
        EventType eventType = null;
        for (EventType type : EventType.values()) {
            if (type.name().equals(value)) {
                eventType = type;
            }
        }
        return eventType;
    }
}
