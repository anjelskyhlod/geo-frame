package geoframe.servers.locationservice.repository;

import geoframe.servers.locationservice.model.UserLocationInfo;
import geoframe.servers.locationservice.model.UserLocationKey;
import geoframe.servers.locationservice.service.UserLocationinfoService;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Lucie Kucharova
 */

@Service
public class UserLocationInfoRepositoryImpl implements UserLocationInfoRepositoryCustom {
    @Override
    public void clearAllObjects(UserLocationKey key) {
        // not needed
    }

}
