package geoframe.servers.locationservice.repository;

import geoframe.servers.locationservice.model.UserLocationInfo;
import geoframe.servers.locationservice.model.UserLocationKey;

import java.util.List;

/**
 * @author Lucie Kucharova
 */

public interface UserLocationInfoRepositoryCustom {
    void clearAllObjects(UserLocationKey key);
}
