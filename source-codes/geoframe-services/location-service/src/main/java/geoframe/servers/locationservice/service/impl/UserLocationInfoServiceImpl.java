package geoframe.servers.locationservice.service.impl;

import geoframe.servers.locationservice.model.UserLocationInfo;
import geoframe.servers.locationservice.model.UserLocationKey;
import geoframe.servers.locationservice.repository.UserLocationInfoRepository;
import geoframe.servers.locationservice.service.UserLocationinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lucie Kucharova
 */

@Service
public class UserLocationInfoServiceImpl implements UserLocationinfoService {
    @Autowired
    private UserLocationInfoRepository userLocationInfoRepository;

    @Override
    public UserLocationInfo findById(UserLocationKey id) {
        return userLocationInfoRepository.findById(id);
    }

    @Override
    public void saveNewUserLocationInfo(UserLocationInfo info) {
        userLocationInfoRepository.save(info);
    }

    @Override
    public void update(UserLocationInfo info) {
        userLocationInfoRepository.save(info);
    }

    @Override
    public void deleteById(UserLocationKey key) {
        userLocationInfoRepository.delete(key);
    }

    @Override
    public void clearAllObjects(UserLocationKey key) {
        userLocationInfoRepository.clearAllObjects(key);
    }

    @Override
    public List<UserLocationInfo> findAllByGameObjectId(Long id) {
        return userLocationInfoRepository.findAllByObjectIdsIsContaining(id);
    }

    @Override
    public List<UserLocationInfo> fingAllByGameId(Long id) {
        return userLocationInfoRepository.findAllByIdGameId(id);
    }
}
