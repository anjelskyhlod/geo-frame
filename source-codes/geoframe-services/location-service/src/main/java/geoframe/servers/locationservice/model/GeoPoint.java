package geoframe.servers.locationservice.model;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * @author Lucie Kucharova
 * Representation of place via GPS coordinates
 */

public class GeoPoint {

    private Long id;
    private Double lattitude;
    private Double longitude;

    public GeoPoint(Double lattitude, Double longitude) {
        this.lattitude = lattitude;
        this.longitude = longitude;
    }

    public GeoPoint() {
    }

    public GeoPoint(JSONObject location) {
        setAttributes(location);
    }

    public GeoPoint(String jsonStringData) {
        JSONParser parser = new JSONParser();
        JSONObject gameObj = null;
        try {
            gameObj = (JSONObject) parser.parse(jsonStringData);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        setAttributes(gameObj);
    }

    private void setAttributes(JSONObject data) {
        setLattitude(Double.parseDouble(data.get("lattitude").toString()));
        setLongitude(Double.parseDouble(data.get("longitude").toString()));
        if (data.get("id") != null) {
            setId(Long.parseLong(data.get("id").toString()));
        }
    }

    public Double getLattitude() {
        return lattitude;
    }

    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        obj.put("lattitude", Double.toString(this.lattitude));
        obj.put("longitude", Double.toString(this.longitude));
        obj.put("id", Long.toString(this.id));
        return obj;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GeoPoint other = (GeoPoint) obj;
        if (id != other.id)
            return false;
        return true;
    }
}
