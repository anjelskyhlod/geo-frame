package geoframe.servers.locationservice.connection;

/**
 * @author Lucie Kucharova
 * These are all types of events, which can be received or produced by
 * this service.
 */

public enum EventType {
    AUTH_TOKEN,
    IM_HERE,
    SEND_OBJECTS_BY_LOCATION,
    I_END,

    UPDATED_GAME_OBJECT,
    DELETED_GAME_OBJECT,
    SAVED_GAME_OBJECT
}
