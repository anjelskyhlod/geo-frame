package geoframe.servers.locationservice.model;

import org.json.simple.JSONObject;

import javax.persistence.*;
import java.util.List;

/**
 * @author Lucie Kucharova
 * Class representing user's game object state
 */

@Entity
@Table(name = "user_location_info")
public class UserLocationInfo {

    @EmbeddedId
    private UserLocationKey id;

    @Column(name = "lattitude")
    private Double lattitude;
    @Column(name = "longitude")
    private Double longitude;
    @Column(name = "objectIds")
    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> objectIds;
    @Column(name = "visibleRange")
    private Long visibleRange;
    @Transient
    private boolean firstTimeChecking;

    public UserLocationInfo() {
    }

    public UserLocationInfo(JSONObject json) {
        if (json.get("userId") == null || json.get("gameId") == null || json.get("lattitude") == null ||
                json.get("longitude") == null) {
            throw new IllegalArgumentException("Some key values are null");
        }
        this.id = new UserLocationKey(json.get("userId").toString(), Long.parseLong(json.get("gameId").toString()));

        this.lattitude = Double.parseDouble(json.get("lattitude").toString());
        this.longitude = Double.parseDouble(json.get("longitude").toString());
        if (json.get("visibleRange") != null) {
            this.visibleRange = Long.parseLong(json.get("visibleRange").toString());
        }
        if (json.get("firstTime") != null) {
            this.firstTimeChecking = Boolean.parseBoolean(json.get("firstTime").toString());
        }
    }

    public UserLocationKey getId() {
        return id;
    }

    public void setId(UserLocationKey id) {
        this.id = id;
    }

    public Double getLattitude() {
        return lattitude;
    }

    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public List<String> getObjectIds() {
        return objectIds;
    }

    public void setObjectIds(List<String> objectIds) {
        this.objectIds = objectIds;
    }

    public Long getVisibleRange() {
        return visibleRange;
    }

    public void setVisibleRange(Long visibleRange) {
        this.visibleRange = visibleRange;
    }

    public boolean isFirstTimeChecking() {
        return firstTimeChecking;
    }

    public void setFirstTimeChecking(boolean firstTimeChecking) {
        this.firstTimeChecking = firstTimeChecking;
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("lattitude", getLattitude());
        json.put("longitude", getLongitude());
        json.put("visibleRange", getVisibleRange());
        json.put("gameId", getId().getGameId());
        json.put("userId", getId().getUserId());
        return json;
    }
}
