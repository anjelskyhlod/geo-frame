package geoframe.servers.locationservice.service;

import geoframe.servers.locationservice.model.UserLocationInfo;
import geoframe.servers.locationservice.model.UserLocationKey;

import java.util.List;

/**
 * @author Lucie Kucharova
 */


public interface UserLocationinfoService {
    UserLocationInfo findById(UserLocationKey id);

    void saveNewUserLocationInfo(UserLocationInfo info);

    void update(UserLocationInfo info);

    void deleteById(UserLocationKey key);

    void clearAllObjects(UserLocationKey key);

    List<UserLocationInfo> findAllByGameObjectId(Long id);

    List<UserLocationInfo> fingAllByGameId(Long id);
}
