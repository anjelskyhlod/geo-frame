package geoframe.servers.locationservice.connection;

import geoframe.servers.locationservice.security.auth.JwtSecurityContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;

/**
 * @author Lucie Kucharova
 * Object representation of incoming message from broker
 */

public class Event {

    private String userId;
    private EventType type;
    private JSONObject value;
    private JwtSecurityContext securityContext;

    public Event(String userId, EventType type, JSONObject value, JwtSecurityContext ctx) {
        this.userId = userId;
        this.type = type;
        this.value = value;
        this.securityContext = ctx;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public JSONObject getValue() {
        return value;
    }

    public void setValue(JSONObject value) {
        this.value = value;
    }

    public JwtSecurityContext getSecurityContext() {
        return securityContext;
    }

    public void setSecurityContext(JwtSecurityContext securityContext) {
        this.securityContext = securityContext;
    }
}
