package geoframe.servers.locationservice.kafka;

import geoframe.servers.locationservice.connection.*;
import geoframe.servers.locationservice.security.auth.JwtSecurityContext;
import geoframe.servers.locationservice.security.auth.TokenAuthenticationFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * @author Lucie Kucharova
 * This class represents broker listener. Works in loop
 * and provides new messeges from Kafka to the rest of
 * service from topics specified in constructor
 */

@Component("simpleConsumer")
@Scope("prototype")
public class SimpleConsumer implements Runnable {

    private final Log LOGGER = LogFactory.getLog(this.getClass());

    @Autowired
    private EventHandler eventHandler;

    @Autowired
    private TokenAuthenticationFilter tokenAuthenticationFilter;

    KafkaConsumer<String, String> consumer;
    Event currentEvent;
    private EventBuilder eventBuilder;

    public SimpleConsumer(@Value("${kafka.url}") final String KAFKA_URL) {
        this.eventBuilder = new EventBuilder();

        List<String> topics = new ArrayList<>();

        topics.add(EventType.IM_HERE.toString());
        topics.add(EventType.I_END.toString());
        topics.add(EventType.SAVED_GAME_OBJECT.toString());
        topics.add(EventType.UPDATED_GAME_OBJECT.toString());
        topics.add(EventType.DELETED_GAME_OBJECT.toString());
        topics.add("location_service");

        Properties props = new Properties();

        props.put("bootstrap.servers", KAFKA_URL);
        props.put("group.id", "location");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "30000");
        props.put("key.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        consumer = new KafkaConsumer
                <String, String>(props);

        consumer.subscribe(topics);
        System.out.println("Subscribed to topic " + topics.toString());

    }

    public void run() {
        System.out.println("running");
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(100);
            for (ConsumerRecord<String, String> record : records) {
                // print the topic,key and value for the consumer records.
                LOGGER.info("Location service consumer: ");
                LOGGER.info(String.format("topic = %s, key = %s, value = %s\n",
                        record.topic(), record.key(), record.value()));

                if (record.key().endsWith("_service")) {
                    currentEvent = eventBuilder.createEvent(record.key(), record.value(), null);
                    eventHandler.process(currentEvent);
                } else {
                    JwtSecurityContext ctx = tokenAuthenticationFilter.filterAndCreateContext(record.value());
                    currentEvent = eventBuilder.createEvent(record.key(), record.value(), ctx);
                    eventHandler.process(currentEvent);
                }
            }
        }
    }
}

