package geoframe.servers.locationservice.connection;

import geoframe.servers.locationservice.controller.UserLocationInfoController;
import geoframe.servers.locationservice.kafka.SimpleProducer;
import geoframe.servers.locationservice.model.UserLocationInfo;
import geoframe.servers.locationservice.security.TokenHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Lucie Kucharova
 * Mechanism responsible for navigating incoming @{@link Event} from broker
 * to right controller, which knows how to handle it.
 */

@Component
public class EventHandler {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    @Autowired
    SimpleProducer eventSender;

    @Autowired
    UserLocationInfoController userLocationInfoController;

    @Autowired
    TokenHelper tokenHelper;

    public void process(Event event) {
        EventType eventType = event.getType();
        EventResponse response = null;
        switch (eventType) {
            case IM_HERE:
                userLocationInfoController.setUserPosition(event);
                break;
            case SEND_OBJECTS_BY_LOCATION:
                response = userLocationInfoController.storeObjectsAndSendToUser(event);
                break;
            case I_END:
                userLocationInfoController.deleteUserData(event);
                break;
            case AUTH_TOKEN:
//                authenticationController.setAuthToken(body);
//                return;
//                break;
            case SAVED_GAME_OBJECT:
                response = userLocationInfoController.saveUpdateCheck(event);
                break;
            case UPDATED_GAME_OBJECT:
                response = userLocationInfoController.updateUpdateCheck(event);
                break;
            case DELETED_GAME_OBJECT:
                response = userLocationInfoController.deleteObject(event);
                break;
            default:
                LOGGER.warn("Process event: Unknown event Type.");
        }
        if (response != null) {
                eventSender.sendResponseMessage(response.getTopic(), response.toJson());
        }
    }
}
