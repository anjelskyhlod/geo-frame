package geoframe.servers.locationservice.connection;

import org.json.simple.JSONObject;

/**
 * @author Lucie Kucharova
 * Interface for JSON-serializable objects
 */

public interface JsonSerializable {
    JSONObject toJson();
}
