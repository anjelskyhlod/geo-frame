package geoframe.servers.locationservice.repository;

import geoframe.servers.locationservice.model.UserLocationInfo;
import geoframe.servers.locationservice.model.UserLocationKey;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Lucie Kucharova
 */

public interface UserLocationInfoRepository extends JpaRepository<UserLocationInfo, UserLocationKey>, UserLocationInfoRepositoryCustom {
    UserLocationInfo findById(UserLocationKey key);
    List<UserLocationInfo> findAllByObjectIdsIsContaining(Long id);
    List<UserLocationInfo> findAllByIdGameId(Long id);
}

