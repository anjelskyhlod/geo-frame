package geoframe.servers.locationservice.model;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.List;

/**
 * @author Lucie Kucharova
 * Template class for all game ojects
 */

public class GameObject{

    private Long id;
    private Long gameId;
    private String name;
    private String description;
    private Double accessibleRadius;
    private GeoPoint location;
    private List<String> quests;
    private List<String> inventoryObjectIds;
    private Double value;
    private boolean canMove;
    private Double movementSpeed;
    private boolean hidden;


    public GameObject() {
    }

    public GameObject(JSONObject data) {
        setAttributes(data);
    }

    public GameObject(String jsonStringData) {
        JSONParser parser = new JSONParser();
        JSONObject gameObj = null;
        try {
            gameObj = (JSONObject) parser.parse(jsonStringData);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        setAttributes(gameObj);
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        if (id != null)
            obj.put("id", Long.toString(this.id));
        if (name != null)
            obj.put("name", this.name);
        if (description != null)
            obj.put("description", this.description);
        if (gameId != null)
            obj.put("gameId", this.gameId);
        if (location != null)
            obj.put("location", this.location.toJson());
        if (quests != null)
            obj.put("quests", this.quests);
        if (accessibleRadius != null)
            obj.put("accessibleRadius", this.accessibleRadius);
        if (inventoryObjectIds != null)
            obj.put("inventoryObjectIds", this.inventoryObjectIds);
        obj.put("canMove", this.canMove);
        if (value != null)
            obj.put("value", this.value);
        obj.put("hidden", this.hidden);
        return obj;
    }

    protected void setAttributes(JSONObject data) {
        if (data.get("id") != null) {
            setId(Long.parseLong(data.get("id").toString()));
        }
        if (data.get("description") != null)
            setDescription((String) data.get("description"));
        if (data.get("name") != null)
            setName((String) data.get("name"));
        if (data.get("gameId") != null)
            setGameId(Long.parseLong(data.get("gameId").toString()));
        if (data.get("location") != null)
            setLocation(new GeoPoint((JSONObject) data.get("location")));
        if (data.get("quests") != null)
            setQuests((List<String>) data.get("quests"));
        if (data.get("accessibleRadius") != null)
            setAccessibleRadius(Double.parseDouble(data.get("accessibleRadius").toString()));
        if (data.get("value") != null)
            setValue((Double) data.get("value"));
        if (data.get("canMove") != null)
            setCanMove((boolean) data.get("canMove"));
        if (data.get("inventoryObjectIds") != null)
            setInventory((List<String>) data.get("inventoryObjectIds"));
        if (data.get("hidden") != null)
            setHidden((boolean) data.get("hidden"));
    }

    public boolean getHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public boolean isCanMove() {
        return canMove;
    }

    public void setCanMove(boolean canMove) {
        this.canMove = canMove;
    }

    public Double getMovementSpeed() {
        return movementSpeed;
    }

    public void setMovementSpeed(Double movementSpeed) {
        this.movementSpeed = movementSpeed;
    }

    public Double getAccessibleRadius() {
        return accessibleRadius;
    }

    public List<String> getInventory() {
        return inventoryObjectIds;
    }

    public void setInventory(List<String> inventory) {
        this.inventoryObjectIds = inventory;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public List<String> getQuests() {
        return quests;
    }

    public void setQuests(List<String> quests) {
        this.quests = quests;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public Double isAreaRadius() {
        return accessibleRadius;
    }

    public void setAccessibleRadius(Double accessibleRadius) {
        this.accessibleRadius = accessibleRadius;
    }

    public List<String> getInventoryObjectIds() {
        return inventoryObjectIds;
    }

    public void setInventoryObjectIds(List<String> inventoryObjectIds) {
        this.inventoryObjectIds = inventoryObjectIds;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GameObject other = (GameObject) obj;
        if (id != other.id)
            return false;
        return true;
    }

    public GameObject copy() {
        GameObject obj = new GameObject();
        obj.setHidden(this.hidden);
        obj.setDescription(this.description);
        obj.setGameId(this.gameId);
        obj.setId(this.id);
        obj.setLocation(this.location);
        obj.setName(this.name);
        obj.setQuests(this.quests);
        obj.setAccessibleRadius(this.accessibleRadius);
        obj.setCanMove(this.canMove);
        obj.setInventoryObjectIds(this.inventoryObjectIds);
        obj.setValue(this.value);
        obj.setMovementSpeed(this.movementSpeed);
        return obj;
    }

    public void copyAttributes(GameObject obj) {
        this.hidden = obj.getHidden();
        this.description = obj.getDescription();
        this.gameId = obj.getGameId();
        this.id = obj.getId();
        this.location = obj.getLocation();
        this.name = obj.getName();
        this.quests = obj.getQuests();
        this.accessibleRadius = obj.getAccessibleRadius();
        this.canMove = obj.isCanMove();
        this.inventoryObjectIds = obj.getInventoryObjectIds();
        this.value = obj.getValue();
        this.movementSpeed = obj.getMovementSpeed();
    }
}
