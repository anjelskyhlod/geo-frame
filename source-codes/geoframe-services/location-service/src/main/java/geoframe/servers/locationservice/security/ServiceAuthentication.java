package geoframe.servers.locationservice.security;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Lucie Kucharova
 * Provides service authentication
 */

@Component
@Scope("singleton")
public class ServiceAuthentication {

    private String authToken;
    private final String SERVICE_NAME = "location_service";

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getSERVICE_NAME() {
        return SERVICE_NAME;
    }
}
