package geoframe.servers.riddleQuestService.service;

import geoframe.servers.riddleQuestService.world.Riddle;

import java.util.List;

/**
 * @author Lucie Kucharova
 */

public interface RiddleService extends BaseService<Riddle>{
    List<Riddle> findByIds(List<Long> idsList);
}
