package geoframe.servers.riddleQuestService.connection;

/**
 * @author Lucie Kucharova
 * These are all types of events, which can be received or produced by
 * this service.
 */

public enum EventType {
    GET_QUEST_TYPE,
    QUEST_TYPE_RIDDLE,
    SAVE_QUEST_TYPE_RIDDLE,
    SAVED_QUEST_TYPE_RIDDLE,
    GET_ALL_QUEST_TYPE_RIDDLE,
    ALL_QUEST_TYPE_RIDDLE,
    QUESTS_BY_IDS,
    QUEST_TYPE_RIDDLE_BY_IDS,
    CHECK_SOLVED,
    IS_SOLVED,
    GET_AVAILABLE_QUEST_TYPES,
    GET_QUESTS_BY_IDS,
    NEW_QUEST_ADDED
}
