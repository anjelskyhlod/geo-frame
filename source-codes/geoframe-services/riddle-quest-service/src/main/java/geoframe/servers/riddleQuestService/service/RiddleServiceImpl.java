package geoframe.servers.riddleQuestService.service;

import geoframe.servers.riddleQuestService.dao.RiddleDao;
import geoframe.servers.riddleQuestService.world.Riddle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lucie Kucharova
 */

@Service
public class RiddleServiceImpl implements RiddleService {

    @Autowired
    RiddleDao dao;

    @Override
    public Riddle findById(Long id) {
        return dao.findById(id);
    }

    @Override
    public List<Riddle> findAll() {
        return dao.findAll();
    }

    @Override
    public Riddle save(Riddle entity) {
        return dao.save(entity);
    }

    @Override
    public void update(Riddle entity) {
        dao.update(entity);
    }

    @Override
    public void delete(Riddle entity) {
        dao.delete(entity);
    }

    @Override
    public void deleteById(Long entityId) {
        dao.deleteById(entityId);
    }

    @Override
    public List<Riddle> findByIds(List<Long> idsList) {
        return dao.findByIds(idsList);
    }
}
