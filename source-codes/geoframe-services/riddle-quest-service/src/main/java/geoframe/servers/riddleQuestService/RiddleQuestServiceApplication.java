package geoframe.servers.riddleQuestService;

import geoframe.servers.riddleQuestService.kafka.SimpleConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

/**
 * @author Lucie Kucharova
 * Main application class. This application handles specific
 * type of quest - quest riddle.
 */

@SpringBootApplication
public class RiddleQuestServiceApplication {

    @Autowired
    ApplicationContext applicationContext;

    @Bean
    public TaskExecutor taskExecutor() {
        return new SimpleAsyncTaskExecutor();
    }

    @Bean
    public CommandLineRunner schedulingRunner(TaskExecutor executor) {
        return new CommandLineRunner() {
            public void run(String... args) throws Exception {
                SimpleConsumer consumer = applicationContext.getBean(SimpleConsumer.class);
                executor.execute(consumer);
            }
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(RiddleQuestServiceApplication.class, args);

    }
}
