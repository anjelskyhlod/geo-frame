package geoframe.servers.riddleQuestService.connection;

import org.json.simple.JSONObject;

/**
 * @author Lucie Kucharova
 * Interface for JSON-serializable objects
 */

public interface JsonSerializable {
    JSONObject toJson();
}
