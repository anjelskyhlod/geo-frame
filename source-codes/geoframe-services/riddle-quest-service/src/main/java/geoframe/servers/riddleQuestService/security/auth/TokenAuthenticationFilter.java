package geoframe.servers.riddleQuestService.security.auth;

import geoframe.servers.riddleQuestService.security.CustomUserDetails;
import geoframe.servers.riddleQuestService.security.TokenHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Lucie Kucharova
 * Provides @{@link JwtSecurityContext} from given token
 */

@Component
public class TokenAuthenticationFilter {

    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private TokenHelper tokenHelper;

//    public TokenAuthenticationFilter(TokenHelper tokenHelper) {
//        this.tokenHelper = tokenHelper;
//    }

    public JwtSecurityContext filterAndCreateContext(String request) {

        String username = "";
        List<String> authorities;
        JwtSecurityContext context = null;

        String authToken = tokenHelper.getToken(request);

        if (authToken != null) {
            username = tokenHelper.getUsernameFromToken(authToken);

            if (username != null) {
                authorities = tokenHelper.getAuthoritiesFromToken(authToken);
                CustomUserDetails userDetails = new CustomUserDetails(username, authorities);

                TokenBasedAuthentication authentication = new TokenBasedAuthentication(userDetails);
                authentication.setToken(authToken);
                context = new JwtSecurityContext(authentication);
            }
        }
        return context;
    }

}