package geoframe.servers.riddleQuestService.security.auth;

import java.util.List;

/**
 * @author Lucie Kucharova
 * Stores information about user's authentication
 */

public class JwtSecurityContext {

    private TokenBasedAuthentication authentication = null;

    public JwtSecurityContext(TokenBasedAuthentication auth) {
        this.authentication = auth;
    }

    public TokenBasedAuthentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(TokenBasedAuthentication auth) {
        authentication = auth;
    }

    public boolean hasRole(String role) {
        List<String> roles = authentication.getPrincipal().getAuthorities();
        for (String auth : roles) {
            String sAuth = auth.toString();
            if (sAuth.equals(role)) {
                return true;
            }
        }
        return false;
    }
}
