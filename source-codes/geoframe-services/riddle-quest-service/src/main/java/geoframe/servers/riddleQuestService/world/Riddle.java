package geoframe.servers.riddleQuestService.world;

import geoframe.servers.riddleQuestService.connection.JsonSerializable;
import org.json.simple.JSONObject;

import javax.persistence.*;

/**
 * @author Lucie Kucharova
 * Representation of riddle quest type
 */

@Entity
@Table(name = "riddle")
public class Riddle implements JsonSerializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private String answer;
    @Column
    private String hint;
    @Column
    private boolean active;
    @Column
    private String reward;

    public Riddle() {
    }

    public Riddle(JSONObject json) {
        fromJson(json);
    }

    public void setDefaultData() {
        this.name = "Simple text riddle";
        this.description = "This quest is based od simple riddle, question, which has to be correctly " +
                "answered in order to succeed. Here you put your question.";
        this.answer = "42";
        this.hint = "Riddle hint - little help, please!";
        this.active = true;
        this.reward = "Choose reward for completion.";
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("id", id);
        json.put("name", name);
        json.put("description", description);
        json.put("answer", answer);
        json.put("hint", hint);
        json.put("active", active);
        json.put("reward", reward);
        return json;
    }

    public JSONObject fromJson(JSONObject json) {
        Object data;
        data = json.get("id");
        if (data != null) {
            this.id = Long.parseLong(data.toString());
        }
        data = json.get("name");
        if (data != null) {
            this.name = data.toString();
        }
        data = json.get("description");
        if (data != null) {
            this.description = data.toString();
        }
        data = json.get("answer");
        if (data != null) {
            this.answer = data.toString();
        }
        data = json.get("hint");
        if (data != null) {
            this.hint = data.toString();
        }
        data = json.get("active");
        if (data != null) {
            this.active = Boolean.parseBoolean(data.toString());
        }
        data = json.get("reward");
        if (data != null) {
            this.reward = (data.toString());
        }
        return json;
    }
}
