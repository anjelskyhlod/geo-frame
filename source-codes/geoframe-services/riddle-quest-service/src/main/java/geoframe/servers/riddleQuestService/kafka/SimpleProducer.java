package geoframe.servers.riddleQuestService.kafka;

import geoframe.servers.riddleQuestService.security.ServiceAuthentication;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Properties;

/**
 * @author Lucie Kucharova
 * This class ensures message delivery to Kafka broker.
 */

@Component
public class SimpleProducer {

    private Producer<String, String> producer;

    @Autowired
    ServiceAuthentication serviceAuthentication;

    public SimpleProducer(@Value("${kafka.url}") final String KAFKA_URL) {
        Properties props = new Properties();
        props.put("bootstrap.servers", KAFKA_URL);
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        props.put("request.required.acks", "1");
        producer = new KafkaProducer
                <String, String>(props);
    }

    public void sendMessage(String topic, JSONObject msg) {
        System.out.println(msg.toJSONString());
        producer.send(new ProducerRecord<String, String>(topic,
                topic, msg.toJSONString()));
        System.out.println("Message sent to topic: " + topic + " successfully");
    }

    public void sendToService(String topic, JSONObject msg) {
        producer.send(new ProducerRecord<String, String>(topic,
                serviceAuthentication.getSERVICE_NAME(), msg.toJSONString()));
    }

    public void closeConnections() {
        producer.close();
    }

}
