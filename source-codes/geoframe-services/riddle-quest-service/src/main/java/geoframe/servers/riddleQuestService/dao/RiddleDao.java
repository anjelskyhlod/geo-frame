package geoframe.servers.riddleQuestService.dao;

import geoframe.servers.riddleQuestService.world.Riddle;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Lucie Kucharova
 * DAO for @{@link Riddle}
 */

@Component
public class RiddleDao extends AbstractDao<Riddle> {
    public RiddleDao() {
        super(Riddle.class);
    }

    public List<Riddle> findByIds(List<Long> idsList) {
        String values = "(";
        boolean first = true;
        for(Long id : idsList){
            if(first){
                values += id.toString();
                first = false;
                continue;
            }
            values += "," + id.toString();
        }
        values += ")";
        System.out.println(values);
        return em.createQuery("SELECT g FROM "+ clazz.getName() + " g WHERE g.id in" + values).getResultList();
    }
}
