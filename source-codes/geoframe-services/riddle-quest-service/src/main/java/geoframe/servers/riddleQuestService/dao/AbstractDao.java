package geoframe.servers.riddleQuestService.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Lucie Kucharova
 * Abstract DAO with base operations defined
 */

public class AbstractDao<T> {
    @PersistenceContext
    EntityManager em;

    protected Class<T> clazz;

    public AbstractDao(Class clazz) {
        this.clazz = clazz;
    }

    public T findById(Long id) {
        return em.find(clazz, id);
    }

    public List<T> findAll() {
        return em.createQuery("from " + clazz.getName())
                .getResultList();
    }

    @Transactional
    public T save(T entity) {
        em.persist(entity);
        em.flush();
        return entity;
    }

    @Transactional
    public void update(T entity) {
        em.merge(entity);
    }

    @Transactional
    public void delete(T entity) {
//        em.remove(em.contains(entity) ? entity : em.merge(entity));
        em.remove(entity);
    }

    @Transactional
    public void deleteById(Long entityId) {
        T entity = findById(entityId);
        delete(entity);
    }
}
