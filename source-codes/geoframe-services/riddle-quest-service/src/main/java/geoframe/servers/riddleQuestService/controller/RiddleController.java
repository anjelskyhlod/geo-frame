package geoframe.servers.riddleQuestService.controller;

import geoframe.servers.riddleQuestService.connection.Event;
import geoframe.servers.riddleQuestService.connection.EventResponse;
import geoframe.servers.riddleQuestService.connection.EventType;
import geoframe.servers.riddleQuestService.connection.JsonHelper;
import geoframe.servers.riddleQuestService.kafka.SimpleProducer;
import geoframe.servers.riddleQuestService.security.TokenHelper;
import geoframe.servers.riddleQuestService.service.RiddleService;
import geoframe.servers.riddleQuestService.world.Riddle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Lucie Kucharova
 * This class handles actions related to @{@link Riddle}
 */

@Component
public class RiddleController {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    @Autowired
    RiddleService riddleService;

    @Autowired
    SimpleProducer producer;

    @Autowired
    JsonHelper jsonHelper;

    @Autowired
    TokenHelper tokenHelper;

    public EventResponse getQuestTypeInfo() {
        EventResponse response = new EventResponse();
        response.setEventType(EventType.QUEST_TYPE_RIDDLE);
        Riddle riddle = new Riddle();
        riddle.setDefaultData();
        response.addData("questType", riddle);
        return response;
    }

    /**
     * Saves new quest and notifies game object service
     * for binding it to it
     *
     * @param event
     * @return Success/failue response
     */
    public EventResponse saveQuest(Event event) {
        EventResponse response = createEventResponse(EventType.SAVED_QUEST_TYPE_RIDDLE);
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");

        if (data == null) {
            response.addData("success", false);
            return response;
        }

        JSONObject quest = jsonHelper.getJsonObject(data, "quest");
        Long gameObjectId = null;
        try{
            gameObjectId = jsonHelper.getLong(data, "objectId");
        } catch (NumberFormatException e){
            LOGGER.info("Object has bad id: '" + jsonHelper.getString(data, "objectId") + "'. Saving quest but" +
                    "not notifying game object");
//            e.printStackTrace();
        }

        Riddle riddle = new Riddle(quest);
        // every riddle is unique, can be copied etc, so when
        // one is updated/deleted, other copies stays the same
        riddle.setId(null);
        try {
            riddleService.save(riddle);
            response.addData("success", true);
        } catch (Exception e) {
            response.addData("success", false);
        }
        if(gameObjectId != null) {
            notifyAndAddToGameObject(gameObjectId, riddle.getId(), event.getReturnTo());
        }
        response.addData("questId", createServiceSpecificId(riddle.getId()));
        riddle.setId(null);
        response.addData("quest", riddle.toJson());
        response.addData("success", true);
        return response;
    }

    /**
     * Finds all quests from db and marks their ID with
     * ID specific for this quest type
     *
     * @return all quests id db
     */
    public EventResponse getAll() {
        EventResponse response = createEventResponse(EventType.ALL_QUEST_TYPE_RIDDLE);

        List<Riddle> riddles = riddleService.findAll();
        JSONArray serRiddles = new JSONArray();
        for (Riddle riddle : riddles) {
            JSONObject jsonRiddle = createServiceSpecificId(riddle.toJson());
            serRiddles.add(jsonRiddle);
        }
        response.addData("allQuests", serRiddles);
        return response;
    }

    /**
     * Checks given ids and loads from database quest info
     * according to id
     *
     * @param event
     * @return quest type riddles for specific ids
     */
    public EventResponse getRiddlesById(Event event) {
        EventResponse response = createEventResponse(EventType.QUEST_TYPE_RIDDLE_BY_IDS);
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");

        List<Long> idsList = new ArrayList<>();
        List<Object> objList = (List<Object>) data.get("questIds");
        // if quest ids to load are empty...
        if (objList == null || objList.size() <= 0) {
            response.addData("success", false);
            return response;
        }
        // if ids are in specific id form for this service.. QUEST_TYPE_RIDDLE_[id]
        if (objList.get(0) instanceof String) {
            List<String> list = (List<String>) data.get("questIds");
            if (list.get(0).startsWith("QUEST_TYPE_RIDDLE_"))
                idsList = undoServiceSpecificId(list);
        } else {
            idsList = (List<Long>) data.get("questIds");
        }

        List<Riddle> riddles = riddleService.findByIds(idsList);
        JSONArray serRiddles = new JSONArray();
        for (Riddle riddle : riddles) {
            JSONObject jsonRiddle = createServiceSpecificId(riddle.toJson());
            serRiddles.add(jsonRiddle);
        }
        response.addData("success", true);
        response.addData("quests", serRiddles);
        return response;
    }

    /**
     * Checks if user answer on certain quest is correct
     *
     * @param event
     * @return Succes/failure answer
     */
    public EventResponse checkRightAnswer(Event event) {
        EventResponse response = createEventResponse(EventType.IS_SOLVED);
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");

        String username = tokenHelper.getUsernameFromToken(event.getSecurityContext().getAuthentication().getToken());

        Long questId = jsonHelper.getLong(data, "questId");
        String answer = jsonHelper.getString(data, "answer");

        Riddle riddle = riddleService.findById(questId);
        if (isAnswerCorrect(answer, riddle.getAnswer())) {
            response.addData("isCorrect", true);
            updateUserGameState(data, username, riddle.getReward());
        } else {
            response.addData("isCorrect", false);
        }
        response.addData("answer", answer);
        response.addData("questId", questId.toString());
        response.addData("questType", "QUEST_TYPE_RIDDLE");
        return response;
    }

//    ############################
//    ###### Helper methods ######
//    ############################

    private EventResponse createEventResponse(EventType eventType) {
        EventResponse response = new EventResponse();
        response.setEventType(eventType);
        return response;
    }

    /**
     * Emits event to inform new quest is to be
     * bound to object
     *
     * @param gameObjectId
     * @param riddleId
     * @param returnTo
     */
    private void notifyAndAddToGameObject(Long gameObjectId, Long riddleId, String returnTo) {
        JSONObject json = new JSONObject();
        JSONObject data = new JSONObject();
        data.put("objectId", gameObjectId);
        data.put("questId", createServiceSpecificId(riddleId));
        json.put("data", data);
        json.put("eventType", EventType.NEW_QUEST_ADDED.toString());
        json.put("returnTo", returnTo);
        producer.sendToService("game_object_service", json);
    }

    /**
     * Emits event to inform that user answered quest
     * correctly
     *
     * @param body
     * @param username
     * @param reward
     */
    private void updateUserGameState(JSONObject body, String username, String reward) {
        Long questId = jsonHelper.getLong(body, "questId");
        String questTypeSpecificId = createServiceSpecificId(questId);
        Long gameId = jsonHelper.getLong(body, "gameId");

        JSONObject data = new JSONObject();
        data.put("username", username);
        data.put("gameId", gameId);
        data.put("questId", questTypeSpecificId);
        data.put("reward", reward);

        JSONObject msg = new JSONObject();
        msg.put("data", data);
        msg.put("eventType", "UPDATE_CHARACTER");
        producer.sendToService("UPDATE_CHARACTER", msg);
    }

    private boolean isAnswerCorrect(String userAnswer, String correctAnswer) {
        return userAnswer.toLowerCase().equals(correctAnswer.toLowerCase());
    }

    /**
     * Specific ID is used by other services to determine
     * which quest type are they dealing with
     *
     * @param json
     * @return specific ID for this quest type
     */
    private JSONObject createServiceSpecificId(JSONObject json) {
        json.put("id", "QUEST_TYPE_RIDDLE_" + json.get("id").toString());
        return json;
    }

    /**
     * Specific ID is used by other services to determine
     * which quest type are they dealing with
     *
     * @param id
     * @return specific ID for this quest type
     */
    private String createServiceSpecificId(Long id) {
        return "QUEST_TYPE_RIDDLE_" + id.toString();
    }

    /**
     * Specific ID is used by other services to determine
     * which quest type are they dealing with
     *
     * @param list
     * @return IDs of quest which can be found id db
     */
    private List<Long> undoServiceSpecificId(List<String> list) {
        List<Long> ids = new ArrayList<>();

        for (String specId : list) {
            String str = specId.substring(specId.lastIndexOf('_') + 1);
            ids.add(Long.parseLong(str));
        }
        return ids;
    }
}
