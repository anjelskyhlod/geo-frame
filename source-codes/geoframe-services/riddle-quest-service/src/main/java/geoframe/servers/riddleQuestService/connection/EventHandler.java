package geoframe.servers.riddleQuestService.connection;

import geoframe.servers.riddleQuestService.controller.AuthenticationController;
import geoframe.servers.riddleQuestService.controller.RiddleController;
import geoframe.servers.riddleQuestService.kafka.SimpleProducer;
import geoframe.servers.riddleQuestService.security.TokenHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @author Lucie Kucharova
 * Mechanism responsible for navigating incoming @{@link Event} from broker
 * to right controller, which knows how to handle it.
 */

@Component
public class EventHandler {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    @Autowired
    SimpleProducer eventSender;

    @Autowired
    AuthenticationController authenticationController;

    @Autowired
    RiddleController riddleController;

    @Autowired
    TokenHelper tokenHelper;

    public void process(Event event) {
        EventType eventType = event.getType();
        EventResponse response = null;
        switch (eventType) {
            case GET_AVAILABLE_QUEST_TYPES:
                response = riddleController.getQuestTypeInfo();
                break;
            case SAVE_QUEST_TYPE_RIDDLE:
                response = riddleController.saveQuest(event);
                break;
            case GET_ALL_QUEST_TYPE_RIDDLE:
                response = riddleController.getAll();
                break;
            case GET_QUESTS_BY_IDS:
                response = riddleController.getRiddlesById(event);
                break;
            case CHECK_SOLVED:
                response = riddleController.checkRightAnswer(event);
                break;
//            case QUEST_TYPE_RIDDLE:
//                response = riddleController.getRiddlesById(body);
//                isResponseExpected = false;
//                break;
            default:
                LOGGER.warn("Process event: Unknown event Type.");
        }
        if (response != null) {
            eventSender.sendMessage(getReturnTopic(event), response.toJson());
        }
    }

    private String getReturnTopic(Event event) {
        return event.getReturnTo() != null ? event.getReturnTo() : event.getUserId();
    }
}
