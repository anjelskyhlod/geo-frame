package geoframe.servers.riddleQuestService.security;

import org.json.simple.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Lucie Kucharova
 * Provides service authentication
 */

@Component
@Scope("singleton")
public class ServiceAuthentication {

    private String authToken;
    private final String SERVICE_NAME = "riddle_quest_service";
    private final String SERVICE_PASS = "pass";

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public JSONObject getCredentials() {
        JSONObject json = new JSONObject();
        json.put("username", SERVICE_NAME);
        json.put("password", SERVICE_PASS);
        return json;
    }

    public String getSERVICE_NAME() {
        return SERVICE_NAME;
    }
}
