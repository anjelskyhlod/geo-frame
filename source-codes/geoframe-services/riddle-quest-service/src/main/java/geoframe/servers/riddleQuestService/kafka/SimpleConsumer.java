package geoframe.servers.riddleQuestService.kafka;

import geoframe.servers.riddleQuestService.connection.Event;
import geoframe.servers.riddleQuestService.connection.EventBuilder;
import geoframe.servers.riddleQuestService.connection.EventHandler;
import geoframe.servers.riddleQuestService.connection.EventType;
import geoframe.servers.riddleQuestService.security.ServiceAuthentication;
import geoframe.servers.riddleQuestService.security.auth.JwtSecurityContext;
import geoframe.servers.riddleQuestService.security.auth.TokenAuthenticationFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * @author Lucie Kucharova
 * This class represents broker listener. Works in loop
 * and provides new messeges from Kafka to the rest of
 * service from topics specified in constructor
 */

@Component("simpleConsumer")
@Scope("prototype")
public class SimpleConsumer implements Runnable {

    private final Log LOGGER = LogFactory.getLog(this.getClass());

    @Autowired
    private EventHandler eventHandler;

    @Autowired
    private TokenAuthenticationFilter tokenAuthenticationFilter;

    @Autowired
    ServiceAuthentication serviceAuthentication;

    @Autowired
    SimpleProducer producer;

    KafkaConsumer<String, String> consumer;
    Event currentEvent;
    private EventBuilder eventBuilder;


    public SimpleConsumer(@Value("${kafka.url}") final String KAFKA_URL) {
        this.eventBuilder = new EventBuilder();

        List<String> topics = new ArrayList<>();
        EventType[] events = EventType.values();
        for (EventType event : events) {
            String evt = event.toString();
            if (evt.startsWith("GET_") ||
                    evt.startsWith("SAVE_") ||
                    evt.startsWith("DELETE_") ||
                    evt.startsWith("UPDATE_"))
                topics.add(event.toString());
        }
        topics.add("GET_QUEST_TYPE_service");
        topics.add("RIDDLE_QUEST_SERVICE");
        topics.add("RIDDLE_QUEST_SERVICE_service");
        topics.add("QUEST_TYPE_RIDDLE");
        Properties props = new Properties();

        props.put("bootstrap.servers", KAFKA_URL);
        props.put("group.id", "test");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "30000");
        props.put("key.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        consumer = new KafkaConsumer
                <String, String>(props);

        consumer.subscribe(topics);
        LOGGER.info("Subscribed to topic " + topics.toString());

    }

    public void run() {
        System.out.println("running");
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(100);
            for (ConsumerRecord<String, String> record : records) {
                // print the topic, key and value of the consumer records.
                LOGGER.info("Riddle Quest service consumer: ");
                LOGGER.info(String.format("topic = %s, key = %s, value = %s\n",
                        record.topic(), record.key(), record.value()));

                // differentiate between service and user access
                if (record.topic().endsWith("_service")) {
                    currentEvent = eventBuilder.createEvent(record.key(), record.value(), null);
                    eventHandler.process(currentEvent);
                } else {
                    JwtSecurityContext ctx = tokenAuthenticationFilter.filterAndCreateContext(record.value());
                    if (ctx != null) {
                        currentEvent = eventBuilder.createEvent(record.key(), record.value(), ctx);
                        eventHandler.process(currentEvent);
                    }
                }
            }
        }
    }
}

