package geoframe.servers.authenticationservice.security;

import geoframe.servers.authenticationservice.common.TimeProvider;
import geoframe.servers.authenticationservice.model.Authority;
import geoframe.servers.authenticationservice.model.User;
import geoframe.servers.authenticationservice.service.AuthorityService;
import geoframe.servers.authenticationservice.service.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by fan.jin on 2016-10-19.
 * Updated by Kucharova Lucie
 * Helps with token manipulation, retrieving data from it or
 * generation token string
 */

@Component
public class TokenHelper {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    @Value("${app.name}")
    private String APP_NAME;

    @Value("${jwt.secret}")
    public String SECRET;

    @Value("${jwt.expires_in}")
    private int EXPIRES_IN;

    @Value("${jwt.mobile_expires_in}")
    private int MOBILE_EXPIRES_IN;

    @Value("${jwt.header}")
    private String AUTH_HEADER;

    @Autowired
    TimeProvider timeProvider;

    @Autowired
    AuthorityService authorityService;

    @Autowired
    UserService userService;

    private SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS512;

    public String getUsernameFromToken(String token) {
        String username;
        try {
            final Claims claims = this.getAllClaimsFromToken(token);
            username = claims.getSubject();
        } catch (Exception e) {
            username = null;
        }
        if (username != null && username.startsWith("\"")) {
            username = username.replace("\"", "");
        }
        return username;
    }

    public List<String> getAuthoritiesFromToken(String token) {
        List<String> scopes;
        List<String> authorities;
        try {
            final Claims claims = this.getAllClaimsFromToken(token);
            scopes = claims.get("scopes", List.class);
            authorities = scopes.stream()
                    .map(authority -> authority)
                    .collect(Collectors.toList());

        } catch (Exception e) {
            authorities = null;
        }
        return authorities;
    }

    public String refreshToken(String token) {
        String refreshedToken;
        Date a = timeProvider.now();
        try {
            final Claims claims = this.getAllClaimsFromToken(token);
            claims.setIssuedAt(a);
            String username = this.getUsernameFromToken(token);
            User user = userService.findByUsername(username);
            List<Authority> authorities = user.getAuthorities();
            claims.put("scopes", authorities.stream().map(s -> s.toString()).collect(Collectors.toList()));
            refreshedToken = Jwts.builder()
                    .setClaims(claims)
                    .setExpiration(generateExpirationDate())
                    .signWith(SIGNATURE_ALGORITHM, SECRET)
                    .compact();
        } catch (Exception e) {
            refreshedToken = null;
        }
        return refreshedToken;
    }

    public String generateToken(String username, Collection authorities) {
        if (username.startsWith("\"")) {
            username = username.replace("\"", "");
        }
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("scopes", authorities.stream().map(s -> s.toString()).collect(Collectors.toList()));

        return Jwts.builder()
                .setIssuer(APP_NAME)
                .setClaims(claims)
                .setIssuedAt(timeProvider.now())
                .signWith(SIGNATURE_ALGORITHM, SECRET)
                .compact();
    }

    private Claims getAllClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }

    private Date generateExpirationDate() {
        long expiresIn = EXPIRES_IN;
        return new Date(timeProvider.now().getTime() + expiresIn * 1000);
    }

    public int getExpiredIn() {
        return EXPIRES_IN;
    }

    public String getToken(String value) {
        /**
         *  Getting the token from Authentication header
         *  e.g Bearer your_token
         */

        String authHeader = getAuthHeaderFromHeader(value);
        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            return authHeader.substring(7);
        }

        return null;
    }

    public String getToken(JSONObject value) {
        /**
         *  Getting the token from Authentication header
         *  e.g Bearer your_token
         */

        String authHeader = null;

        Object objToken = value.get(AUTH_HEADER);
        if (objToken == null) objToken = value.get(AUTH_HEADER.toLowerCase());
        if (objToken != null) {
            authHeader = objToken.toString();
        }


        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            return authHeader.substring(7);
        }

        return null;
    }

    public String getAuthHeaderFromHeader(String str) {
        JSONParser parser = new JSONParser();
        JSONObject json = null;
        String token = null;
        try {
            json = (JSONObject) parser.parse(str);
            Object objToken = json.get(AUTH_HEADER);
            if (objToken == null) objToken = json.get(AUTH_HEADER.toLowerCase());
            if (objToken != null) {
                token = objToken.toString();
            }

        } catch (ParseException e) {
            LOGGER.info("createEvent parsing value to json se nezdarilo.");
            e.printStackTrace();
        }
        return token;
    }

}