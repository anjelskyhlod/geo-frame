package geoframe.servers.authenticationservice.kafka;

import geoframe.servers.authenticationservice.security.ServiceAuthentication;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Properties;

/**
 * @author Lucie Kucharova
 * This class ensures message delivery to Kafka broker.
 */

@Component
public class SimpleProducer {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    @Autowired
    ServiceAuthentication serviceAuthentication;

    private Producer<String, String> producer;

    @Autowired
    public SimpleProducer(@Value("${kafka.url}") final String KAFKA_URL) {
        Properties props = new Properties();
        LOGGER.info("Kafka bootstrap servers:" + KAFKA_URL);
        props.put("bootstrap.servers", KAFKA_URL);
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        producer = new KafkaProducer
                <String, String>(props);
    }

    public void sendResponseMessage(String topic, JSONObject msg) {
        producer.send(new ProducerRecord<String, String>(topic.toString(),
                topic, msg.toJSONString()));
        log(topic);
    }

    public void closeConnections() {
        producer.close();
    }

    private void log(String topic) {
        LOGGER.info("Message sent to topic: " + topic + "  successfully");
    }

}
