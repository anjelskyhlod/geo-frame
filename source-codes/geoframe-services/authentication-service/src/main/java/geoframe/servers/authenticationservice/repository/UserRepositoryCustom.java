package geoframe.servers.authenticationservice.repository;

/**
 * @author Lucie Kucharova
 * Custom repository
 */

public interface UserRepositoryCustom {
    void addNewRole(Long autdId, Long userId);
}
