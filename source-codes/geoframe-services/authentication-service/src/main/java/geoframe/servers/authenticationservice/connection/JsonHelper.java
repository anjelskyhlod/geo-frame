package geoframe.servers.authenticationservice.connection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Component;

/**
 * @author Lucie Kucharova
 * This utility is used for retreiving data from JSON format objects.
 */

@Component
public class JsonHelper {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    JSONParser parser;

    public JsonHelper() {
        parser = new JSONParser();
    }

    public JSONObject getJsonObject(JSONObject source, String key) {
        if (isNotNull(source, key)) {
            try {
                return (JSONObject) parser.parse(source.get(key).toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public String getString(JSONObject source, String key) {
        if (isNotNull(source, key)) {
            return source.get(key).toString();
        }
        return null;
    }

    public JSONObject createJsonFromString(String str) {
        JSONParser parser = new JSONParser();
        JSONObject json = null;
        try {
            json = (JSONObject) parser.parse(str);
        } catch (ParseException e) {
            LOGGER.error("Error parsing json from string.");
            e.printStackTrace();
        }
        return json;
    }


//    ############################
//    ###### Helper methods ######
//    ############################

    private boolean isNotNull(JSONObject source, String key) {
        if (source == null || key == null) return false;
        return source.get(key) != null;
    }
}
