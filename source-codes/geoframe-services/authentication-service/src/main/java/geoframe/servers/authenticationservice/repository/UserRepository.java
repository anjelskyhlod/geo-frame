package geoframe.servers.authenticationservice.repository;

import geoframe.servers.authenticationservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Lucie Kucharova
 * repository of @{@link User}
 */

public interface UserRepository extends JpaRepository<User, Long>, UserRepositoryCustom {
    User findByUsername(String username);
}

