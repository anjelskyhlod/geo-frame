package geoframe.servers.authenticationservice.service.impl;

import geoframe.servers.authenticationservice.model.User;
import geoframe.servers.authenticationservice.repository.UserRepository;
import geoframe.servers.authenticationservice.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lucie Kucharova
 * Service for @{@link User}
 */

@Service
public class UserServiceImpl implements UserService {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    @Autowired
    private UserRepository userRepository;

    @Override
    public User findByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username);
    }

    public User findById(Long id) throws AccessDeniedException {
        return userRepository.findOne(id);
    }

    public List<User> findAll() throws AccessDeniedException {
        return userRepository.findAll();
    }

    public void updateUser(User user) {
        userRepository.save(user);
    }

    public void addNewRole(Long autdId, Long userId) {
        userRepository.addNewRole(autdId, userId);
    }
}
