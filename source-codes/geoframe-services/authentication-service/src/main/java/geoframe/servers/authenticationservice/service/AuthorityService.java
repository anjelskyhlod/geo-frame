package geoframe.servers.authenticationservice.service;

import geoframe.servers.authenticationservice.model.Authority;

/**
 * @author Lucie Kucharova
 */

public interface AuthorityService {
    Authority findByName(String name);

    void saveNewAuthority(Authority authority);

    void deleteAuthority(Authority authToRemove);
}
