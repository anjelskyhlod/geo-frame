package geoframe.servers.authenticationservice.connection;

import geoframe.servers.authenticationservice.controller.AuthorityController;
import geoframe.servers.authenticationservice.kafka.SimpleProducer;
import geoframe.servers.authenticationservice.security.TokenHelper;
import geoframe.servers.authenticationservice.service.impl.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Lucie Kucharova
 * Mechanism responsible for navigating incoming @{@link Event} from broker
 * to right controller, which knows how to handle it. Simmilar to @{@link EventHandler},
 * but handles messages sent by other services only (not user applications).
 */

@Component
public class ServiceEventHandler {

    @Autowired
    SimpleProducer eventSender;

    @Autowired
    TokenHelper tokenHelper;

    @Autowired
    CustomUserDetailsService userDetailsService;

    @Autowired
    AuthorityController authorityController;

    public void process(Event event) {
        EventType eventType = event.getType();
        switch (eventType) {
            case SAVE_NEW_ROLES:
                authorityController.saveNewRoles(event);
                break;
            case DELETE_ROLES:
                authorityController.deleteRoles(event);
                break;
            default:
                System.out.println("Process event: Unknown event Type.");
        }
    }


}
