package geoframe.servers.authenticationservice.repository;

import geoframe.servers.authenticationservice.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Lucie Kucharova
 * repository of @{@link Authority}
 */

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    Authority findByName(String name);
}
