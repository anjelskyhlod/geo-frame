package geoframe.servers.authenticationservice.connection;

/**
 * @author Lucie Kucharova
 * These are all types of events, which can be received or produced by
 * this service.
 */
public enum EventType {
    REGISTER,
    GET_LOGIN,
    SEND_LOGIN,
    GET_REFRESH,
    AUTH_TOKEN,
    GET_SINGLE_USER,
    SINGLE_USER,
    SAVE_NEW_ROLES,
    REGISTERED,
    DELETE_ROLES
}
