package geoframe.servers.authenticationservice.connection;

import geoframe.servers.authenticationservice.controller.UserController;
import geoframe.servers.authenticationservice.kafka.SimpleProducer;
import geoframe.servers.authenticationservice.security.TokenHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Lucie Kucharova
 * Mechanism responsible for navigating incoming @{@link Event} from broker
 * to right controller, which knows how to handle it.
 */

@Component
public class EventHandler {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    @Autowired
    SimpleProducer eventSender;

    @Autowired
    TokenHelper tokenHelper;

    @Autowired
    UserController userController;

    public void process(Event event) {
        EventType eventType = event.getType();
        EventResponse response = null;
        switch (eventType) {
            case REGISTER:
                response = userController.register(event);
                break;
            case GET_LOGIN:
                response = userController.login(event);
                break;
            case GET_REFRESH:
                response = userController.refresh(event);
                break;
            case GET_SINGLE_USER:
                response = userController.getSingleUser(event);
                break;
            default:
                LOGGER.warn("Process event: Unknown event Type.");
        }
        if (response != null) {
            eventSender.sendResponseMessage(getReturnTopic(event), response.toJson());
        }
    }

    private String getReturnTopic(Event event) {
        return event.getReturnTo() != null ? event.getReturnTo() : event.getUserId();
    }

}
