package geoframe.servers.authenticationservice.kafka;

import geoframe.servers.authenticationservice.connection.*;
import geoframe.servers.authenticationservice.security.auth.JwtSecurityContext;
import geoframe.servers.authenticationservice.security.auth.TokenAuthenticationFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


/**
 * @author Lucie Kucharova
 * This class represents broker listener. Works in loop
 * and provides new messeges from Kafka to the rest of
 * service from topics specified in constructor
 */


@Component("simpleConsumer")
@Scope("prototype")
public class SimpleConsumer implements Runnable {

    private final Log LOGGER = LogFactory.getLog(this.getClass());

    @Autowired
    private EventHandler eventHandler;

    @Autowired
    private ServiceEventHandler serviceEventHandler;

    @Autowired
    private TokenAuthenticationFilter tokenAuthenticationFilter;

    private KafkaConsumer<String, String> consumer;
    private Event currentEvent;
    private EventBuilder eventBuilder;


    @Autowired
    public SimpleConsumer(@Value("${kafka.url}") final String KAFKA_URL) {
        this.eventBuilder = new EventBuilder();
        List<String> topics = new ArrayList<>();

        topics.add(EventType.GET_LOGIN.toString());
        topics.add(EventType.REGISTER.toString());
        topics.add(EventType.GET_REFRESH.toString());
        topics.add(EventType.GET_SINGLE_USER.toString());
        topics.add("authentication_service");

        Properties props = new Properties();
        LOGGER.info("Kafka bootstrap servers:" + KAFKA_URL);
        props.put("bootstrap.servers", KAFKA_URL);
        props.put("group.id", "auth");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "30000");
        props.put("key.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        consumer = new KafkaConsumer
                <String, String>(props);
        consumer.subscribe(topics);
        LOGGER.info("Subscribed to topics: " + topics.toString());
    }

    public void run() {
        System.out.println("running");
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(100);
            for (ConsumerRecord<String, String> record : records) {
                LOGGER.info("GameManager consumer: ");
                LOGGER.info(String.format("topic = %s, key = %s, value = %s\n",
                        record.topic(), record.key(), record.value()));

                // differentiate between service and user access
                if (record.key() != null && record.key().endsWith("_service")) {
                    currentEvent = eventBuilder.createEvent(record.key(), record.value(), null);
                    serviceEventHandler.process(currentEvent);
                } else {
                    JwtSecurityContext ctx = tokenAuthenticationFilter.filterAndCreateContext(record.value());
//                    if (ctx != null) { // not logged in yet
                    currentEvent = eventBuilder.createEvent(record.key(), record.value(), ctx);
                    eventHandler.process(currentEvent);
//                    }
                }

            }
        }
    }
}

