package geoframe.servers.authenticationservice.controller;

import geoframe.servers.authenticationservice.connection.Event;
import geoframe.servers.authenticationservice.connection.JsonHelper;
import geoframe.servers.authenticationservice.model.Authority;
import geoframe.servers.authenticationservice.model.User;
import geoframe.servers.authenticationservice.service.AuthorityService;
import geoframe.servers.authenticationservice.service.UserService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Lucie Kucharova
 * This class handles actions related to @{@link Authority}
 */

@Component
public class AuthorityController {

    @Autowired
    JsonHelper jsonHelper;

    @Autowired
    AuthorityService authorityService;

    @Autowired
    UserService userService;

    /**
     * Adds new role for user according to data given in @{@link Event}
     *
     * @param event
     */

    public void saveNewRoles(Event event) {
        JSONObject body = jsonHelper.getJsonObject(event.getValue(), "data");
        String role = jsonHelper.getString(body, "role");
        String username = jsonHelper.getString(body, "username");

        if (role != null && username != null) {
            Authority auth = new Authority();
            auth.setName(role);
            authorityService.saveNewAuthority(auth);
            try {
                User user = userService.findByUsername(username);
                userService.addNewRole(auth.getId(), user.getId());
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
    }

    /**
     * Deletes specific role from user
     *
     * @param event
     */

    public void deleteRoles(Event event) {
        JSONObject body = jsonHelper.getJsonObject(event.getValue(), "data");
        String role = jsonHelper.getString(body, "role");
        String username = jsonHelper.getString(body, "username");

        if (role != null && username != null) {
            try {
                Authority authToRemove = new Authority();
                authToRemove.setName(role);
                User user = userService.findByUsername(username);
                for (Authority authority : user.getAuthorities()) {
                    if (authority.getName().equals(role)) {
                        authToRemove = authority;
                    }
                }
                user.getAuthorities().remove(authToRemove);
                userService.updateUser(user);
                authorityService.deleteAuthority(authToRemove);
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        }
    }
}
