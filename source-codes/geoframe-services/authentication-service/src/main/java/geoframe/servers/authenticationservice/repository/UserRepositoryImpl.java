package geoframe.servers.authenticationservice.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

/**
 * @author Lucie Kucharova
 * Implementation of @{@link UserRepositoryCustom}
 */

public class UserRepositoryImpl implements UserRepositoryCustom {

    @PersistenceContext
    EntityManager em;

    @Override
    @Transactional
    public void addNewRole(Long autdId, Long userId) {
        Query query = em.createNativeQuery("INSERT INTO user_authority " +
                "(user_id, authority_id) VALUES (" + userId + ", " + autdId + ")");
        query.executeUpdate();
    }
}
