package geoframe.servers.authenticationservice.security;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * @author Lucie Kucharova
 * Object representation of user's roles
 */

public class CustomUserDetails {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    private String username;
    private List<String> authorities;

    public CustomUserDetails(String username, List<String> authorities) {
        this.username = username;
        this.authorities = authorities;
    }

    public List<String> getAuthorities() {
        return this.authorities;
    }


    public String getPassword() {
        return null;
    }


    public String getUsername() {
        return this.username;
    }

}