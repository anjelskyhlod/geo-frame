package geoframe.servers.authenticationservice.service.impl;

import geoframe.servers.authenticationservice.model.Authority;
import geoframe.servers.authenticationservice.repository.AuthorityRepository;
import geoframe.servers.authenticationservice.service.AuthorityService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Lucie Kucharova
 * Service for @{@link Authority}
 */

@Service
public class AuthorityServiceImpl implements AuthorityService {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    @Autowired
    AuthorityRepository authorityRepository;

    @Override
    public Authority findByName(String name) {
        return authorityRepository.findByName(name);
    }

    @Override
    public void saveNewAuthority(Authority authority) {
        authorityRepository.save(authority);
    }

    @Override
    public void deleteAuthority(Authority authToRemove) {
        authorityRepository.delete(authToRemove.getId());
    }
}
