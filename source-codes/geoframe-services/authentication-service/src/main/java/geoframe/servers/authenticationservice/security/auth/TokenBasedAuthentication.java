package geoframe.servers.authenticationservice.security.auth;

import geoframe.servers.authenticationservice.security.CustomUserDetails;

/**
 * @author Lucie Kucharova
 * Provides information about user's authentication
 */

public class TokenBasedAuthentication {

    private String token;
    private final CustomUserDetails principle;

    public TokenBasedAuthentication(CustomUserDetails principle) {
        this.principle = principle;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public CustomUserDetails getPrincipal() {
        return principle;
    }

}