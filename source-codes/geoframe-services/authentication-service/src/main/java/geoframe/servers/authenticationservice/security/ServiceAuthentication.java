package geoframe.servers.authenticationservice.security;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Lucie Kucharova
 * Provides service authentication
 */

@Component
@Scope("singleton")
public class ServiceAuthentication {

    private String authToken;
    private final String SERVICE_NAME = "auth_service";

    public String getAuthToken() {
        return authToken;
    }
}
