package geoframe.servers.authenticationservice.security.auth;

/**
 * @author Lucie Kucharova
 * Stores information about user's authentication
 */

public class JwtSecurityContext {

    private TokenBasedAuthentication authentication = null;

    public JwtSecurityContext(TokenBasedAuthentication auth) {
        this.authentication = auth;
    }

    public TokenBasedAuthentication getAuthentication() {
        return authentication;
    }

    public void setAuthentication(TokenBasedAuthentication auth) {
        authentication = auth;
    }
}
