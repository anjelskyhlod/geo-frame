package geoframe.servers.authenticationservice.controller;

import geoframe.servers.authenticationservice.connection.*;
import geoframe.servers.authenticationservice.kafka.SimpleProducer;
import geoframe.servers.authenticationservice.model.Authority;
import geoframe.servers.authenticationservice.model.User;
import geoframe.servers.authenticationservice.security.TokenHelper;
import geoframe.servers.authenticationservice.service.AuthorityService;
import geoframe.servers.authenticationservice.service.UserService;
import geoframe.servers.authenticationservice.service.impl.CustomUserDetailsService;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

/**
 * @author Lucie Kucharova
 * This class handles actions related to @{@link User}
 */

@Component
public class UserController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    JsonHelper jsonHelper;

    @Autowired
    TokenHelper tokenHelper;

    @Autowired
    SimpleProducer eventSender;

    @Autowired
    UserService userService;

    @Autowired
    CustomUserDetailsService userDetailsService;

    @Autowired
    AuthorityService authorityService;


    /**
     * Creates new @{@link User}, encodes password, adds new role
     * (ROLE_USER) for it and saves data to db.
     *
     * @param event
     * @return Success/failure info
     */
    public EventResponse register(Event event) {
        EventResponse response = createEventResponse(EventType.REGISTERED);

        User newUser = createNewUser(jsonHelper.getJsonObject(event.getValue(), "data"));
        try {
            Authority userRole = authorityService.findByName("ROLE_USER");
            userDetailsService.registerNewUser(newUser);
            userService.addNewRole(userRole.getId(), newUser.getId());

            response.addData("success", true);
        } catch (DataIntegrityViolationException e) {
            response.addData("success", false);
            response.addData("reason", "Username already exists.");
        }

        return response;
    }

    /**
     * Checks sender authentication, eventually generates and sends back
     * authentication token.
     *
     * @param event
     * @return Success/failure info, eventually user's detailed information
     */
    public EventResponse login(Event event) {
        EventResponse response = createEventResponse(EventType.AUTH_TOKEN);

        User user = createNewUser(jsonHelper.getJsonObject(event.getValue(), "data"));

        String jws = "";
        int expiresIn = 0;
        Authentication authentication = getAuthentication(user, response);

        if (authentication != null) {
            // create token
            user = (User) authentication.getPrincipal();
            jws = tokenHelper.generateToken(user.getUsername(), user.getAuthorities());
            expiresIn = tokenHelper.getExpiredIn();
            response.addData("success", "true");
        }
        response.addData("access_token", jws);
        response.addData("expires_in", expiresIn);

        eventSender.sendResponseMessage(getReturnTopic(event), response.toJson());
        response.setEventType(EventType.SEND_LOGIN);
        return response;
    }

    /**
     * If provided authentication token is valid, refreshes it
     * (prolongs it's expiration time)
     *
     * @param event
     * @return Success/failure info, if auth token is valid then
     * returns refreshed token
     */
    public EventResponse refresh(Event event) {
        EventResponse response = createEventResponse(EventType.AUTH_TOKEN);

        String authToken = getAuthToken(event);
        if (authToken != null) {
            String refreshedToken = tokenHelper.refreshToken(authToken);
            int expiresIn = tokenHelper.getExpiredIn();

            if (refreshedToken == null) {
                response.addData("success", false);
                return response;
            }
            response.addData("access_token", refreshedToken);
            response.addData("expires_in", expiresIn);
            response.addData("success", true);
        } else {
            response.addData("success", false);
        }
        return response;
    }

    /**
     * Finds user information from username stored in auth token
     *
     * @param event
     * @return Success/failure info, eventually user info
     */
    public EventResponse getSingleUser(Event event) {
        EventResponse response = createEventResponse(EventType.SINGLE_USER);

        String jws = getAuthToken(event);
        String username = tokenHelper.getUsernameFromToken(jws);

        try {
            User user = userService.findByUsername(username);
            if (user != null) {
                response.addData("user", user.toJson());
                response.addData("success", true);
            } else {
                response.addData("success", false);
            }
        } catch (Exception e) {
            response.addData("success", false);
            e.printStackTrace();
        }
        return response;
    }


//    ############################
//    ###### Helper methods ######
//    ############################

    /**
     * Creates new @{@link User} object from JSONObject
     *
     * @param body
     * @return @{@link User}
     */
    private User createNewUser(JSONObject body) {
        String username = jsonHelper.getString(body, "username");
        String password = jsonHelper.getString(body, "password");
        String email = jsonHelper.getString(body, "password");

        User newUser = new User();
        newUser.setUsername(username);
        newUser.setPassword(password);
        newUser.setEmail(email);
        newUser.setEnabled(true);

        return newUser;
    }

    private EventResponse createEventResponse(EventType eventType) {
        EventResponse response = new EventResponse();
        response.setEventType(eventType);
        return response;
    }

    /**
     * Checks if user can be authenticated
     *
     * @param user
     * @param response
     * @return Authentication, if user is authenticated
     */
    private Authentication getAuthentication(User user, EventResponse response) {
        Authentication authentication = null;
        if (user.getUsername() != null && user.getPassword() != null) {
            try {
                authentication = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(
                                user.getUsername(),
                                user.getPassword()
                        )
                );
            } catch (BadCredentialsException exception) {
                System.out.println(exception.getMessage());
                response.addData("reason", "Bad credentials.");
                response.addData("success", false);
            }
        }
        return authentication;
    }

    private String getReturnTopic(Event event) {
        return event.getReturnTo() != null ? event.getReturnTo() : event.getUserId();
    }

    /**
     * Deals with two possible structures of incoming JSON object
     * for retrieving auth token
     *
     * @param event
     * @return
     */
    private String getAuthToken(Event event) {
        String authToken = tokenHelper.getToken(event.getValue());
        if (authToken == null) {
            authToken = tokenHelper.getToken(jsonHelper.getString(event.getValue(), "data"));
        }
        return authToken;
    }


}
