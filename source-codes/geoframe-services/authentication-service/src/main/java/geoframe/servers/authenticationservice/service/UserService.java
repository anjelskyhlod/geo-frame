package geoframe.servers.authenticationservice.service;

import geoframe.servers.authenticationservice.model.User;

import java.util.List;

/**
 * @author Lucie Kucharova
 */

public interface UserService {
    User findById(Long id);

    User findByUsername(String username);

    List<User> findAll();

    void updateUser(User user);

    void addNewRole(Long autdId, Long userId);
}
