INSERT INTO users (id, username, password, first_name, last_name, email, phone_number, enabled, last_password_reset_date) VALUES (1, 'marvin1', '$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'Marvin1', 'Robot', 'marvin1@heartofgold.com', '+1234567890', true, '2017-10-01 18:57:58.508');
INSERT INTO users (id, username, password, first_name, last_name, email, phone_number, enabled, last_password_reset_date) VALUES (2, 'marvin2', '$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'Marvin2', 'Robot', 'marvin2@heartofgold.com', '+1234567890', true, '2017-10-01 18:57:58.508');
INSERT INTO users (id, username, password, first_name, last_name, email, phone_number, enabled, last_password_reset_date) VALUES (3, 'marvin3', '$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'Marvin3', 'Robot', 'marvin3@heartofgold.com', '+1234567890', true, '2017-10-01 18:57:58.508');
INSERT INTO users (id, username, password, first_name, last_name, email, phone_number, enabled, last_password_reset_date) VALUES (4, 'marvin4', '$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'Marvin4', 'Robot', 'marvin4@heartofgold.com', '+1234567890', true, '2017-10-01 18:57:58.508');
INSERT INTO users (id, username, password, first_name, last_name, email, phone_number, enabled, last_password_reset_date) VALUES (5, 'marvin5', '$2a$04$Vbug2lwwJGrvUXTj6z7ff.97IzVBkrJ1XfApfGNl.Z695zqcnPYra', 'Marvin5', 'Robot', 'marvin5@heartofgold.com', '+1234567890', true, '2017-10-01 18:57:58.508');

INSERT INTO authority (id, name) VALUES (1, 'ROLE_USER');
INSERT INTO authority (id, name) VALUES (2, 'ROLE_OWNER_1');

INSERT INTO user_authority (user_id, authority_id) VALUES (1, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES (2, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES (3, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES (4, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES (5, 1);

INSERT INTO user_authority (user_id, authority_id) VALUES (1, 2);
