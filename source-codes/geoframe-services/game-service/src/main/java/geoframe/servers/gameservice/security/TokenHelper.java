package geoframe.servers.gameservice.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by fan.jin on 2016-10-19.
 * Updated by Kucharova Lucie
 * Helps with token manipulation, retrieving data from it or
 * generation token string
 */

@Component
public class TokenHelper {

    @Value("${app.name}")
    private String APP_NAME;

    @Value("${jwt.secret}")
    public String SECRET;

    @Value("${jwt.header}")
    private String AUTH_HEADER;

    public String getUsernameFromToken(String token) {
        String username;
        try {
            final Claims claims = this.getAllClaimsFromToken(token);
            username = claims.getSubject();
        } catch (Exception e) {
            username = null;
        }
        if (username != null && username.startsWith("\"")) {
            username = username.replace("\"", "");
        }
        return username;
    }

    public List<String> getAuthoritiesFromToken(String token) {
        List<String> scopes;
        List<String> authorities;
        try {
            final Claims claims = this.getAllClaimsFromToken(token);
            scopes = claims.get("scopes", List.class);
            authorities = scopes.stream()
                    .map(authority -> authority)
                    .collect(Collectors.toList());

        } catch (Exception e) {
            authorities = null;
        }
        return authorities;
    }

    private Claims getAllClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }

    public String getToken(String value) {
        /**
         *  Getting the token from Authentication header
         *  e.g Bearer your_token
         */

        String authHeader = getAuthHeaderFromHeader(value);
        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            return authHeader.substring(7);
        }

        return null;
    }

    public String getAuthHeaderFromHeader(String str) {
        JSONParser parser = new JSONParser();
        JSONObject json = null;
        String token = null;
        try {
            json = (JSONObject) parser.parse(str);
            Object objToken = json.get(AUTH_HEADER);
            if (objToken == null) objToken = json.get(AUTH_HEADER.toLowerCase());
            if (objToken != null) {
                token = objToken.toString();
            }

        } catch (ParseException e) {
            System.out.println("createEvent parsing value to json se nezdarilo.");
            e.printStackTrace();
        }
        return token;
    }

}