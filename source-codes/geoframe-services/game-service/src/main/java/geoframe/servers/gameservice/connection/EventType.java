package geoframe.servers.gameservice.connection;

/**
 * @author Lucie Kucharova
 * These are all types of events, which can be received or produced by
 * this service.
 */

public enum EventType {
    NEW_CONNECTION,
    NEW_GAME,
    UPDATE_GAME,
    UPDATED_GAME,
    DELETE_GAME,
    DELETED_GAME,

    GET_GAMES_BY_USER,
    SAVE_NEW_GAME,
    SEND_GAMES_BY_USER,
    SAVED_NEW_GAME,
    GET_ALL_GAMES_BASIC,
    SEND_ALL_GAMES_BASIC,
    GET_GAME_BY_ID,
    SEND_GAME_BY_ID,

    AUTH_TOKEN
}
