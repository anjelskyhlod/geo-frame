package geoframe.servers.gameservice.connection;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.List;

/**
 * @author Lucie Kucharova
 * Representation of a response from controller, which is send back to broker.
 */

public class EventResponse<T> {
    private EventType eventType;
    private String topic;
    private JSONObject data;

    public EventResponse() {
        data = new JSONObject();
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public void addData(String key, T value) {
        this.data.put(key, value);
    }

    public void addData(String key, JSONObject data) {
        this.data.put(key, data);
    }

    public void addJsonArrayData(String key, JSONArray data) {
        this.data.put(key, data);
    }

    public void addData(String key, List<JsonSerializable> data) {
        JSONArray records = new JSONArray();
        for (JsonSerializable record : data) {
            records.add(record.toJson());
        }
        this.data.put(key, records);
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        json.put("eventType", eventType.toString());
        json.put("topic", topic);
        if (data != null)
            json.put("data", data);
        return json;
    }
}
