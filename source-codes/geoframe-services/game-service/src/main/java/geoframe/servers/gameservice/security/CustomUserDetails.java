package geoframe.servers.gameservice.security;

import java.util.List;

/**
 * @author Lucie Kucharova
 * Object representation of user's roles
 */

public class CustomUserDetails {

    private String username;
    private List<String> authorities;

    public CustomUserDetails(String username, List<String> authorities) {
        this.username = username;
        this.authorities = authorities;
    }


    public List<String> getAuthorities() {
        return this.authorities;
    }


    public String getPassword() {
        return null;
    }


    public String getUsername() {
        return this.username;
    }


    public boolean isAccountNonExpired() {
        return false;
    }


    public boolean isAccountNonLocked() {
        return false;
    }


    public boolean isCredentialsNonExpired() {
        return false;
    }


    public boolean isEnabled() {
        return false;
    }
}
