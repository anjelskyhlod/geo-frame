package geoframe.servers.gameservice.controller;

import geoframe.servers.gameservice.security.ServiceAuthentication;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Lucie Kucharova
 * Handles service authentication
 */

@Component
public class AuthenticationController {

    @Autowired
    ServiceAuthentication serviceAuthentication;

    public void setAuthToken(JSONObject data) {
        String token = data.get("access_token").toString();
        serviceAuthentication.setAuthToken(token);
    }

}
