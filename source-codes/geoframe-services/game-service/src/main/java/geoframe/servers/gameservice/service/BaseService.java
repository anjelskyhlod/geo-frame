package geoframe.servers.gameservice.service;

import java.util.List;

/**
 * @author Lucie Kucharova
 * Service with base operations
 */

public interface BaseService<T> {
    T findById(Long id);

    List<T> findAll();

    T save(T entity);

    void update(T entity);

    void delete(T entity);

    void deleteById(Long entityId);
}
