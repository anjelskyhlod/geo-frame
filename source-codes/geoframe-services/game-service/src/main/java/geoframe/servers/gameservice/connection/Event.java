package geoframe.servers.gameservice.connection;

import geoframe.servers.gameservice.security.auth.JwtSecurityContext;
import org.json.simple.JSONObject;

/**
 * @author Lucie Kucharova
 * Object representation of incoming message from broker
 */

public class Event {

    private String userId;
    private EventType type;
    private JSONObject value;
    private JwtSecurityContext securityContext;
    private String returnTo;

    public Event(String userId, EventType type, JSONObject value, JwtSecurityContext ctx) {
        this.userId = userId;
        this.type = type;
        this.value = value;
        this.securityContext = ctx;
    }

    public Event(String key, EventType type, JSONObject jsonValue, JwtSecurityContext ctx, String s) {
        this.userId = key;
        this.type = type;
        this.value = jsonValue;
        this.securityContext = ctx;
        this.returnTo = s;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public JSONObject getValue() {
        return value;
    }

    public void setValue(JSONObject value) {
        this.value = value;
    }

    public JwtSecurityContext getSecurityContext() {
        return securityContext;
    }

    public void setSecurityContext(JwtSecurityContext securityContext) {
        this.securityContext = securityContext;
    }

    public String getReturnTo() {
        return returnTo;
    }

    public void setReturnTo(String returnTo) {
        this.returnTo = returnTo;
    }
}
