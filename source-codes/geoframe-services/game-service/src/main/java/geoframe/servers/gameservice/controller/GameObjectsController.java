package geoframe.servers.gameservice.controller;

import geoframe.servers.gameservice.kafka.SimpleProducer;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Lucie Kucharova
 * This class handles game objects
 */

@Component
public class GameObjectsController {

    @Autowired
    SimpleProducer producer;

    public void deleteGameObjectsByGameId(Long gameId){
        JSONObject dataa = new JSONObject();
        dataa.put("gameId", gameId);
        JSONObject msg = new JSONObject();
        msg.put("data", dataa);
        msg.put("eventType", "DELETE_OBJECTS_BY_GAME");
        producer.sendToService("game_object_service", msg);
    }
}
