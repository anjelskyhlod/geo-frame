package geoframe.servers.gameservice.world;

import geoframe.servers.gameservice.connection.JsonSerializable;
import org.json.simple.JSONObject;

import javax.persistence.*;

/**
 * @author Lucie Kucharova
 * Representation of Game. Contains all game possibilities.
 */

@Entity
@Table(name = "game")
public class Game implements JsonSerializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "visibleRange")
    private Long visibleRange; //radius around player in which he sees game objects, in meters

    @Column(name = "characterNeeded")
    private boolean characterNeeded;

    @Column(name = "password")
    private String password;

    public Game() {
    }

    public Game(JSONObject json) {
        if (json.get("id") != null) {
            this.id = Long.parseLong(json.get("id").toString());
        }
        if (json.get("name") != null) {
            this.name = json.get("name").toString();
        }
        if (json.get("description") != null) {
            this.description = json.get("description").toString();
        }
        if (json.get("visibleRange") != null) {
            this.visibleRange = Long.parseLong(json.get("visibleRange").toString());
        }
        if (json.get("characterNeeded") != null) {
            this.characterNeeded = Boolean.parseBoolean(json.get("characterNeeded").toString());
        }
        if (json.get("password") != null) {
            this.password = json.get("password").toString();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVisibleRange() {
        return visibleRange;
    }

    public void setVisibleRange(Long visibleRange) {
        this.visibleRange = visibleRange;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isCharacterNeeded() {
        return characterNeeded;
    }

    public void setCharacterNeeded(boolean characterNeeded) {
        this.characterNeeded = characterNeeded;
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        obj.put("id", Long.toString(this.id));
        obj.put("name", this.name);
        obj.put("description", this.description);
        obj.put("visibleRange", this.visibleRange);
        obj.put("characterNeeded", this.characterNeeded);
        obj.put("password", this.password);
        return obj;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Game other = (Game) obj;
        if (id != other.id)
            return false;
        return true;
    }
}
