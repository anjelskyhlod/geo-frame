package geoframe.servers.gameservice.controller;

import geoframe.servers.gameservice.kafka.SimpleProducer;
import geoframe.servers.gameservice.security.auth.JwtSecurityContext;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Lucie Kucharova
 * This class handles user authorities
 */

@Component
public class AuthorizationController {

    @Autowired
    SimpleProducer producer;

    public boolean hasRole(JwtSecurityContext ctx, String role) {
        List<String> roles = ctx.getAuthentication().getPrincipal().getAuthorities();
        for (String auth : roles) {
            if (auth.equals(role)) {
                return true;
            }
        }
        return false;
    }

    public void addNewRoles(Long gameId, String username) {
        JSONObject dataa = new JSONObject();
        String newRole = "ROLE_PLAYER_" + gameId;
        dataa.put("role", newRole);
        dataa.put("username", username);
        JSONObject msg = new JSONObject();
        msg.put("data", dataa);
        msg.put("eventType", "SAVE_NEW_ROLES");
        producer.sendToService("authentication_service", msg);
    }

    public void deleteRoles(String username, String roleName) {
        JSONObject data = new JSONObject();
        data.put("role", roleName);
        data.put("username", username);
        JSONObject msg = new JSONObject();
        msg.put("data", data);
        msg.put("eventType", "DELETE_ROLES");
        producer.sendToService("authentication_service", msg);
    }
}
