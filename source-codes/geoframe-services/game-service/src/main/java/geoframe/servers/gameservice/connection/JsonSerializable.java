package geoframe.servers.gameservice.connection;

import org.json.simple.JSONObject;

public interface JsonSerializable {
    JSONObject toJson();
}
