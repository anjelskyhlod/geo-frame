package geoframe.servers.gameservice.controller;

import ch.qos.logback.core.encoder.EchoEncoder;
import geoframe.servers.gameservice.connection.*;
import geoframe.servers.gameservice.kafka.SimpleProducer;
import geoframe.servers.gameservice.security.TokenHelper;
import geoframe.servers.gameservice.security.auth.JwtSecurityContext;
import geoframe.servers.gameservice.service.GameService;
import geoframe.servers.gameservice.world.Game;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lucie Kucharova
 * This class handles actions related to @{@link Game}
 */

@Component
public class GameController {

    @Autowired
    GameService gameService;

    @Autowired
    SimpleProducer producer;

    @Autowired
    AuthorizationController authorizationController;

    @Autowired
    GameObjectsController gameObjectsController;

    @Autowired
    JsonHelper jsonHelper;

    @Autowired
    TokenHelper tokenHelper;

    /**
     * @param event
     * @return all games where given user has ownership rights
     */
    public EventResponse getGamesByUser(Event event) {
        EventResponse response = createEventResponse(EventType.SEND_GAMES_BY_USER);

        List<JsonSerializable> gamesOwned = new ArrayList<>();

        for (String role : event.getSecurityContext().getAuthentication().getPrincipal().getAuthorities()) {
            if (role.startsWith("ROLE_OWNER_")) {
                String gameId = role.substring(role.lastIndexOf('_') + 1);
                JsonSerializable game = gameService.findById(Long.valueOf(gameId));
                if (game != null)
                    gamesOwned.add(game);
            }
        }
        response.addData("games", gamesOwned);
        return response;
    }

    /**
     * @return Basic info (id, name, desc) of all games
     */
    public EventResponse getAllGamesBasicInfo() {
        EventResponse response = createEventResponse(EventType.SEND_ALL_GAMES_BASIC);

        List<Game> allGames = gameService.findAll();
        JSONArray gamesBasicInfo = new JSONArray();
        for (Game game : allGames) {
            JSONObject gameBasic = new JSONObject();
            gameBasic.put("id", game.getId());
            gameBasic.put("name", game.getName());
            gameBasic.put("description", game.getDescription());
            gamesBasicInfo.add(gameBasic);
        }
        response.addJsonArrayData("allGamesBasic", gamesBasicInfo);
        return response;
    }

    /**
     * Saves new game and invokes event sent to authentication
     * service to store new ownership right
     *
     * @param event
     * @return
     */
    public EventResponse saveNewGame(Event event) {
        EventResponse response = createEventResponse(EventType.SAVED_NEW_GAME);
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");
        String username = tokenHelper.getUsernameFromToken(event.
                getSecurityContext().getAuthentication().getToken());

        Game newGame = new Game(data);
        Game myGame = gameService.save(newGame);
        if (myGame.getId() != null) {
            JSONObject dataa = new JSONObject();
            String newRole = "ROLE_OWNER_" + newGame.getId();
            dataa.put("role", newRole);
            dataa.put("username", username);
            JSONObject msg = new JSONObject();
            msg.put("data", dataa);
            msg.put("eventType", "SAVE_NEW_ROLES");
            producer.sendToService("authentication_service", msg);
        }
        response.addData("game", myGame.toJson());
        response.addData("success", true);
        return response;
    }

    /**
     * Updates game if user has rights
     * @param event
     * @return Response success/failure (eventually
     * game data)
     */
    public EventResponse updateGame(Event event) {
        EventResponse response = createEventResponse(EventType.UPDATED_GAME);
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");
        Game myGame = new Game(data);
        if(authorizationController.hasRole(event.getSecurityContext(), "ROLE_OWNER_"+myGame.getId())) {
            try {
                gameService.update(myGame);
                response.addData("success", true);
                response.addData("game", myGame.toJson());
            } catch (Exception e) {
                response.addData("success", false);
            }
        } else {
            response.addData("success", false);
            response.addData("reason", "Not authorized");
        }
        return response;
    }

    /**
     * Deletes specified game, notifies auth service and
     * game object service to delete corresponding
     * roles/objects
     * @param event
     * @return
     */
    public EventResponse deleteGame(Event event) {
        EventResponse response = createEventResponse(EventType.DELETED_GAME);
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");
        Game myGame = new Game(data);
        if(authorizationController.hasRole(event.getSecurityContext(), "ROLE_OWNER_"+myGame.getId())) {
            try {
                response.addData("success", true);
                authorizationController.deleteRoles(tokenHelper.getUsernameFromToken(event.
                        getSecurityContext().getAuthentication().getToken()), "ROLE_OWNER_"+myGame.getId());
                gameObjectsController.deleteGameObjectsByGameId(myGame.getId());
                gameService.deleteById(myGame.getId());
            } catch (Exception e) {
                response.addData("success", false);
            }
        } else {
            response.addData("success", false);
            response.addData("reason", "Not authorized");
        }
        return response;
    }

    /**
     * Finds game by given ID, if player is connected for
     * the first time, event to auth service is invoked
     *
     * @param event
     * @return Success/failure response, eventually game info
     * if user has permission to fetch game
     */
    public EventResponse getgameById(Event event) {
        EventResponse response = createEventResponse(EventType.SEND_GAME_BY_ID);
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");

        Long id = jsonHelper.getLong(data, "gameId");

        if (!authorizationController.hasRole(event.getSecurityContext(), "ROLE_PLAYER_" + id.toString())) {
            registerNewPlayer(id, tokenHelper.getUsernameFromToken(
                    event.getSecurityContext().getAuthentication().getToken()));
        }

        try {
            Game game = gameService.findById(id);
            response.addData("game", game.toJson());
            response.addData("success", true);
        } catch (Exception e) {
            response.addData("success", false);
        }
        return response;
    }

//    ############################
//    ###### Helper methods ######
//    ############################

    private EventResponse createEventResponse(EventType eventType) {
        EventResponse response = new EventResponse();
        response.setEventType(eventType);
        return response;
    }

    /**
     * Sends event to auth service to store new
     * player role
     *
     * @param gameId
     * @param username
     */
    private void registerNewPlayer(Long gameId, String username) {
        authorizationController.addNewRoles(gameId, username);
        //check for character creation criteria
        JSONObject dataa = new JSONObject();
        dataa.put("gameId", gameId);
        dataa.put("username", username);
        JSONObject msg = new JSONObject();
        msg.put("data", dataa);
        msg.put("eventType", "SAVE_NEW_CHARACTER");
        producer.sendToService("SAVE_NEW_CHARACTER", msg);
    }
}
