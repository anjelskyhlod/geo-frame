package geoframe.servers.gameservice.dao;

import geoframe.servers.gameservice.world.Game;
import org.springframework.stereotype.Repository;

/**
 * @author Lucie Kucharova
 * DAO for @{@link Game}
 */

@Repository
public class GameDao extends AbstractDao<Game> {
    public GameDao() {
        super(Game.class);
    }
}
