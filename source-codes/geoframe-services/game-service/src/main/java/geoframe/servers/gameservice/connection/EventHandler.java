package geoframe.servers.gameservice.connection;

import geoframe.servers.gameservice.controller.AuthenticationController;
import geoframe.servers.gameservice.controller.GameController;
import geoframe.servers.gameservice.kafka.SimpleProducer;
import geoframe.servers.gameservice.security.TokenHelper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

/**
 * @author Lucie Kucharova
 * Mechanism responsible for navigating incoming @{@link Event} from broker
 * to right controller, which knows how to handle it.
 */

@Component
public class EventHandler {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    @Autowired
    SimpleProducer eventSender;

    @Autowired
    GameController gameController;

    @Autowired
    AuthenticationController authenticationController;

    @Autowired
    TokenHelper tokenHelper;


    public void process(Event event) {
        EventType eventType = event.getType();
        EventResponse response = null;
        switch (eventType) {
            case GET_GAMES_BY_USER:
                response = gameController.getGamesByUser(event);
                break;
            case GET_GAME_BY_ID:
                response = gameController.getgameById(event);
                break;
            case GET_ALL_GAMES_BASIC:
                response = gameController.getAllGamesBasicInfo();
                break;
            case SAVE_NEW_GAME:
                response = gameController.saveNewGame(event);
                break;
            case UPDATE_GAME:
                response = gameController.updateGame(event);
                break;
            case DELETE_GAME:
                response = gameController.deleteGame(event);
                break;
            case AUTH_TOKEN:
//                authenticationController.setAuthToken(body);
//                return;
//                break;
            default:
                LOGGER.warn("Process event: Unknown event Type.");
        }
        if (response != null) {
            eventSender.sendResponseMessage(getReturnTopic(event), response.toJson());
        }
    }

    private String getReturnTopic(Event event) {
        return event.getReturnTo() != null ? event.getReturnTo() : event.getUserId();
    }

}
