package geoframe.servers.gameservice.security.auth;

import geoframe.servers.gameservice.security.CustomUserDetails;

/**
 * @author Lucie Kucharova
 * Provides information about user's authentication
 */

public class TokenBasedAuthentication {

    private String token;
    private final CustomUserDetails principle;

    public TokenBasedAuthentication(CustomUserDetails principle) {
        this.principle = principle;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isAuthenticated() {
        return true;
    }

    public Object getCredentials() {
        return token;
    }

    public CustomUserDetails getPrincipal() {
        return principle;
    }

}
