package geoframe.servers.gameservice.service;

import geoframe.servers.gameservice.world.Game;

/**
 * @author Lucie Kucharova
 */

public interface GameService extends BaseService<Game> {
}
