package geoframe.servers.gameservice.service;

import geoframe.servers.gameservice.dao.GameDao;
import geoframe.servers.gameservice.world.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lucie Kucharova
 */

@Service
public class GameServiceImpl implements GameService {

    @Autowired
    GameDao dao;

    @Override
    public Game findById(Long id) {
        return dao.findById(id);
    }

    @Override
    public List<Game> findAll() {
        return dao.findAll();
    }

    @Override
    public Game save(Game entity) {
        return dao.save(entity);
    }

    @Override
    public void update(Game entity) {
        dao.update(entity);
    }

    @Override
    public void delete(Game entity) {
        dao.delete(entity);
    }

    @Override
    public void deleteById(Long entityId) {
        dao.deleteById(entityId);
    }
}
