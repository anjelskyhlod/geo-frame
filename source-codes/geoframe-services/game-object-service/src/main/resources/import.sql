INSERT INTO geo_point (id, lattitude, longitude) VALUES (1, 49.845526, 18.304798);
INSERT INTO geo_point (id, lattitude, longitude) VALUES (2, 49.845511, 18.30518);

INSERT INTO game_object (id, accessible_radius, can_move, description, game_id, hidden, movement_speed, name, value, location_id) VALUES(1, 100, false, 'Solve me for next clue', 1, false, NULL, 'Point1', NULL, 1);
INSERT INTO game_object (id, accessible_radius, can_move, description, game_id, hidden, movement_speed, name, value, location_id) VALUES(2, 100, false, 'You found me!', 1, true, NULL, 'Point2', NULL, 2);

INSERT INTO game_object_quests (game_object_id, quests) VALUES (1, 'QUEST_TYPE_RIDDLE_1');
