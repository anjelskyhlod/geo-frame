package geoframe.servers.gameobjectservice.service;

import geoframe.servers.gameobjectservice.world.PlayerObject;

/**
 * @author Lucie Kucharova
 */

public interface PlayerObjectService extends BaseService<PlayerObject> {
    PlayerObject findByUsernameAndGameId(String username, Long gameId);
}
