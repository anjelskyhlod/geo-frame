package geoframe.servers.gameobjectservice.connection;

/**
 * @author Lucie Kucharova
 * These are all types of events, which can be received or produced by
 * this service.
 */

public enum EventType {

    GET_OBJECTS_BY_GAME,
    SEND_OBJECTS_BY_GAME,
    DELETE_OBJECTS_BY_GAME,
    SAVE_OBJECT_BY_GAME,
    DELETE_OBJECT_BY_GAME,
    UPDATE_OBJECT_BY_GAME,

    GET_OBJECTS_BY_LOCATION,
    SEND_OBJECTS_BY_LOCATION,

    SAVED_GAME_OBJECT,
    DELETED_GAME_OBJECT,
    UPDATED_GAME_OBJECT,

    SAVE_NEW_CHARACTER,
    UPDATE_CHARACTER,
    GET_CHARACTER,
    SEND_CHARACTER,

    NEW_QUEST_ADDED,
    SAVED_NEW_QUEST,
    GET_ALL_REWARD_TYPES,
    SEND_ALL_REWARD_TYPES,

    AUTH_TOKEN
}
