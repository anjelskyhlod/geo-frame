package geoframe.servers.gameobjectservice.world;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import geoframe.servers.gameobjectservice.connection.JsonSerializable;
import org.json.simple.parser.JSONParser;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Lucie Kucharova
 * Representation of player and it's state
 */

@Entity
@Table(name = "player_object")
public class PlayerObject implements JsonSerializable {

    @Column
    private String username;
    @Column
    private Long gameId;

    @ElementCollection()
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<String> questsCompletedIds;

    @ElementCollection()
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Long> knownObjectsIds;

    @OneToMany(cascade = CascadeType.ALL,
            orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Reward> rewards;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;
    @Column
    private String description;

    //radius around object location from where is object accessible, in meters
    @Column
    private Double accessibleRadius;

    @OneToOne(cascade = CascadeType.ALL)
    private GeoPoint location;

    @ElementCollection(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<String> quests;

    @ElementCollection()
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<String> inventoryObjectIds;

    //price of object
    @Column
    private Double value;

    @Column
    private boolean canMove;

    @Column
    private Double movementSpeed;

    @Column
    private boolean hidden;

    /////

    public PlayerObject() {
        super();
    }

    public PlayerObject(String username, Long gameId) {
        this.username = username;
        this.gameId = gameId;
    }

    public PlayerObject(JSONObject json) {
        setAttributes(json);
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public List<String> getQuestsCompletedIds() {
        if (questsCompletedIds == null) questsCompletedIds = new ArrayList<>();
        return questsCompletedIds;
    }

    public void setQuestsCompletedIds(List<String> questsCompletedIds) {
        this.questsCompletedIds = questsCompletedIds;
    }

    public List<Long> getKnownObjectsIds() {
        if (knownObjectsIds == null) knownObjectsIds = new ArrayList<>();
        return knownObjectsIds;
    }

    public void setKnownObjectsIds(List<Long> knownObjectsIds) {
        this.knownObjectsIds = knownObjectsIds;
    }

    public List<Reward> getRewards() {
        if (rewards == null) rewards = new ArrayList<>();
        return rewards;
    }

    public void setRewards(List<Reward> rewards) {
        this.rewards = rewards;
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        if (username != null)
            obj.put("username", username);
        if (gameId != null)
            obj.put("gameId", this.gameId);
        if (questsCompletedIds != null)
            obj.put("questsCompletedIds", this.questsCompletedIds);
        if (knownObjectsIds != null)
            obj.put("knownObjectsIds", this.knownObjectsIds);
        if (rewards != null) {
            JSONArray arr = new JSONArray();
            for (Reward reward : this.rewards) {
                arr.add(reward.toJson());
            }
            obj.put("rewards", arr);
        }

        ///
        if (id != null)
            obj.put("id", Long.toString(this.id));
        if (name != null)
            obj.put("name", this.name);
        if (description != null)
            obj.put("description", this.description);
        if (location != null)
            obj.put("location", this.location.toJson());
        if (quests != null)
            obj.put("quests", this.quests);
        if (accessibleRadius != null)
            obj.put("accessibleRadius", this.accessibleRadius);
        if (inventoryObjectIds != null)
            obj.put("inventoryObjectIds", this.inventoryObjectIds);
        obj.put("canMove", this.canMove);
        if (value != null)
            obj.put("value", this.value);
        obj.put("hidden", this.hidden);

        ///
        return obj;
    }

    protected void setAttributes(JSONObject data) {
        if (data.get("username") != null) {
            setUsername(data.get("username").toString());
        }
        if (data.get("gameId") != null)
            setGameId(Long.parseLong(data.get("gameId").toString()));
        if (data.get("questsCompletedIds") != null)
            setQuestsCompletedIds((List<String>) data.get("questsCompletedIds"));
        if (data.get("questId") != null) {
            if (this.getQuestsCompletedIds() == null) this.setQuestsCompletedIds(new ArrayList<>());
            this.questsCompletedIds.add(data.get("questId").toString());
        }
        if (data.get("knownObjectsIds") != null)
            setKnownObjectsIds((List<Long>) data.get("knownObjectsIds"));
        if (data.get("rewards") != null) {

            JSONArray arr = (JSONArray) data.get("rewards");
            for (int i = 0; i < arr.size(); i++) {
                Reward reward = new Reward((JSONObject) arr.get(i));
                if (this.rewards == null) this.rewards = new ArrayList<>();
                this.rewards.add(reward);
            }
        }
        ///
        if (data.get("id") != null) {
            setId(Long.parseLong(data.get("id").toString()));
        }
        if (data.get("description") != null)
            setDescription((String) data.get("description"));
        if (data.get("name") != null)
            setName((String) data.get("name"));
        if (data.get("gameId") != null)
            setGameId(Long.parseLong(data.get("gameId").toString()));
        if (data.get("location") != null)
            setLocation(new GeoPoint((JSONObject) data.get("location")));
        if (data.get("quests") != null)
            setQuests((List<String>) data.get("quests"));
        if (data.get("accessibleRadius") != null)
            setAccessibleRadius((Double) data.get("accessibleRadius"));
        if (data.get("value") != null)
            setValue((Double) data.get("value"));
        if (data.get("canMove") != null)
            setCanMove((boolean) data.get("canMove"));
        if (data.get("inventoryObjectIds") != null)
            setInventoryObjectIds((List<String>) data.get("inventoryObjectIds"));
        if (data.get("hidden") != null)
            setHidden((boolean) data.get("hidden"));
        ///
    }

    /////////

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAccessibleRadius() {
        return accessibleRadius;
    }

    public void setAccessibleRadius(Double accessibleRadius) {
        this.accessibleRadius = accessibleRadius;
    }

    public GeoPoint getLocation() {
        return location;
    }

    public void setLocation(GeoPoint location) {
        this.location = location;
    }

    public List<String> getQuests() {
        return quests;
    }

    public void setQuests(List<String> quests) {
        this.quests = quests;
    }

    public List<String> getInventoryObjectIds() {
        return inventoryObjectIds;
    }

    public void setInventoryObjectIds(List<String> inventoryObjectIds) {
        this.inventoryObjectIds = inventoryObjectIds;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public boolean isCanMove() {
        return canMove;
    }

    public void setCanMove(boolean canMove) {
        this.canMove = canMove;
    }

    public Double getMovementSpeed() {
        return movementSpeed;
    }

    public void setMovementSpeed(Double movementSpeed) {
        this.movementSpeed = movementSpeed;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }
}
