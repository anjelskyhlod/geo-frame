package geoframe.servers.gameobjectservice.service;

import geoframe.servers.gameobjectservice.dao.PlayerObjectDao;
import geoframe.servers.gameobjectservice.world.PlayerObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lucie Kucharova
 */


@Service
public class PlayerObjectServiceImpl implements PlayerObjectService {

    @Autowired
    PlayerObjectDao dao;

    @Override
    public PlayerObject findById(Long id) {
        return dao.findById(id);
    }

    @Override
    public List<PlayerObject> findAll() {
        return dao.findAll();
    }

    @Override
    public PlayerObject save(PlayerObject entity) {
        return dao.save(entity);
    }

    @Override
    public void update(PlayerObject entity) {
        dao.update(entity);
    }

    @Override
    public void delete(PlayerObject entity) {
        dao.delete(entity);
    }

    @Override
    public void deleteById(Long entityId) {
        dao.deleteById(entityId);
    }

    @Override
    public PlayerObject findByUsernameAndGameId(String username, Long gameId) {
        return dao.findBy(username, gameId);
    }
}
