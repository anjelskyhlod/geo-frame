package geoframe.servers.gameobjectservice.world;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.persistence.*;

/**
 * @author Lucie Kucharova
 * Representation of quest reward
 */

@Entity
@Table(name = "reward")
public class Reward {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private RewardType rewardType;
    @Transient
    private JSONObject rewardData;
    @Column
    private String rewardDataString;

    public Reward() {
    }

    ;

    public Reward(JSONObject json) {
        if (json.get("rewardType") != null) {
            this.rewardType = createRewardType(json.get("rewardType").toString());
        }
        this.rewardData = json;
        rewardDataString = json.toJSONString();
    }

    private RewardType createRewardType(String value) {
        RewardType rewardType = null;
        for (RewardType type : RewardType.values()) {
            if (type.name().equals(value)) {
                rewardType = type;
                break;
            }
        }
        return rewardType;
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        if (rewardType != null) {
            json.put("rewardType", rewardType.toString());
        }
        if (rewardData != null) {
            json.put("rewardData", rewardData);
        }
        return json;
    }


    public RewardType getRewardType() {
        return rewardType;
    }

    public void setRewardType(RewardType rewardType) {
        this.rewardType = rewardType;
    }

    public JSONObject getRewardData() {
        if (rewardData == null) {
            JSONParser parser = new JSONParser();
            try {
                rewardData = (JSONObject) parser.parse(rewardDataString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return rewardData;
    }

    public void setRewardData(JSONObject rewardData) {
        this.rewardData = rewardData;
        this.rewardDataString = rewardData.toJSONString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
