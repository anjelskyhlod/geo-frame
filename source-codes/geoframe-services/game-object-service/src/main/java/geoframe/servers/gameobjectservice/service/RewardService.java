package geoframe.servers.gameobjectservice.service;

import geoframe.servers.gameobjectservice.world.Reward;

/**
 * @author Lucie Kucharova
 */


public interface RewardService extends BaseService<Reward> {
}
