package geoframe.servers.gameobjectservice.dao;

import geoframe.servers.gameobjectservice.world.Reward;
import org.springframework.stereotype.Repository;

/**
 * @author Lucie Kucharova
 * DAO for @{@link Reward}
 */

@Repository
public class RewardDao extends AbstractDao<Reward> {
    public RewardDao() {
        super(Reward.class);
    }
}
