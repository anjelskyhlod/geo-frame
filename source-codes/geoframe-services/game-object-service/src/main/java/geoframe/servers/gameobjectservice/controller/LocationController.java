package geoframe.servers.gameobjectservice.controller;

import geoframe.servers.gameobjectservice.connection.EventResponse;
import geoframe.servers.gameobjectservice.kafka.SimpleProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class LocationController {

    @Autowired
    SimpleProducer producer;

    public void objectChanged(EventResponse response){
        producer.sendResponseMessage(response.getEventType().toString(), response.toJson());
    }
}
