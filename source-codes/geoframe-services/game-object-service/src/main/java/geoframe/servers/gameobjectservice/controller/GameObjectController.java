package geoframe.servers.gameobjectservice.controller;

import geoframe.servers.gameobjectservice.connection.*;
import geoframe.servers.gameobjectservice.security.auth.JwtSecurityContext;
import geoframe.servers.gameobjectservice.service.GameObjectService;
import geoframe.servers.gameobjectservice.service.PlayerObjectService;
import geoframe.servers.gameobjectservice.world.GameObject;
import geoframe.servers.gameobjectservice.world.GeoPoint;
import geoframe.servers.gameobjectservice.world.PlayerObject;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lucie Kucharova
 * This class handles actions related to @{@link GameObject}
 */

@Component
public class GameObjectController {

    @Autowired
    GameObjectService gameObjectService;

    @Autowired
    PlayerObjectService playerObjectService;

    @Autowired
    RewardController rewardController;

    @Autowired
    LocationController locationController;

    @Autowired
    JsonHelper jsonHelper;

    /**
     * Finds game objects which belongs to certain game
     *
     * @param event
     * @return Success/failure info, game objects data
     */
    public EventResponse getGameObjectsByGame(Event event) {
        EventResponse response = createEventResponse(EventType.SEND_OBJECTS_BY_GAME);
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");

        Long id = jsonHelper.getLong(data, "gameId");

        List<GameObject> gameObjects = new ArrayList<>();
        if (hasRole(event.getSecurityContext(), "ROLE_OWNER_" + id)) {
            gameObjects = gameObjectService.findAllByGameId(id);
        }

        response.addData("gameObjects", gameObjects);
        return response;
    }

    /**
     * Saves object for certain game if user has authority
     * to do so
     *
     * @param event
     * @return Success/failure info
     */
    public EventResponse saveObjectByGame(Event event) {
        EventResponse response = createEventResponse(EventType.SAVED_GAME_OBJECT);
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");

        GameObject go = new GameObject(jsonHelper.getJsonObject(data, "gameObject"));
        go.setId(null);
        if (hasRole(event.getSecurityContext(), "ROLE_OWNER_" + go.getGameId())) {
            go = gameObjectService.save(go);
            response.addData("success", true);
            response.addData("gameObject", go.toJson());
            if (!go.getHidden()) {
                locationController.objectChanged(response);
            }
            return response;
        }
        response.addData("success", false);
        response.addData("reason", "Not authorized.");
        return response;
    }

    /**
     * Deletes object for certain game if user has authority
     * to do so
     *
     * @param event
     * @return Success/failure info
     */
    public EventResponse deleteObjectByName(Event event) {
        EventResponse response = createEventResponse(EventType.DELETED_GAME_OBJECT);
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");

        GameObject go = new GameObject(jsonHelper.getJsonObject(data, "gameObject"));
        if (hasRole(event.getSecurityContext(), "ROLE_OWNER_" + go.getGameId())) {
            gameObjectService.deleteById(go.getId());
            response.addData("success", true);
            response.addData("gameObjectId", go.getId());
            response.addData("gameId", go.getGameId());
            locationController.objectChanged(response);
            return response;
        }
        response.addData("success", false);
        response.addData("reason", "Not authorized.");
        return response;
    }

    /**
     * Deletes all objects which belongs to certain game
     *
     * @param event
     */
    public void deleteObjectsByGame(Event event) {
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");
        Long gameId = jsonHelper.getLong(data, "gameId");

        gameObjectService.deleteAllByGameId(gameId);
    }

    /**
     * Updates object for certain game if user has authority
     * to do so
     *
     * @param event
     * @return Success/failure info
     */
    public EventResponse updateObjectByGame(Event event) {
        EventResponse response = createEventResponse(EventType.UPDATED_GAME_OBJECT);
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");

        GameObject go = new GameObject(jsonHelper.getJsonObject(data, "gameObject"));
        if (go.getId() == null) {
            response.addData("success", false);
            response.addData("reason", "No such object.");
            return response;
        }
        GameObject goOld = gameObjectService.findById(go.getId());
        goOld.copyAttributes(go);

        if (hasRole(event.getSecurityContext(), "ROLE_OWNER_" + goOld.getGameId())) {
            gameObjectService.update(goOld);
            response.addData("success", true);
            response.addData("gameObject", goOld.toJson());
            if (!goOld.getHidden()) {
                locationController.objectChanged(response);
            }
            return response;
        }
        response.addData("success", false);
        response.addData("reason", "Not authorized.");
        return response;
    }

    /**
     * This function loads objects by given area specifications and filters them by
     * player's game state
     *
     * @param event
     * @return Response with filtered game objects
     */
    public EventResponse getGameObjectsByLocation(Event event) {
        EventResponse response = createEventResponse(EventType.SEND_OBJECTS_BY_LOCATION);
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");

        // determine position of player
        GeoPoint center = new GeoPoint(jsonHelper.getDouble(data, "lattitude"),
                jsonHelper.getDouble(data, "longitude"));
        // determine area to cover by objects
        Long range = jsonHelper.getLong(data, "visibleRange");

        // determine objects already loaded in user application
        List<Long> omitObjectsIds = jsonHelper.getLongList(data, "existing");

        Long gameId = jsonHelper.getLong(data, "gameId");
        String userId = jsonHelper.getString(data, "userId");
        PlayerObject player = null;
        try {
            player = playerObjectService.findByUsernameAndGameId(userId, gameId);
        } catch (EmptyResultDataAccessException ex) {
            saveNewPlayerObject(gameId, userId);
        }
        player = playerObjectService.findByUsernameAndGameId(userId, gameId);

        // load game objects
        List<GameObject> gameObjects = getGameObjects(range, center, gameId);
        // filter objects by player state and already loaded objects
        List<JsonSerializable> filteredObjects = filterByPlayerState(gameObjects, omitObjectsIds, player);
        //include also player's character
        filteredObjects.add(player);

        response.addData("gameObjects", filteredObjects);
        response.addData("userId", userId);
        response.addData("gameId", gameId);
        return response;
    }


    /**
     * Saves new player object (if it doesn't exist already)
     *
     * @param event
     * @return Response with player's data
     */
    public EventResponse saveNewPlayerObject(Event event) {
        EventResponse response = createEventResponse(EventType.SEND_CHARACTER);
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");

        String username = jsonHelper.getString(data, "username");
        Long gameId = jsonHelper.getLong(data, "gameId");

        if (username != null && gameId != null) {
            try {
                playerObjectService.findByUsernameAndGameId(username, gameId);
            } catch (EmptyResultDataAccessException e) {
                saveNewPlayerObject(gameId, username);
            }
        }
        event.setReturnTo(username);
        response.addData("player", playerObjectService.findByUsernameAndGameId(username, gameId).toJson());
        return response;
    }

    private void saveNewPlayerObject(Long gameId, String username) {
        PlayerObject player = new PlayerObject(username, gameId);
        playerObjectService.save(player);
    }

    /**
     * Updates player state after user action and adds rewards, if
     * there are any
     *
     * @param event
     */
    public void updatePlayerObject(Event event) {
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");

        PlayerObject updatedPlayer = new PlayerObject(data);
        PlayerObject oldPlayer = playerObjectService.findByUsernameAndGameId(updatedPlayer.getUsername(),
                updatedPlayer.getGameId());
        for (String id : updatedPlayer.getQuestsCompletedIds()) {
            oldPlayer.getQuestsCompletedIds().add(id);
        }
        for (Long id : updatedPlayer.getKnownObjectsIds()) {
            oldPlayer.getKnownObjectsIds().add(id);
        }
        rewardController.resolveReward(oldPlayer, data);
        playerObjectService.update(oldPlayer);
    }

    /**
     * Adds new quest id to game object
     *
     * @param event
     * @return Response info
     */
    public EventResponse addNewQuest(Event event) {
        EventResponse response = createEventResponse(EventType.SAVED_NEW_QUEST);
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");

        String questId = jsonHelper.getString(data, "questId");
        Long gameObjectId = jsonHelper.getLong(data, "objectId");

        GameObject object = gameObjectService.findById(gameObjectId);
        if (!object.getQuests().contains(questId))
            object.getQuests().add(questId);
        gameObjectService.update(object);

        return response;
    }

    /**
     * Finds player object by given username and game id
     *
     * @param event
     * @return Response with player data
     */
    public EventResponse getPlayerObject(Event event) {
        EventResponse response = createEventResponse(EventType.SEND_CHARACTER);
        JSONObject data = jsonHelper.getJsonObject(event.getValue(), "data");

        String username = jsonHelper.getString(data, "username");
        Long gameId = jsonHelper.getLong(data, "gameId");

        PlayerObject player = playerObjectService.findByUsernameAndGameId(username, gameId);
        response.addData("player", player.toJson());
        return response;
    }

//    ############################
//    ###### Helper methods ######
//    ############################

    private EventResponse createEventResponse(EventType eventType) {
        EventResponse response = new EventResponse();
        response.setEventType(eventType);
        return response;
    }

    private boolean hasRole(JwtSecurityContext ctx, String role) {
        List<String> roles = ctx.getAuthentication().getPrincipal().getAuthorities();
        for (String auth : roles) {
            if (auth.equals(role)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Finds game objects by provided area restrictions
     *
     * @param range
     * @param center
     * @param gameId
     * @return found objects
     */
    private List<GameObject> getGameObjects(Long range, GeoPoint center, Long gameId) {
        List<GameObject> gameObjects = new ArrayList<>();
        if (range != null) {
            List<GameObject> allGameObjects = gameObjectService.findAllByRange(center, range, gameId);
            List<GameObject> filteredGameObjects = new ArrayList<>();
            // distance filter
            for (GameObject gameObject : allGameObjects) {
                double distance = distFrom(center.getLattitude(), center.getLongitude(),
                        gameObject.getLocation().getLattitude(), gameObject.getLocation().getLongitude());
                if (distance <= range) {
                    filteredGameObjects.add(gameObject);
                }
            }
            gameObjects = filteredGameObjects;
        } else {
            gameObjects = gameObjectService.findAllByGameId(gameId);
        }
        return gameObjects;
    }

    /**
     * Filteres game objects by provided player state
     *
     * @param gameObjects
     * @param omitObjectsIds
     * @param player
     * @return filtered objects
     */
    private List<JsonSerializable> filterByPlayerState(List<GameObject> gameObjects, List<Long> omitObjectsIds,
                                                       PlayerObject player) {
        List<JsonSerializable> filteredObjects = new ArrayList<>();
        for (GameObject obj : gameObjects) {
            //hidden objects are revealed on e.g. quest completion
            //some quests was already sent to client
            if ((!obj.getHidden() || rewardController.areQuestsSolved(player, obj)) && !omitObjectsIds.contains(obj.getId())) {
                filteredObjects.add(obj);
            }
        }
        return filteredObjects;
    }

    /**
     * Calculates distance between two GPS points
     *
     * @param lat1
     * @param lng1
     * @param lat2
     * @param lng2
     * @return Distance in meters
     */
    private static double distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 3958.75; // miles (or 6371.0 kilometers)
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;
        return dist * 1000;
    }
}
