package geoframe.servers.gameobjectservice.dao;

import geoframe.servers.gameobjectservice.world.PlayerObject;
import org.springframework.stereotype.Repository;

/**
 * @author Lucie Kucharova
 * DAO for @{@link PlayerObject}
 */

@Repository
public class PlayerObjectDao extends AbstractDao<PlayerObject> {
    public PlayerObjectDao() {
        super(PlayerObject.class);
    }

    public PlayerObject findBy(String username, Long gameId) {
        return (PlayerObject) em.createQuery("SELECT g FROM " + clazz.getName() + " g WHERE g.gameId=" + gameId + " AND g.username='" +
                username + "'").getSingleResult();
    }
}
