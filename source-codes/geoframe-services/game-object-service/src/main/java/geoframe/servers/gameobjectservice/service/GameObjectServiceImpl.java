package geoframe.servers.gameobjectservice.service;

import geoframe.servers.gameobjectservice.dao.GameObjectDao;
import geoframe.servers.gameobjectservice.world.GameObject;
import geoframe.servers.gameobjectservice.world.GeoPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lucie Kucharova
 */

@Service
public class GameObjectServiceImpl implements GameObjectService {

    @Autowired
    GameObjectDao dao;

    @Override
    public GameObject findById(Long id) {
        return dao.findById(id);
    }

    @Override
    public List<GameObject> findAll() {
        return dao.findAll();
    }

    @Override
    public GameObject save(GameObject entity) {
        return dao.save(entity);
    }

    @Override
    public void update(GameObject entity) {
        dao.update(entity);
    }

    @Override
    public void delete(GameObject entity) {
        dao.delete(entity);
    }

    @Override
    public void deleteById(Long entityId) {
        dao.deleteById(entityId);
    }

    @Override
    public List<GameObject> findAllByGameId(Long gameId) {
        return dao.findAllByGameId(gameId);
    }

    @Override
    public List<GameObject> findAllByRange(GeoPoint center, Long range, Long gameId) {
        return dao.findAllByRange(center, range, gameId);
    }

    @Override
    public void deleteAllByGameId(Long gameId) {
        dao.deleteAllByGameId(gameId);
    }
}
