package geoframe.servers.gameobjectservice.dao;

import geoframe.servers.gameobjectservice.world.GameObject;
import geoframe.servers.gameobjectservice.world.GeoPoint;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author Lucie Kucharova
 * DAO for @{@link GameObject}
 */

@Repository
public class GameObjectDao extends AbstractDao<GameObject> {
    public GameObjectDao() {
        super(GameObject.class);
    }

    public List<GameObject> findAllByGameId(Long gameId) {
        return em.createQuery("SELECT g FROM GameObject g WHERE g.gameId=" + gameId).getResultList();
    }

    // Add function to db to calculate range
    public List<GameObject> findAllByRange(GeoPoint center, Long range, Long gameId) {
        return findAllByGameId(gameId);
    }

    @Transactional
    public void deleteAllByGameId(Long gameId) {
        em.createQuery("DELETE FROM GameObject g WHERE g.gameId=" + gameId).executeUpdate();
    }
}
