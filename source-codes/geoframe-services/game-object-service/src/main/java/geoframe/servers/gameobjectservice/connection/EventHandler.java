package geoframe.servers.gameobjectservice.connection;

import geoframe.servers.gameobjectservice.controller.AuthenticationController;
import geoframe.servers.gameobjectservice.controller.GameObjectController;
import geoframe.servers.gameobjectservice.controller.RewardController;
import geoframe.servers.gameobjectservice.kafka.SimpleProducer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Lucie Kucharova
 * Mechanism responsible for navigating incoming @{@link Event} from broker
 * to right controller, which knows how to handle it.
 */

@Component
public class EventHandler {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    @Autowired
    SimpleProducer eventSender;

    @Autowired
    GameObjectController gameObjectController;

    @Autowired
    AuthenticationController authenticationController;

    @Autowired
    RewardController rewardController;

    public void process(Event event) {
        EventType eventType = event.getType();
        EventResponse response = null;
        switch (eventType) {
            case GET_OBJECTS_BY_GAME:
                response = gameObjectController.getGameObjectsByGame(event);
                break;
            case DELETE_OBJECTS_BY_GAME:
                gameObjectController.deleteObjectsByGame(event);
                break;
            case SAVE_OBJECT_BY_GAME:
                response = gameObjectController.saveObjectByGame(event);
                break;
            case DELETE_OBJECT_BY_GAME:
                response = gameObjectController.deleteObjectByName(event);
                break;
            case UPDATE_OBJECT_BY_GAME:
                response = gameObjectController.updateObjectByGame(event);
                break;
            case GET_OBJECTS_BY_LOCATION:
                response = gameObjectController.getGameObjectsByLocation(event);
                break;
            case SAVE_NEW_CHARACTER:
                response = gameObjectController.saveNewPlayerObject(event);
                break;
            case GET_CHARACTER:
                response = gameObjectController.getPlayerObject(event);
                break;
            case UPDATE_CHARACTER:
                gameObjectController.updatePlayerObject(event);
                break;
            case NEW_QUEST_ADDED:
                response = gameObjectController.addNewQuest(event);
                break;
            case GET_ALL_REWARD_TYPES:
                response = rewardController.getAllRewardTypes();
                break;
            case AUTH_TOKEN:
//                authenticationController.setAuthToken(body);
//                return;
//                break;
            default:
                LOGGER.warn("Process event: Unknown event Type.");
        }
        if (response != null) {
            eventSender.sendResponseMessage(getReturnTopic(event), response.toJson());
        }
    }

    private String getReturnTopic(Event event) {
        return event.getReturnTo() != null ? event.getReturnTo() : event.getUserId();
    }


}
