package geoframe.servers.gameobjectservice.controller;

import geoframe.servers.gameobjectservice.connection.EventResponse;
import geoframe.servers.gameobjectservice.connection.EventType;
import geoframe.servers.gameobjectservice.connection.JsonHelper;
import geoframe.servers.gameobjectservice.kafka.SimpleProducer;
import geoframe.servers.gameobjectservice.service.GameObjectService;
import geoframe.servers.gameobjectservice.service.PlayerObjectService;
import geoframe.servers.gameobjectservice.service.RewardService;
import geoframe.servers.gameobjectservice.world.GameObject;
import geoframe.servers.gameobjectservice.world.PlayerObject;
import geoframe.servers.gameobjectservice.world.Reward;
import geoframe.servers.gameobjectservice.world.RewardType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Lucie Kucharova
 * This class handles actions related to @{@link Reward}
 */

@Component
public class RewardController {

    protected final Log LOGGER = LogFactory.getLog(getClass());

    @Autowired
    SimpleProducer simpleProducer;

    @Autowired
    GameObjectService gameObjectService;

    @Autowired
    PlayerObjectService playerObjectService;

    @Autowired
    RewardService rewardService;

    @Autowired
    JsonHelper jsonHelper;

    /**
     * Checks player reward type and invokes right handling method
     *
     * @param player
     * @param data
     */
    public void resolveReward(PlayerObject player, JSONObject data) {
        String rewardStr = jsonHelper.getString(data, "reward");
        if(rewardStr != null) {
            JSONObject rewardJson = jsonHelper.createJsonFromString(rewardStr);
            Reward reward = new Reward(rewardJson);
            switch (reward.getRewardType()) {
                case UNCOVER:
                    uncoverThing(player, reward);
                    break;
                default:
                    LOGGER.warn("Unknown reward type.");
                    break;
            }
        }
    }

    /**
     * Loads object which has to be uncovered and updates player state
     *
     * @param player
     * @param reward
     */
    public void uncoverThing(PlayerObject player, Reward reward) {
        if (reward.getRewardData() == null) {
            LOGGER.info("Nothing to uncover");
        }
        Long hiddenObjectId = jsonHelper.getLong(reward.getRewardData(), "gameObjectId");

        GameObject hiddenGameObject = gameObjectService.findById(hiddenObjectId);

        JSONObject msg = new JSONObject();
        msg.put("data", hiddenGameObject.toJson());
        msg.put("eventType", "OBJECT_UNCOVERED");
        simpleProducer.sendResponseMessage(player.getUsername(), msg);
        rewardService.save(reward);
        player.getRewards().add(reward);
    }

    /**
     * Finds all possible reward types
     *
     * @return All reward types
     */
    public EventResponse getAllRewardTypes() {
        EventResponse response = new EventResponse();
        response.setEventType(EventType.SEND_ALL_REWARD_TYPES);
        JSONArray arr = new JSONArray();
        for (RewardType type : RewardType.values()) {
            arr.add(type.toString());
        }
        response.addJsonArrayData("rewardTypes", arr);
        return response;
    }

    /**
     * Checks if player already solved quests bound
     * to given object
     *
     * @param player
     * @param obj
     * @return true if solved, false if not
     */
    public boolean areQuestsSolved(PlayerObject player, GameObject obj) {
        for (Reward reward : player.getRewards()) {
            if (reward.getRewardType().equals(RewardType.UNCOVER)) {
                if (Long.parseLong(reward.getRewardData().get("gameObjectId").toString()) == obj.getId()) {
                    return true;
                }
            }
        }
        return false;
    }
}
