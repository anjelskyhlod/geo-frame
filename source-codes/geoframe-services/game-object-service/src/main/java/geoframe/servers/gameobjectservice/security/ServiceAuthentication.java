package geoframe.servers.gameobjectservice.security;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author Lucie Kucharova
 * Provides service authentication
 */

@Component
@Scope("singleton")
public class ServiceAuthentication {

    private String authToken;
    private final String SERVICE_NAME = "game_service";
    private final String SERVICE_PASS = "pass";

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getSERVICE_NAME() {
        return SERVICE_NAME;
    }
}
