package geoframe.servers.gameobjectservice.service;

import geoframe.servers.gameobjectservice.dao.RewardDao;
import geoframe.servers.gameobjectservice.world.Reward;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lucie Kucharova
 */


@Service
public class RewardServiceImpl implements RewardService {

    @Autowired
    RewardDao rewardDao;

    @Override
    public Reward findById(Long id) {
        return rewardDao.findById(id);
    }

    @Override
    public List<Reward> findAll() {
        return rewardDao.findAll();
    }

    @Override
    public Reward save(Reward entity) {
        return rewardDao.save(entity);
    }

    @Override
    public void update(Reward entity) {
        rewardDao.update(entity);
    }

    @Override
    public void delete(Reward entity) {
        rewardDao.delete(entity);
    }

    @Override
    public void deleteById(Long entityId) {
        rewardDao.deleteById(entityId);
    }
}
