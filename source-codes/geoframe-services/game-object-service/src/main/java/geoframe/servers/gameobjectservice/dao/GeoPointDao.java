package geoframe.servers.gameobjectservice.dao;

import org.springframework.stereotype.Repository;
import geoframe.servers.gameobjectservice.world.GeoPoint;

/**
 * @author Lucie Kucharova
 * DAO for @{@link GeoPoint}
 */

@Repository
public class GeoPointDao extends AbstractDao<GeoPoint> {
    public GeoPointDao() {
        super(GeoPoint.class);
    }
}
