package geoframe.servers.gameobjectservice.service;

import geoframe.servers.gameobjectservice.world.GameObject;
import geoframe.servers.gameobjectservice.world.GeoPoint;

import java.util.List;

/**
 * @author Lucie Kucharova
 */

public interface GameObjectService extends BaseService<GameObject> {
    List<GameObject> findAllByGameId(Long gameId);

    List<GameObject> findAllByRange(GeoPoint center, Long range, Long gameId);

    void deleteAllByGameId(Long gameId);
}
