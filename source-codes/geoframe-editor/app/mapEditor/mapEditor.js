'use strict';

/**
 * @author Lucie Kucharova
 * Core functionalities providing manipulation with map
 */

angular.module('GeoFrame.mapEditor', ['ngRoute', 'leaflet-directive', 'ngFileUpload'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/mapEditor', {
            templateUrl: 'mapEditor/mapEditor.html',
            controller: 'MarkerController'
        });
    }])
    .controller('MarkerController', ['$scope', '$rootScope', 'GameObjectsService', 'GameService', 'CommonService',
        'LeafletControlBtnFactory', 'AuthService', '$location', '$q', 'EventService', '$timeout', 'QuestService',
        'Notification', 'WebSocketService',
        function ($scope, $rootScope, GameObjectsService, GameService, CommonService, LeafletControlBtnFactory,
                  AuthService, $location, $q, EventService, $timeout, QuestService, Notification, WebSocketService) {
            console.log("creating MarkerController");
            $scope.isAuthenticated = function () {
                var oldToken = AuthService.getJwtToken();
                if (oldToken !== null) {
                    var data = AuthService.createAuthorizationTokenHeader();
                    EventService.fire("GET_REFRESH", angular.extend(data, {"eventType": "GET_REFRESH"}));
                }
            };
            $rootScope.authenticated = false;
            $scope.isAuthenticated();

            delete L.Icon.Default.prototype._getIconUrl;
            L.Icon.Default.mergeOptions({
                iconRetinaUrl: 'bower_components/leaflet/dist/images/marker-icon-2x.png',
                iconUrl: 'bower_components/leaflet/dist/images/marker-icon.png',
                shadowUrl: 'bower_components/leaflet/dist/images/marker-shadow.png',
            });
            $scope.markers = new Array();
            $scope.controls = {
                custom: []
            };
            $scope.controls.custom.push(LeafletControlBtnFactory.getLeafletControlBtn('topright', 'map-icon-menu',
                'sideMenuToggle', $scope));
            $scope.controls.custom.push(LeafletControlBtnFactory.getLeafletControlBtn('topright', 'map-icon-login',
                'userProfileModal', $scope));

            angular.extend($scope, {
                ostrava: {
                    lat: 49.835556,
                    lng: 18.292778,
                    zoom: 11,
                    markers: {}
                },
                events: {},
                defaults: {
                    zoomControlPosition: 'topright'
                }
            });

            $scope.model = {};
            $scope.model.allGames = {};
            $scope.model.currentGame = {};
            $scope.model.currentGameObject = {quests: [], questObjs: []};
            $scope.model.currentGameObjects = [];
            $scope.chooseIcon = false;
            $scope.questFormOpen = false;

            $scope.addMarker = function (gameObj) {
                if (!(gameObj.location)) {
                    return;
                }
                gameObj.location.lattitude = parseFloat(gameObj.location.lattitude);
                gameObj.location.longitude = parseFloat(gameObj.location.longitude);
                $scope.markers.push({
                    lat: gameObj.location.lattitude,
                    lng: gameObj.location.longitude,
                    message: "<div ng-include=\"'mapEditor/actionMenu/gameObjects/gameObject.html'\"></div>",
                    getMessageScope: function () {
                        return $scope;
                    },
                    draggable: gameObj.draggable,
                    focus: gameObj.focus,
                    title: gameObj.id
                })
            };

            $scope.removeMarkers = function () {
                $scope.markers = new Array();
            };

            $scope.saveNewGameObject = function () {
                $scope.model.currentGameObject.gameId = $scope.model.currentGame.id;
                delete $scope.model.currentGameObject.id;
                GameObjectsService.saveGameObject($scope.model.currentGameObject)
            };

            $scope.updateGameObject = function () {
                $scope.model.currentGameObject.gameId = $scope.model.currentGame.id;
                $scope.model.currentGameObject.id;
                if (angular.isDefined($scope.model.currentGameObject)) {
                    GameObjectsService.updateGameObject($scope.model.currentGameObject)
                }
            };

            $scope.deleteGameObject = function () {
                if ($scope.model.currentGameObject.id == "new") {
                    $scope.removeFromArray($scope.model.currentGameObjects, $scope.model.currentGameObject);
                    $scope.removeFromArray($scope.markers, $scope.getMarkerByGameObjectById($scope.model.currentGameObject.id));
                    return;
                }
                $scope.model.currentGameObject.gameId = $scope.model.currentGame.id;
                if (angular.isDefined($scope.model.currentGameObject)) {
                    GameObjectsService.deleteGameObject($scope.model.currentGameObject);
                }
            };

            $scope.reloadGame = function () {
                $scope.model.currentGameObject = {quests: [], questObjs: []};
                $scope.removeMarkers();
            };

            $scope.getGameObjectById = function (id) {
                for (var i = 0; i < $scope.model.currentGameObjects.length; i++) {
                    if ($scope.model.currentGameObjects[i].id == id) {
                        return $scope.model.currentGameObjects[i];
                    }
                }
            };

            $scope.removeFromArray = function (arr, obj) {
                var index = arr.indexOf(obj);
                if (index > -1) {
                    arr.splice(index, 1);
                }
            };

            $scope.getMarkerByGameObjectById = function (id) {
                for (var i = 0; i < $scope.markers.length; i++) {
                    if ($scope.markers[i].title == id) {
                        return $scope.markers[i];
                    }
                }
            };

            $scope.reloadMarkers = function () {
                console.log("REMOVING markers");
                $scope.removeMarkers();
                if ($scope.model.currentGameObjects.length > 0) {
                    for (var i = 0; i < $scope.model.currentGameObjects.length; i++) {
                        $scope.addMarker($scope.model.currentGameObjects[i]);
                    }
                } else {
                    console.log("no objects");
                }
                console.log("RELOADED markers");
            };

            $scope.updateGameObjectPosition = function (gameObject, newLatitude, newLongitude) {
                gameObject.location.lattitude = newLatitude;
                gameObject.location.longitude = newLongitude;
                var mapPoint = $scope.getMarkerByGameObjectById(gameObject.id);
                mapPoint.lat = newLatitude;
                mapPoint.lng = newLongitude;
            };

            $scope.loadQuestsByIds = function () {
                QuestService.loadQuestsByIds($scope.model.currentGameObject.quests);
                QuestService.getAllRewardTypes();
            };

            $scope.logout = function () {
                AuthService.removeJwtToken();
                AuthService.removeUsername();
                $rootScope.authenticated = false;
                Notification.info('Logged out.');
                $scope.removeMarkers();
                WebSocketService.closeConnection();
            };

            $scope.$on("leafletDirectiveMap.click", function (event, args) {
                var leafEvent = args.leafletEvent;
                console.log(args);
                console.log("CLICKED");
                if ($rootScope.authenticated == false) {
                    Notification.primary("Log in, please.");
                    return;
                }
                if (angular.isUndefined(CommonService.getCurrentGame()) ||
                    CommonService.getCurrentGame() == null) {
                    Notification.primary("Please select or create game.");
                    return;
                }
                var notSavedNewObject = $scope.getGameObjectById("new");
                if (notSavedNewObject) {
                    $scope.updateGameObjectPosition(notSavedNewObject, leafEvent.latlng.lat, leafEvent.latlng.lng);
                    return;
                }
                var newObj = {
                    location: {
                        lattitude: leafEvent.latlng.lat,
                        longitude: leafEvent.latlng.lng
                    },
                    focus: true,
                    draggable: true,
                    id: "new",
                }
                $scope.model.currentGameObject = newObj;
                $scope.model.currentGameObjects.push(newObj);
                CommonService.setCurrentGameObject($scope.model.currentGameObject);
                $scope.addMarker(newObj);
            });

            $scope.$on("leafletDirectiveMarker.dragend", function (event, args) {
                var leafObject = args.leafletObject;
                $scope.model.currentGameObject.location.lattitude = leafObject._latlng.lat;
                $scope.model.currentGameObject.location.longitude = leafObject._latlng.lng;
            });

            $scope.$on("leafletDirectiveMarker.click", function (event, args) {
                var leafObject = args.leafletObject;
                $scope.model.currentGameObject = $scope.getGameObjectById(leafObject.options.title);
                $scope.model.currentGameObject.id = leafObject.options.title;
                CommonService.setCurrentGameObject($scope.model.currentGameObject);
            });

            $scope.$watch('model.currentGame', function (newValue, oldValue, scope) {
                if (newValue != oldValue && newValue) {
                    CommonService.setCurrentGame($scope.model.currentGame);
                    GameObjectsService.getAllGameObjectsByGameId($scope.model.currentGame.id);
                    $scope.reloadGame();
                }
                else {
                    CommonService.setCurrentGame(null);
                }
            });

            $scope.$watch(function () {
                return CommonService.getAllGamesByUser();
            }, function (newVal, oldVal) {
                if (angular.isDefined(newVal)) {
                    $scope.model.allGames = newVal;
                }
            });

            $scope.$watch(function () {
                return CommonService.getAllGameObjects();
            }, function (newVal, oldVal) {
                if (angular.isDefined(newVal)) {
                    $scope.model.currentGameObjects = newVal;
                    $scope.removeMarkers();
                    $timeout(function () {
                        $scope.reloadMarkers();
                        if (angular.isDefined($scope.currentGameObject)) {
                            var id = $scope.currentGameObject.id;
                            $scope.currentGameObject = {quests: [], questObjs: []};
                        }
                        QuestService.getAllQuestTypes();
                    }, 100);
                }
            });

            $scope.$watch(function () {
                return CommonService.getAllQuestIdsByGameObject();
            }, function (newVal, oldVal) {
                if (angular.isDefined(newVal)) {
                    $scope.model.currentGameObject.quests = newVal;
                }
            }, true);

            $scope.$watch(function () {
                return CommonService.getAllQuestObjsByGameObject();
            }, function (newVal, oldVal) {
                if (angular.isDefined(newVal)) {
                    $scope.model.currentGameObject.questsObjs = newVal;
                }
            }, true);
        }]);