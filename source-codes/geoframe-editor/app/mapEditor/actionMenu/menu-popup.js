'use strict';
angular.module('GeoFrame.mapEditor')
    .controller('MenuPopupController', ['$scope', function ($scope) {
        $scope.menuVisible = true;
        $scope.showNewObjectForm = false;
        $scope.addNewGameObject = function () {
            $scope.menuVisible = false;
            $scope.showNewObjectForm = true;
        }
    }]);