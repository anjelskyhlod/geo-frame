'use strict';
/**
 * @author Lucie Kucharova
 * Main module
 */

angular.module('GeoFrame', [
  'ngRoute',
  'ngResource',
  'ngWebSocket',
  'GeoFrame.mapEditor',
  'GeoFrame.version',
    'ui-notification',
]).
config(['$locationProvider',
    '$routeProvider',
    function($locationProvider,
             $routeProvider) {
  $locationProvider.hashPrefix('!');
  $routeProvider.otherwise({redirectTo: '/mapEditor'});
}])



