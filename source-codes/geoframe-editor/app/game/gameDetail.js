angular.module('GeoFrame')
/**
 * @author Lucie Kucharova
 * Handling game details
 */
    .controller('GameCtrl', ['$scope', '$rootScope', 'GameService', 'CommonService',
        function ($scope, $rootScope, GameService, CommonService) {
            console.log("Creating LoginCtrl");

            $scope.error = false;
            $scope.currentWorldBackup = {};
            $scope.newWorld = false;

            $scope.model.game = {};

            $scope.newGame = function () {
                $scope.newWorld = true;
                $scope.currentWorldBackup = angular.copy($scope.model.game);
                $scope.model.game = {};
            };

            $scope.saveNewGame = function () {
                console.log($scope.model.game);
                GameService.saveNewGame($scope.model.game);
                $scope.newWorld = false;
                $scope.model.game = {};
                $('#game-modal').modal('hide');
            };

            $scope.discardNewGame = function () {
                $scope.newWorld = false;
                $scope.model.game = angular.copy($scope.currentWorldBackup);
                $scope.currentWorldBackup = {};
            };

            $scope.deleteGame = function () {
                GameService.deleteGame($scope.model.game);
            };

            $scope.updateGame = function () {
                GameService.updateGame($scope.model.game);
            };

            $('#renderDistance').on('hidden.bs.collapse', function () {
                $('#visibleRangeInput').val('');
                delete $scope.model.game.visibleRange;
            });

            $('#gamePassword').on('hidden.bs.collapse', function () {
                $('#passwordInput').val('');
                delete $scope.model.game.password;
            });

            $scope.$watch('model.currentGame', function (newValue, oldValue, scope) {
                $scope.newWorld = false;
                $scope.model.game = newValue;
            });
        }]);
