'use strict';

angular.module('GeoFrame.version', [
  'GeoFrame.version.interpolate-filter',
  'GeoFrame.version.version-directive'
])

.value('version', '0.1');
