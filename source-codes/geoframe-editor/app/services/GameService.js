angular.module('GeoFrame')

    .service('GameService', ['EventService', function (EventService) {

        this.saveNewGame = function (game) {
            EventService.fire("SAVE_NEW_GAME", angular.extend(game, {"evenType": "SAVE_NEW_GAME"}));
        };

        this.deleteGame = function (game) {
            EventService.fire("DELETE_GAME", angular.extend(game, {"evenType": "DELETE_GAME"}));
        };

        this.updateGame = function (game) {
            EventService.fire("UPDATE_GAME", angular.extend(game, {"evenType": "UPDATE_GAME"}));
        };

    }]);