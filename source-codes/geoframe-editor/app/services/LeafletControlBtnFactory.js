/**
 * @author Lucie Kucharova
 * LeafletControlBtnFactory is a helper component
 * providing easy button addition to current map,
 * specified by button template
 */

angular.module('GeoFrame')
    .factory('LeafletControlBtnFactory', ['$compile',
        function ($compile) {

            var LeafletControl = {}

            LeafletControl.getLeafletControlBtn = function (position, clazz, template, scope) {
                var MyControl = new L.control();
                MyControl.setPosition(position);
                MyControl.onAdd = function () {
                    var container = L.DomUtil.create('div', clazz + ' leaflet-bar');
                    var controlIncl = L.DomUtil.create('div', "", container);
                    controlIncl.setAttribute("ng-include", "'mapEditor/mapControlButtons/" + template + ".html'");
                    L.DomEvent.disableClickPropagation(container);
                    $("." + clazz).html($compile(controlIncl)(scope));
                    return container;
                };
                return MyControl;
            }

            return LeafletControl;


        }]);