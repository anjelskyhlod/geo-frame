
angular.module('GeoFrame')

.service('GameObjectsService', ['EventService',
    function(EventService) {

    this.getAllGameObjectsByGameId = function (gameId) {
        EventService.fire("GET_OBJECTS_BY_GAME", {gameId: gameId, questType: "GET_OBJECTS_BY_GAME"});
    };

    this.saveGameObject = function (gameObject) {
        EventService.fire("SAVE_OBJECT_BY_GAME", {gameObject: gameObject, questType: "SAVE_OBJECT_BY_GAME"});
    };

    this.updateGameObject = function (gameObject) {
        EventService.fire("UPDATE_OBJECT_BY_GAME", {gameObject: gameObject, questType: "UPDATE_OBJECT_BY_GAME"});
    };

    this.deleteGameObject = function (gameObject) {
        EventService.fire("DELETE_OBJECT_BY_GAME", {gameObject: gameObject, questType: "DELETE_OBJECT_BY_GAME"});
    }
}]);