/**
 * @author Lucie Kucharova
 * EventHandler - mechanism responsible for navigating incoming
 * messages to right handler method, new data id stored in
 * CommonService.
 */

angular.module('GeoFrame')
    .service('EventHandler', ['AuthService', '$rootScope', 'CommonService', 'Notification',
        function (AuthService, $rootScope, CommonService, Notification) {
            $rootScope.newEvents = [];

            this.process = function (eventType, response) {
                console.log("PROCESS INCOMING EVENT");
                console.log(eventType);
                console.log(response);
                switch (eventType) {
                    case 'SEND_LOGIN':
                        showLoginResult(response);
                        break;
                    case 'AUTH_TOKEN':
                        console.log(response);
                        $rootScope.authenticated = angular.isDefined(response.access_token) &&
                            response.access_token.length > 0;
                        if ($rootScope.authenticated) {
                            storeToken(response);
                            getUserInfo(response);
                        }
                        break;
                    case 'SINGLE_USER':
                        storeUser(response);
                        getUserGames(response);
                        break;
                    case 'REGISTERED':
                        showRegistrationResult(response);
                        break;
                    case 'SEND_GAMES_BY_USER':
                        setAllGamesByUser(response);
                        break;
                    case 'SAVED_NEW_GAME':
                        addNewGame(response);
                        break;
                    case 'DELETED_GAME':
                        refreshGames(response);
                        break;
                    case 'SEND_OBJECTS_BY_GAME':
                        setAllGameObjects(response);
                        break;
                    case 'SAVED_GAME_OBJECT':
                        refreshAllObjects();
                        break;
                    case 'UPDATED_GAME_OBJECT':
                        refreshAllObjects();
                        break;
                    case 'DELETED_GAME_OBJECT':
                        refreshAllObjects();
                        break;
                    case 'SAVED_NEW_QUEST':
                        refreshAllObjects();
                        break;
                    case 'SEND_ALL_REWARD_TYPES':
                        setAllRewardTypes(response);
                        break;
                    default:
                }
                if (eventType.startsWith('QUEST_TYPE_') && eventType.endsWith('_BY_IDS')) {
                    console.log("EventHandler QUEST_TYPE__BY_IDS");
                    addQuestsByIds(response);
                    return;
                }
                else if (eventType.startsWith('QUEST_TYPE_') && !eventType.endsWith('_BY_IDS')) {
                    console.log("EventHandler QUEST_TYPE_");
                    console.log(eventType);
                    response.questType.questType = eventType;
                    addNewQuestType(response);
                }
                else if (eventType.startsWith('ALL_QUEST_TYPE_')) {
                    console.log("EventHandler ALL_QUEST_TYPE_");
                    setAllQuestsByQuestType(response);
                }
                else if (eventType.startsWith('SAVED_QUEST_TYPE_')) {
                    console.log("EventHandler SAVED_QUEST_TYPE_");
                    setSavedQuest(response);
                }
            };

            var storeToken = function (response) {
                console.log("SETTING TOKEN");
                AuthService.setJwtToken(response.access_token);

            };

            var getUserInfo = function (response) {
                $rootScope.newEvents.push({
                    "eventType":
                        "GET_SINGLE_USER",
                    "data": {
                        "eventType": "GET_SINGLE_USER"
                    }
                });
            };

            var storeUser = function (response) {
                $rootScope.user = {};
                $rootScope.user = response.user;
            };

            var getUserGames = function (response) {
                $rootScope.newEvents.push("GET_GAMES_BY_USER", {"eventType": "GET_GAMES_BY_USER"});
            };

            var showRegistrationResult = function (response) {
                CommonService.setRegistrationResult(response);
                console.log("REmovin usrname");
                AuthService.removeUsername();
            };

            var showLoginResult = function (response) {
                if (angular.isDefined(response)) {
                    if ((response.success)) {
                        Notification.success("Log in success.");
                    } else if (!response.success) {
                        Notification.error("Log in was not successful. Reason: " + response.reason);
                    }
                }
            };

            var setAllGamesByUser = function (response) {
                CommonService.setAllGamesByUser(response.games);
            };

            var setAllGameObjects = function (response) {
                CommonService.setAllGameObjects(response.gameObjects);
            };

            var refreshAllObjects = function () {
                var data = {
                    gameId: CommonService.getCurrentGame().id,
                    eventType: "GET_OBJECTS_BY_GAME"
                };
                $rootScope.newEvents.push({eventType: "GET_OBJECTS_BY_GAME", "data": data});
            };

            var setCurrentQuest = function (response) {
                CommonService.setCurrentQuestId(response.quest.id) //TODO find quest
            };

            var addNewQuestType = function (response) {
                CommonService.addNewQuestType(response.questType);
            };

            var setAllQuestsByQuestType = function (response) {
                CommonService.setAllQuestsByChosenType(response.allQuests);
            };

            var addQuestsByIds = function (response) {
                CommonService.addQuestsByIds(response.quests);
            };

            var setAllRewardTypes = function (response) {
                CommonService.setAllRewardTypes(response.rewardTypes);
            };

            var setSavedQuest = function (response) {
                CommonService.setQuestToCurrentNewObject(response);
            };

            var addNewGame = function (response) {
                if ((response.success)) {
                    Notification.success("New game added.");
                    var data = {};
                    data[CommonService.getAllGamesByUser().length] = response.game;
                    angular.extend(CommonService.getAllGamesByUser(),
                        data);
                    var data = AuthService.createAuthorizationTokenHeader();
                    $rootScope.newEvents.push("GET_REFRESH", angular.extend(data, {"eventType": "GET_REFRESH"}));
                } else if (!response.success) {
                    Notification.error("Cannot be saved.");
                }
            };

            var refreshGames = function (response) {
                if (response.success) {
                    Notification.success("Game deleted.");
                    $rootScope.newEvents.push("GET_GAMES_BY_USER", {"eventType": "GET_GAMES_BY_USER"});
                } else {
                    Notification.error("Cannot be deleted.");
                }
            }
        }]);