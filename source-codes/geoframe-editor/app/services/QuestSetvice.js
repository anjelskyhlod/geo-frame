angular.module('GeoFrame')
    .service('QuestService', ['EventService',
        function (EventService) {

            this.getAllQuestTypes = function () {
                EventService.fire("GET_AVAILABLE_QUEST_TYPES", {eventType: "GET_AVAILABLE_QUEST_TYPES"});
            };

            this.getAllQuestsByChosenType = function (questType) {
                EventService.fire("GET_ALL_" + questType,
                    {eventType: "GET_ALL_" + questType});
            };

            this.loadQuestsByIds = function (quests) {
                console.log("LOADING QUESTS");
                console.log(quests);
                EventService.fire("GET_QUESTS_BY_IDS", {
                    "questIds": quests,
                    "eventType": "GET_QUESTS_BY_IDS"
                });
            };

            this.getAllRewardTypes = function () {
                EventService.fire("GET_ALL_REWARD_TYPES", {"eventType": "GET_ALL_REWARD_TYPES"});
            };


        }]);