/**
 * @author Lucie Kucharova
 * WebSocketService is an endpoint for communication with
 * the rest of the system, connects to websocket-kafka proxy.
 * Websocket is opened according to user's username for
 * identification among all kafka topics.
 */

angular.module('GeoFrame')
    .service('WebSocketService', ['$websocket', 'EventHandler', 'AuthService', 'CommonService', '$timeout',
        '$q',
        function ($websocket, EventHandler, AuthService, CommonService, $timeout, $q) {
            var dataStream;
            var myTopic;
            var wsService = this;

            this.openConnection = function (topic, deferred) {
                myTopic = topic + "_WEB_EDITOR";
                // var query = 'ws://172.21.0.14:7081/v2/broker/?topics=' + myTopic;
                var query = 'ws://0.0.0.0:7081/v2/broker/?topics=' + myTopic;
                dataStream = $websocket(query, null, {reconnectIfNotNormalClose: true});
                var collection = [];
                dataStream.onMessage(function (message) {
                    var parsedMessage = JSON.parse(message.data);
                    var dataMessage = JSON.parse(parsedMessage.message);
                    if (angular.isDefined(dataMessage.data)) {
                        EventHandler.process(dataMessage.eventType, dataMessage.data);
                    }
                    else {
                        EventHandler.process(dataMessage.eventType, dataMessage);
                    }
                });

                dataStream.onOpen(function () {
                    console.log("CONNECTION OPENED");
                    deferred.resolve();
                });

                dataStream.onMessage(function () {
                    console.log("CONNECTION MESSAGE");
                });

                dataStream.onClose(function () {
                    console.log("CONNECTION CLOSE");
                });

                dataStream.onError(function () {
                    console.log("CONNECTION ERROR");
                });

                var methods = {
                    collection: collection,
                    get: function () {
                        dataStream.send(JSON.stringify({action: 'get'}));
                    }
                };
                return methods;
            };

            this.sendMessage = function (topic, message) {
                angular.extend(message, {
                    "returnTo": myTopic
                });
                angular.extend(message, AuthService.createAuthorizationTokenHeader());
                if (angular.isDefined(dataStream)) {
                    dataStream.send({"topic": topic, "message": message});
                } else {
                    console.log("WS ERROR - Undefined data stream.")
                }
            };

            /**
             * Connection has to be checked in case of change
             * of users - every user has to have it's own
             * websocket connection.
             * @param deferred
             */
            this.checkConnection = function (deferred) {
                deferred = $q.defer();
                var waitDefer = $q.defer();
                waitForClose(waitDefer).then(function () {
                    if (angular.isUndefined(dataStream) || (dataStream.readyState != 1 && dataStream.readyState != 2)) {
                        var topic = AuthService.getUsername();
                        if (angular.isDefined(topic) && topic != null && topic.length > 0) {
                            wsService.openConnection(topic, deferred);
                        }
                    }
                    else {
                        deferred.resolve();
                    }
                });
                return deferred.promise;
            };

            this.closeConnection = function () {
                dataStream.socket.close(1000, "Client closed socket");
                myTopic = "";
            };

            var waitForClose = function (waitDefer) {
                var topic = AuthService.getUsername();
                var reconnect = false;
                if (angular.isDefined(myTopic) && myTopic.length > 0) {
                    if (!myTopic.startsWith(topic)) {
                        reconnect = true;
                    }
                }
                console.log(topic);
                if (angular.isDefined(dataStream)) {
                    if (dataStream.readyState == dataStream._readyStateConstants.CLOSING) {
                        $timeout(function () {
                            return waitForClose(waitDefer);
                        }, 500);
                    } else if (dataStream.readyState == dataStream._readyStateConstants.OPEN &&
                        (angular.isUndefined(topic) || topic == null || topic.length <= 0 ||
                            reconnect == true)) {
                        wsService.closeConnection();
                        $timeout(function () {
                            return waitForClose(waitDefer);
                        }, 500);
                    } else {
                        waitDefer.resolve();
                    }
                } else {
                    waitDefer.resolve();
                }
                return waitDefer.promise;
            };
        }]);