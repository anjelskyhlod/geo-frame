/**
 * @author Lucie Kucharova
 * CommonService is used primary for storing newly
 * fetched data from EventHandler. Controllers of
 * certain UI have defined watchers of this data,
 * which are activated on data change and can update
 * user view.
 */

angular.module('GeoFrame')
    .service('CommonService', [function () {
        var allGamesByUser = {};
        var allGameObjects = [];
        var allQuestTypes = [];
        var allQuestsByChosenType = [];
        var allRewardTypes = [];

        var currentGameObject = {quests: [], questObjs: []};
        var currentGame = null;
        var currentQuest = null;

        var registrationResult = {};
        // var loginAttemptUsername = {};

        this.setRegistrationResult = function (result) {
            registrationResult = result;
        };

        this.getRegistrationResult = function () {
            return registrationResult;
        };

        this.getCurrentGameObject = function () {
            return currentGameObject;
        };
        this.setCurrentGameObject = function (gameObject) {
            currentGameObject = gameObject;
        };

        this.getCurrentGame = function () {
            return currentGame;
        };
        this.setCurrentGame = function (game) {
            currentGame = game;
        };

        this.getAllGamesByUser = function () {
            return allGamesByUser;
        };
        this.setAllGamesByUser = function (games) {
            allGamesByUser = games;
        };

        this.getAllGameObjects = function () {
            return allGameObjects;
        };
        this.setAllGameObjects = function (objects) {
            allGameObjects = objects;
        };

        this.getCurrentQuest = function () {
            return currentQuest;
        };
        this.setCurrentQuest = function (quest) {
            currentQuest = quest;
        };

        this.addNewQuestType = function (questType) {
            console.log("ADDING NEW quest type");
            var alreadyHave = false;
            for (var i = 0; i < allQuestTypes.length; i++) {
                if (questType.id == allQuestTypes[i].id) {
                    console.log("quest type je uz tu");
                    allQuestTypes[i] = questType;
                    alreadyHave = true;
                }
            }
            if (!alreadyHave) {
                console.log("push quest type");
                allQuestTypes.push(questType);
            }
        };

        this.getAllQuestTypes = function () {
            return allQuestTypes;
        };

        this.getAllQuestsByChosenType = function () {
            return allQuestsByChosenType;
        };

        this.setAllQuestsByChosenType = function (quests) {
            allQuestsByChosenType = quests;
        };

        this.addQuestsByIds = function (quests) {
            console.log(quests);
            console.log(currentGameObject);
            if (angular.isDefined(currentGameObject)) {
                if (angular.isUndefined(currentGameObject.questObjs)) {
                    currentGameObject.questObjs = [];
                }

                for (var i = 0; i < quests.length; i++) {
                    var alreadyHave = false;
                    for (var j = 0; j < currentGameObject.quests.length; j++) {
                        if (angular.equals(currentGameObject.quests[j], quests[i].id)) {
                            for (var k = 0; k < currentGameObject.questObjs.length; k++) {
                                if (angular.equals(currentGameObject.questObjs[k].id, quests[i].id)) {
                                    alreadyHave = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (!alreadyHave) {
                        currentGameObject.questObjs.push(quests[i]);
                    }
                }
            }
        };
        this.getAllQuestIdsByGameObject = function (quests) {
            return currentGameObject.quests;
        };
        this.getAllQuestObjsByGameObject = function (quests) {
            return currentGameObject.questObjs;
        };

        this.getAllRewardTypes = function () {
            return allRewardTypes;
        };

        this.setAllRewardTypes = function (data) {
            allRewardTypes = data;
        };

        this.setQuestToCurrentNewObject = function (data) {
            if (angular.isUndefined(currentGameObject.quests)) currentGameObject.quests = [];
            if (angular.isUndefined(currentGameObject.questObjs)) currentGameObject.questObjs = [];
            currentGameObject.quests.push(data.questId);
            var questInfo = data.quest;
            questInfo.id = data.questId;
            currentGameObject.questObjs.push(questInfo);
        };
    }]);