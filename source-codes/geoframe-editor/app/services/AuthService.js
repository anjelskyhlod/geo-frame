/**
 * @author Lucie Kucharova
 * Auth service is responsible for handling
 * token and username storage needed for
 * authentication and connection
 */

angular.module('GeoFrame')
    .service('AuthService', [function () {
        var TOKEN_KEY = 'jwtToken';
        var USERNAME = 'username';

        this.getJwtToken = function () {
            return localStorage.getItem(TOKEN_KEY);
        };

        this.setJwtToken = function (token) {
            localStorage.setItem(TOKEN_KEY, token);
        };

        this.setUsername = function (username) {
            localStorage.setItem(USERNAME, username);
        };

        this.getUsername = function (username) {
            return localStorage.getItem(USERNAME, username);
        };

        this.removeUsername = function () {
            localStorage.removeItem(USERNAME);
        }

        this.removeJwtToken = function () {
            localStorage.removeItem(TOKEN_KEY);
        };

        this.createAuthorizationTokenHeader = function () {
            var token = this.getJwtToken();
            if (token) {
                return {
                    "Authorization": "Bearer " + token,
                    'Content-Type': 'application/json'
                };
            } else {
                return {
                    'Content-Type': 'application/json'
                };
            }
        }
    }]);