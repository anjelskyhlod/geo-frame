/**
 * @author Lucie Kucharova
 * EventService is used as helper service for emitting
 * new data to WebSocket connection.
 */

angular.module('GeoFrame')
    .service('EventService', ['$resource', '$q', 'WebSocketService', '$rootScope',
        function ($resource, $q, WebSocketService, $rootScope) {
            var EventService = this;

            this.fire = function (eventType, data) {
                console.log(eventType);
                var deferred = $q.defer();
                WebSocketService.checkConnection(deferred).then(function () {
                    data = resolveEventFormat(eventType, data);
                    WebSocketService.sendMessage(eventType, data);
                });
            };

            var resolveEventFormat = function (eventType, data) {
                data = {"data": data};
                if (angular.isUndefined(data.eventType)) {
                    data.eventType = eventType;
                }
                return data;
            };

            $rootScope.$watch('newEvents', function (newValue, oldValue, fire) {
                for (var i = 0; i < $rootScope.newEvents.length; i++) {
                    EventService.fire($rootScope.newEvents[i].eventType, $rootScope.newEvents[i].data);
                    $rootScope.newEvents.splice(i, 1);
                }
            }, true);

        }]);