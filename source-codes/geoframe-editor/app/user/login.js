/**
 * @author Lucie Kucharova
 * LoginCtrl handles all login actions.
 */

angular.module('GeoFrame')
    .controller('LoginCtrl', ['$scope', '$rootScope', '$http', '$location', 'AuthService', 'EventService',
        'CommonService', 'Notification',
        function ($scope, $rootScope, $http, $location, AuthService, EventService, CommonService,
                  Notification) {
            console.log("Creating LoginCtrl");

            $scope.error = false;
            $rootScope.selectedTab = $location.path() || '/';

            $scope.registration = false;

            $scope.model.credentials = {
                username: "marvin1",
                password: "123"
            };

            $scope.login = function () {
                console.log($scope.model.credentials);
                AuthService.setUsername($scope.model.credentials.username);
                EventService.fire("GET_LOGIN", $scope.model.credentials);
            };

            $scope.register = function () {
                console.log($scope.credentials);
                AuthService.setUsername($scope.model.credentials.username);
                EventService.fire("REGISTER", $scope.model.credentials);
            };

            $scope.$watch(function () {
                return CommonService.getRegistrationResult();
            }, function (newVal, oldVal) {
                if (angular.isDefined(newVal) && angular.isDefined(newVal.success)) {
                    console.log("Registration result here");
                    console.log(newVal);
                    $scope.registration = false;
                    if (newVal.success) {
                        Notification.success("Registered successfully. You may log in.");
                        $scope.model.credentials = {};
                    }
                    else if (!newVal.success){
                        Notification.error("Registration was not successful. Reason: " + newVal.reason);
                        $scope.model.credentials = {};
                    }
                    else{
                        console.log("pruser")
                    }
                }
            }, true);
        }])
;
