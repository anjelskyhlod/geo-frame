/**
 * @author Lucie Kucharova
 * QuestsCtrl is responsible for
 * handling quest actions
 */

angular.module('GeoFrame')
    .controller('QuestsCtrl', ['$scope', '$rootScope', 'EventService', 'CommonService', 'QuestService',
        'GameObjectsService',
        function ($scope, $rootScope, EventService, CommonService, QuestService, GameObjectsService) {
            console.log("Creating QuestCtrl");

            $scope.error = false;
            $scope.new = true;

            $scope.model.allQuestTypes = [];
            $scope.model.allRewardTypes = [];
            $scope.model.allQuestsByChosenType = [];
            $scope.model.chosenQuestType = {};
            $scope.model.currentQuest = {reward: {}};
            $scope.isQuestTypeRiddle = false;

            $scope.setNew = function (isNew) {
                $scope.new = isNew;
            };

            $scope.saveNewQuest = function (questType) {
                $('#quest-editor-modal').modal('hide');
                angular.extend($scope.model.currentQuest, {
                    eventType: "SAVE_" + questType
                });
                delete $scope.model.currentQuest.id;
                if(angular.isUndefined($scope.model.currentQuest.reward) ||
                    angular.isUndefined($scope.model.currentQuest.reward.gameObjectId)){
                    delete $scope.model.currentQuest.reward;
                }
                console.log($scope.model.currentQuest);
                EventService.fire("SAVE_" + questType, {
                    quest: $scope.model.currentQuest,
                    objectId: $scope.model.currentGameObject.id
                });
                // GameObjectsService.updateGameObject($scope.model.currentQuest);
            };

            $scope.removeQuest = function(item){
                console.log(item);
                var gameObj = CommonService.getCurrentGameObject();
                for(var i = 0; i < gameObj.quests.length; i++){
                    if(angular.equals(gameObj.quests[i], item.id)){
                        console.log("tebe smazu!");
                        gameObj.quests.splice(gameObj.quests.indexOf(item.id), 1);
                        GameObjectsService.updateGameObject(gameObj);
                        for(var j = 0; j < gameObj.questObjs.length; j++){
                            if(angular.equals(gameObj.questObjs[j].id, item.id)){
                                gameObj.questObjs.splice(gameObj.questObjs.indexOf(gameObj.questObjs[j]), 1);
                                break;
                            }
                        }
                    }
                }
            };

            $scope.getAllQuestTypes = function () {
                QuestService.getAllQuestTypes();
            };

            $scope.getAllQuestByChosenType = function () {
                QuestService.getAllQuestsByChosenType($scope.model.chosenQuestType.questType);
            };

            $scope.getCurrentObjectQuests = function () {
                return $scope.model.currentGameObject.questObjs;
            };

            $scope.resolveRewardType = function () {
                if (angular.isDefined($scope.model.currentQuest) && angular.isDefined($scope.model.currentQuest.reward)) {
                    var stringReward = $scope.model.currentQuest.reward;
                    stringReward = stringReward.replace(new RegExp("'", 'g'), '"');
                    try {
                        var jsonReward = JSON.parse(stringReward);
                        $scope.model.currentQuest.reward = jsonReward;
                        $scope.setInitRewardType();
                    } catch (e) {
                        $scope.model.currentQuest.reward = {};
                    }
                }
            };

            $scope.setInitRewardType = function () {
                for (var i = 0; i < $scope.model.allRewardTypes.length; i++) {
                    console.log($scope.model.currentQuest.reward.rewardType);
                    console.log($scope.model.allRewardTypes[i]);
                    if (angular.equals($scope.model.currentQuest.reward.rewardType,
                            $scope.model.allRewardTypes[i])) {
                        $scope.model.currentQuest.reward.rewardType = $scope.model.allRewardTypes[i];
                    }
                    // if($scope.model.currentQuest.reward.length > 0){
                    //     console.log("EXPANDING");
                    //     $('#rewardForm').trigger('expand');
                    // } else{
                    //     $('#rewardForm').trigger('collapse');
                    // }
                }
            };

            $scope.$watch('model.chosenQuestType', function (newValue, oldValue, scope) {
                console.log("quest type chosen");
                if (newValue != oldValue && !angular.isUndefined(newValue) &&
                    newValue != null) {
                    if (angular.equals(newValue.questType, "QUEST_TYPE_RIDDLE")) {
                        $scope.isQuestTypeRiddle = true;
                    }
                    if ($scope.model.chosenQuestType) {
                        $scope.model.currentQuest = $scope.new ? angular.copy($scope.model.chosenQuestType) : {};
                    }
                    else {
                        $scope.model.currentQuest = {};
                    }
                    if ($scope.new) {
                        $scope.getAllQuestByChosenType();
                    }
                }
            });

            $scope.$watch('model.currentQuest', function (newValue, oldValue, scope) {
                $scope.resolveRewardType();
                CommonService.setCurrentQuest($scope.model.currentQuest);
            });

            $scope.$watch(function () {
                return CommonService.getAllQuestTypes();
            }, function (newVal, oldVal) {
                if (angular.isDefined(newVal)) {
                    $scope.model.allQuestTypes = newVal;
                }
            }, true);

            $scope.$watch(function () {
                return CommonService.getAllRewardTypes();
            }, function (newVal, oldVal) {
                if (angular.isDefined(newVal)) {
                    $scope.model.allRewardTypes = newVal;
                }
            }, true);

            $scope.$watch(function () {
                return CommonService.getAllQuestsByChosenType();
            }, function (newVal, oldVal) {
                if (angular.isDefined(newVal)) {
                    $scope.model.allQuestsByChosenType = newVal;
                }
            }, true);
        }]);
