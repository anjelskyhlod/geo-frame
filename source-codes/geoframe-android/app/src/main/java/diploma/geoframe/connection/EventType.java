package diploma.geoframe.connection;

/**
 * @author Lucie Kucharova
 * These are all types of events, which can be received or produced by
 * this service.
 */

public enum EventType {
    NEW_POINT, NEW_GAME,

    GET_LOGIN,
    AUTH_TOKEN,
    GET_ALL_GAMES_BASIC,
    SEND_ALL_GAMES_BASIC,
    SEND_GAME_BY_ID,
    GET_GAME_BY_ID,

    GET_OBJECTS_BY_GAME,
    SEND_OBJECTS_BY_GAME,
    SAVED_GAME_OBJECT,
    UPDATED_GAME_OBJECT,
    DELETED_GAME_OBJECT,

    IM_HERE,
    SEND_OBJECTS_BY_LOCATION,

    GET_QUESTS_BY_IDS,
    QUEST_TYPE_RIDDLE_BY_IDS,
    CHECK_SOLVED,
    IS_SOLVED,

    SEND_CHARACTER,
    OBJECT_UNCOVERED,

    I_END,
}
