package diploma.geoframe.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lucie Kucharova
 * Repesentation of Player Object
 */

public class Player extends GameObject {

    String username;
    List<String> completedQuestIds;

    public Player(JSONObject json) {
        super(json);
        completedQuestIds = new ArrayList<>();
        try {
            if (!json.isNull("username")) {
                this.username = json.getString("username");
            }
            if (!json.isNull("questsCompletedIds")) {
                JSONArray arr = json.getJSONArray("questsCompletedIds");
                for (int i = 0; i < arr.length(); i++) {
                    String id = arr.getString(i);
                    completedQuestIds.add(id);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<String> getCompletedQuestIds() {
        return completedQuestIds;
    }

    public void setCompletedQuestIds(List<String> completedQuestIds) {
        this.completedQuestIds = completedQuestIds;
    }
}
