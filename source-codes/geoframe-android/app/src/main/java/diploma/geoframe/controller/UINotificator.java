package diploma.geoframe.controller;

/**
 * @author Lucie Kucharova
 * Every object which wants to notify UI
 * about changes has to implement this
 * interface
 */

public interface UINotificator {
    void produceEvent(UINotification data);
}
