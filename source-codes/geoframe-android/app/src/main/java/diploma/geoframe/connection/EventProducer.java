package diploma.geoframe.connection;

/**
 * @author Lucie Kucharova
 * Interface for controllers to be able to
 * emit new events outside app
 */

public interface EventProducer {
    void produceEvent(Event msg);
}
