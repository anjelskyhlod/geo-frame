package diploma.geoframe.connection;

import android.util.Log;
import diploma.geoframe.controller.GameController;
import diploma.geoframe.controller.LocationController;
import diploma.geoframe.controller.LoginController;
import diploma.geoframe.controller.StorageController;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Lucie Kucharova
 * Mechanism responsible for navigating incoming @{@link Event} from broker
 * to correct controller, which knows how to handle it.
 */

public class EventHandler implements EventListener {

    private EventBuilder eventBuilder;

    private GameController gameController;
    private LoginController loginController;

    public EventHandler() {
        eventBuilder = new EventBuilder();
    }

    public void process(Event event) {
        if(event == null) {
            Log.e(this.getClass().toString(), "Event is empty");
            return;
        }
        EventType eventType = event.getType();
        String msg = "";
        JSONObject msgObj = null;
        JSONObject body = null;
        try {
            msg = event.getValue().get("message").toString();
            msgObj = new JSONObject(msg);
            body = new JSONObject(msgObj.get("data").toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        switch (eventType) {
            case AUTH_TOKEN:
                String token = "";
                try {
                    token = body.get("access_token").toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                loginController.resolveAuthentication(token);
                break;
            case SEND_ALL_GAMES_BASIC:
                gameController.setAllGamesBasic(body);
                break;
            case SEND_OBJECTS_BY_LOCATION:
                gameController.setAllObjects(body);
                break;
            case SEND_GAME_BY_ID:
                gameController.setCurrentGame(body);
                break;
            case QUEST_TYPE_RIDDLE_BY_IDS:
                gameController.setRiddleOnCurrentObject(body);
                break;
            case IS_SOLVED:
                gameController.setResultOfQuest(body);
                break;
            case SEND_CHARACTER:
                gameController.setPlayerObject(body);
                break;
            case OBJECT_UNCOVERED:
                gameController.addHiddenObject(body);
                break;
            case SAVED_GAME_OBJECT:
                gameController.addNesObject(body);
                break;
            case UPDATED_GAME_OBJECT:
                gameController.updateObject(body);
                break;
            case DELETED_GAME_OBJECT:
                gameController.deleteObject(body);
                break;
            default:
                Log.w(this.getClass().toString(), "Process event: Unknown event Type.");
        }
    }

    @Override
    public void handleEvent(String msg) {
        process(eventBuilder.createEvent(msg));
    }

    public void setLoginController(LoginController loginController) {
        this.loginController = loginController;
    }

    public void setGameController(GameController gameController) {
        this.gameController = gameController;
    }
}
