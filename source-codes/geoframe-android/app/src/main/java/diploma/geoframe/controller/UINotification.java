package diploma.geoframe.controller;

/**
 * @author Lucie Kucharova
 * This class is representing event emited to
 * UI when changes happen in model.
 */

public class UINotification<T> {

    private UINotificationType type;
    private T value;

    public UINotification() {
    }

    public UINotification(UINotificationType type, T value) {
        this.type = type;
        this.value = value;
    }

    public UINotificationType getType() {
        return type;
    }

    public void setType(UINotificationType type) {
        this.type = type;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

}
