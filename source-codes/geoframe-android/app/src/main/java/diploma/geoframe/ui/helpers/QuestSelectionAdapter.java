package diploma.geoframe.ui.helpers;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import diploma.geoframe.quests.Quest;

import java.util.List;

/**
 * @author Lucie Kucharova
 * Helper UI component for quest listing
 */

public class QuestSelectionAdapter extends ArrayAdapter<Quest> {


    private Context context;
    private List<Quest> values;
    private List<String> completedQuestIds;

    public QuestSelectionAdapter(Context context, int textViewResourceId,
                                 List<Quest> values, List<String> completedQuestIds) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
        this.completedQuestIds = completedQuestIds;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Quest getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        String text = values.get(position).getName();
        if (completedQuestIds.contains(values.get(position).getQuestType() + "_" + values.get(position).getId().toString())) {
            text += " (completed)";
        }
        label.setText(text);
        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = (TextView) super.getDropDownView(position, convertView, parent);
        label.setTextColor(Color.BLACK);
        label.setText(values.get(position).getName());

        return label;
    }
}
