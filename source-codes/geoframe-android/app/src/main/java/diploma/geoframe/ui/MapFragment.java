package diploma.geoframe.ui;

import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.*;
import android.widget.Toast;
import diploma.geoframe.GlobalState;
import diploma.geoframe.controller.GameController;
import diploma.geoframe.controller.LocationController;
import diploma.geoframe.controller.UINotification;
import diploma.geoframe.controller.UINotificationType;
import diploma.geoframe.model.Game;
import diploma.geoframe.model.GameObject;
import diploma.geoframe.quests.Quest;
import diploma.geoframe.quests.QuestUIHandler;
import diploma.geoframe.quests.QuestResult;
import diploma.geoframe.ui.dialogs.GameObjectDialog;
import org.osmdroid.api.IMapController;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.util.*;
import java.util.logging.Logger;

/**
 * @author Lucie Kucharova
 * Fragment responsible for all actions done
 * on map screen
 */

public class MapFragment extends Fragment implements UINotificationListener {
    private MapView mMapView;
    private MyLocationNewOverlay mLocationOverlay;
    private CompassOverlay mCompassOverlay;
    private ItemizedOverlay<OverlayItem> mItemizedOverlay;

    private GlobalState globalState;
    private GameController gameController;
    private LocationController locationController;
    private Long gameId;

    private Game currentGame;
    private GameObject currentGameObject;

    private Map<Long, OverlayItem> gameObjectIdPointMap;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameId = getArguments().getLong("GAME_ID");
        System.out.println(gameId);
        globalState = (GlobalState) getActivity().getApplicationContext();
        gameController = globalState.getGameController();
        gameController.addListener(this);
        locationController = globalState.getLocationController();
        locationController.addListener(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mMapView = new MapView(inflater.getContext());
        return mMapView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mMapView.setBuiltInZoomControls(true);
        mMapView.setMultiTouchControls(true);
        mMapView.setTilesScaledToDpi(false);
        final Context context = this.getActivity();
        showMyLocation(context);
        // zoom player once we have his position
        mLocationOverlay.runOnFirstFix(
                new Runnable() {
                    @Override
                    public void run() {
                        mMapView.post(new Runnable() {
                                          public void run() {
                                              Log.i("Location", "I HAVE LOCATION");
                                              zoomToLocation(mLocationOverlay.getMyLocation());
                                              locationController.imHere(globalState.getClient().getTopic(),
                                                      mLocationOverlay.getMyLocation(),
                                                      globalState.getCurrentGame().getVisibleRange(),
                                                      globalState.getCurrentGame().getId());
                                          }
                                      }
                        );
                        while (true) {
                            synchronized (this) {
                                locationController.setMyLocation(mLocationOverlay.getMyLocation(),
                                        globalState.getClient().getTopic(),
                                        globalState.getCurrentGame().getVisibleRange(),
                                        globalState.getCurrentGame().getId(),
                                        globalState.getCurrentGame().getVisibleRange());
                                try {
                                    wait(locationController.ITERATION_TIME);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });
        showCompass(context);
        initPoints();
        setHasOptionsMenu(true);
        initPoints();
        gameController.requestCurrentGame(globalState.getClient().getTopic(), gameId);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void showMyLocation(Context context) {
        GpsMyLocationProvider provider = new GpsMyLocationProvider(context);
        provider.addLocationSource(LocationManager.NETWORK_PROVIDER);
        //my location marker
        this.mLocationOverlay = new MyLocationNewOverlay(provider, mMapView);
        mLocationOverlay.enableMyLocation();
        mLocationOverlay.enableFollowLocation();
        mMapView.getOverlays().add(this.mLocationOverlay);
    }

    public void zoomToLocation(GeoPoint point) {
        IMapController mapController = mMapView.getController();
        mapController.setZoom(20);
        mapController.setCenter(point);
    }

    private void showCompass(Context context) {
        //top-left compass
        this.mCompassOverlay = new CompassOverlay(context, new InternalCompassOrientationProvider(context), mMapView);
        this.mCompassOverlay.enableCompass();
        mMapView.getOverlays().add(this.mCompassOverlay);
    }

    /**
     * Creates new overlay on map where new
     * game object will be inserted
     */
    private void initPoints() {
        gameObjectIdPointMap = new HashMap<>();
        ArrayList<OverlayItem> items = new ArrayList<>();
        this.mItemizedOverlay = new ItemizedIconOverlay<>(items,
                new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
                    @Override
                    public boolean onItemSingleTapUp(final int index, final OverlayItem item) {
                        Long objectId = Long.parseLong(item.getTitle());
                        for (GameObject gameObj : currentGame.getGameObjects()) {
                            if (gameObj.getId().equals(objectId)) {
                                currentGameObject = gameObj;
                                gameController.getQuestsById(globalState.getClient().getTopic(), gameObj.getBaseQuests());
                            }
                        }
                        GameObjectDialog dialog = new GameObjectDialog();
                        dialog.setCurrentObject(currentGameObject);
                        dialog.show(getFragmentManager(), "gameObjectDialog");
                        return true; // We 'handled' this event.
                    }

                    @Override
                    public boolean onItemLongPress(final int index, final OverlayItem item) {
                        // nothing needed yet
                        return false;
                    }
                }, this.getActivity().getApplicationContext());
        this.mMapView.getOverlays().add(this.mItemizedOverlay);
    }

    /**
     * Display new UI point in game objects overlay
     *
     * @param object
     */
    public void addPoint(GameObject object) {
        GeoPoint point = new GeoPoint(object.getLocation().getLattitude(), object.getLocation().getLongitude());
        OverlayItem item = new OverlayItem(object.getId().toString(), object.getDescription(), point);
        ((ItemizedIconOverlay) this.mItemizedOverlay).addItem(item);
        gameObjectIdPointMap.put(object.getId(), item);
    }

    public void removePoint(Long gameObjectId){
        ((ItemizedIconOverlay)this.mItemizedOverlay).removeItem(gameObjectIdPointMap.get(gameObjectId));
    }

    /**
     * Filters objects and shows only those with
     * location specified
     *
     * @param gameObjects
     */
    public void addGameObjects(List<GameObject> gameObjects) {
        this.currentGame.addGameObjects(gameObjects);
        for (GameObject gameObject : gameObjects) {
            if (gameObject.getLocation() != null) {
                addPoint(gameObject);
            } else {
                Log.i(getClass().toString(), "Object does not have a location");
            }
        }
    }

    /**
     * Adds newly loaded quests to specific game object
     *
     * @param quests
     */
    public void addNewQuests(List<Quest> quests) {
        for (Quest newQuest : quests) {
            for (ListIterator<Quest> iterator = this.currentGameObject.getBaseQuests().listIterator(); iterator.hasNext(); ) {
                Quest basicQuest = iterator.next();
                if (basicQuest.getId().equals(newQuest.getId())) {
                    iterator.remove();
                    iterator.add(newQuest);
                }
            }
        }
    }

    /**
     * Uses @{@link QuestUIHandler} to resolve quest
     * result for specific quest type
     *
     * @param data
     */
    public void showQuestResult(QuestResult data) {
        QuestUIHandler manager = new QuestUIHandler((GlobalState) getActivity().getApplicationContext(),
                getFragmentManager());
        manager.resolveCompletion(data);
    }

    private void initCurrentGame(Game currentGame) {
        globalState.setCurrentGame(currentGame);
        this.currentGame = currentGame;
    }

    /**
     * Adds new object to map when object appears
     * unexpectedly
     *
     * @param object
     */
    private void addHiddenObject(GameObject object) {
        List<GameObject> list = new ArrayList<>();
        list.add(object);
        addGameObjects(list);
        Toast.makeText(
                getActivity(),
                "New object '" + object.getName() + " appeared.", Toast.LENGTH_LONG).show();
    }

    private void updateObject(GameObject value) {
        GameObject toBeUpdated = null;
        for(GameObject old : this.currentGame.getGameObjects()){
            if(old.getId() == value.getId()){
                Log.i(this.getClass().toString(), "Updating object");
                toBeUpdated = old;
            }
        }
        if(toBeUpdated != null) {
            Log.i(this.getClass().toString(), "Updating object");
            this.currentGame.getGameObjects().remove(toBeUpdated);
            this.currentGame.getGameObjects().add(value);
        }
    }


    private void removeObject(Long gameObjectId) {
        removePoint(gameObjectId);
        GameObject toBeDeleted = null;
        for(GameObject old : this.currentGame.getGameObjects()){
            if(old.getId() == gameObjectId){
                toBeDeleted = old;
            }
        }
        if(toBeDeleted != null) {
            Log.i(this.getClass().toString(), "Deleting object");
            this.currentGame.getGameObjects().remove(toBeDeleted);
        }
    }

    @Override
    public void notification(UINotification data) {
        UINotificationType notificationType = data.getType();
        switch (notificationType) {
            case OBJECTS_BY_GAME:
                Log.i(this.getClass().toString(), "Setting new objects... " + notificationType.toString());
                addGameObjects((List<GameObject>) data.getValue());
                break;
            case CURRENT_GAME:
                Log.i(this.getClass().toString(), "Setting new game... " + notificationType.toString());
                initCurrentGame((Game) data.getValue());
                break;
            case SOME_QUESTS_LOADED:
                Log.i(this.getClass().toString(), "Setting new quests... " + notificationType.toString());
                addNewQuests((List<Quest>) data.getValue());
                break;
            case RESULT_IS_HERE:
                Log.i(this.getClass().toString(), "Notifying about quest result " + notificationType.toString());
                showQuestResult((QuestResult) data.getValue());
                break;
            case ADD_HIDDEN_OBJECT:
                Log.i(this.getClass().toString(), "Adding hidden object... " + notificationType.toString());
                addHiddenObject((GameObject) data.getValue());
                break;
            case UPDATE_EXISTING_OBJECT:
                Log.i(this.getClass().toString(), "Updating object... " + notificationType.toString());
                updateObject((GameObject) data.getValue());
                break;
            case DELETE_EXISTING_OBJECT:
                Log.i(this.getClass().toString(), "Removing object... " + notificationType.toString());
                removeObject((Long) data.getValue());
                break;
            default:
                Log.i(this.getClass().toString(), "No message for me :( ");
        }
    }
}
