package diploma.geoframe.quests.riddleQuest;

import android.util.Log;
import diploma.geoframe.quests.QuestResult;
import diploma.geoframe.quests.QuestType;
import org.json.JSONObject;

/**
 * @author Lucie Kucharova
 * Class representation of player's success/failure
 * on quest completion
 */

public class RiddleResult implements QuestResult {
    private boolean isCorrect;
    private String userAnswer;
    private Long questId;

    public RiddleResult(boolean isCorrect, String userAnswer, Long questId) {
        this.isCorrect = isCorrect;
        this.userAnswer = userAnswer;
        this.questId = questId;
    }

    public RiddleResult(JSONObject json) {
        try {
            if (!json.isNull("isCorrect")) {
                isCorrect = json.getBoolean("isCorrect");
            }
            if (!json.isNull("answer")) {
                userAnswer = json.getString("answer");
            }
            if (!json.isNull("questId")) {
                questId = json.getLong("questId");
            }
        } catch (Exception e) {
            Log.e(this.getClass().toString(), e.getMessage());
        }
    }

    public boolean isCorrect() {
        return isCorrect;
    }

    public void setCorrect(boolean correct) {
        isCorrect = correct;
    }

    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(String userAnswer) {
        this.userAnswer = userAnswer;
    }

    public Long getQuestId() {
        return questId;
    }

    public void setQuestId(Long questId) {
        this.questId = questId;
    }

    @Override
    public QuestType getQuestType() {
        return QuestType.QUEST_TYPE_RIDDLE;
    }
}
