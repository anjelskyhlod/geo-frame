package diploma.geoframe.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import diploma.geoframe.GlobalState;
import diploma.geoframe.R;
import diploma.geoframe.controller.*;
import diploma.geoframe.model.Game;
import diploma.geoframe.ui.helpers.GameSpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

import static diploma.geoframe.R.id.allGamesSpinner;

/**
 * @author Lucie Kucharova
 * Activity handling welcome screen
 */

public class WelcomeActivity extends Activity implements UINotificationListener {

    private GlobalState globalState;

    private GameController gameController;
    private List<Game> basicGamesInfoList;
    private List<String> gameNamesList;

    private Spinner spinner;
    private GameSpinnerAdapter arrayAdapter;
    private Button joinButt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        globalState = (GlobalState) getApplicationContext();
        gameController = globalState.getGameController();
        gameController.addListener(this);
        setContentView(R.layout.welcome);
        gameController.requestGetAllGames(globalState.getClient().getTopic());

        gameNamesList = new ArrayList<>();
        this.spinner = (Spinner) findViewById(R.id.allGamesSpinner);
        joinButt = findViewById(R.id.joinGameButt);
        joinButt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeActivity.this, GameAreaActivity.class);
                intent.putExtra("GAME_ID", ((Game) spinner.getSelectedItem()).getId());
                startActivity(intent);
            }
        });
    }

    private void fillSpinnerWithGameNames(List<Game> gameNamesList) {
        arrayAdapter = new GameSpinnerAdapter(this, R.layout.my_spinner_item, gameNamesList);
//        arrayAdapter.setDropDownViewResource(R.layout.my_spinner_item);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                spinner.setAdapter(arrayAdapter);
            }
        });

    }

    public void setController(GameController controller) {
        gameController = controller;
    }

    @Override
    public void notification(UINotification notification) {
        UINotificationType notificationType = notification.getType();
        switch (notificationType) {
            case ALL_GAMES_BASIC:
                Log.i(this.getClass().toString(), notification.getValue().toString());
                basicGamesInfoList = (List<Game>) notification.getValue();
                fillSpinnerWithGameNames(basicGamesInfoList);
                break;
            default:
                Log.i(this.getClass().toString(), "No message for me :( ");
        }
    }
}
