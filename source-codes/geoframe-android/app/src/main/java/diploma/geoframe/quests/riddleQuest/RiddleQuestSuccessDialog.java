package diploma.geoframe.quests.riddleQuest;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.EditText;
import diploma.geoframe.R;
import diploma.geoframe.quests.QuestDialog;

/**
 * @author Lucie Kucharova
 * UI Representation of @{@link RiddleResult} object
 * in case of successful answer
 */

public class RiddleQuestSuccessDialog extends DialogFragment implements QuestDialog {

    RiddleResult result;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.success)
                .setMessage("Your answer is correct!");
        return builder.create();
    }

    public void setResult(RiddleResult result) {
        this.result = result;
    }
}
