package diploma.geoframe.quests;

import android.util.Log;
import diploma.geoframe.quests.riddleQuest.RiddleQuest;
import diploma.geoframe.quests.riddleQuest.RiddleQuestController;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Lucie Kucharova
 * Determines type of given quest and invokes
 * type specific actions via specialized controllers
 */

public class QuestHandler {

    public QuestResult resolveCompletition(JSONObject data) {
        QuestType type = null;
        try {
            String sType = data.getString("questType");
            for (QuestType typ : QuestType.values()) {
                if (typ.toString().equals(sType)) {
                    type = typ;
                    break;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        QuestResult result = null;
        switch (type) {
            case QUEST_TYPE_RIDDLE:
                RiddleQuestController ctrl = new RiddleQuestController();
                result = ctrl.resolveCompletion(data);
                break;
            default:
                Log.w(this.getClass().toString(), "Unknown quest type");
        }
        return result;
    }

    public Quest createQuestObject(JSONObject jQuest) {
//        again switch (type)......
//        little cheat for now (current impl just riddle)
        return new RiddleQuest(jQuest);
    }
}
