package diploma.geoframe.connection;

/**
 * @author Lucie Kucharova
 * Interface for event handlers
 */

public interface EventListener {
    void handleEvent(String msg);
}
