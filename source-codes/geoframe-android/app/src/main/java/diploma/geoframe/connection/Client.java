package diploma.geoframe.connection;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import diploma.geoframe.controller.StorageController;
import okhttp3.WebSocket;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocketListener;
import okio.ByteString;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Lucie Kucharova
 * Client allows access to websocket server
 */

public class Client implements EventProducer {

    public final String DEVICE = "ANDROID";
    //    public String URL = "ws://88.100.200.164:7081/v2/broker/";
    public String URL = "ws://10.0.0.9:7081/v2/broker/";
    private WebSocketEcho listener;
    private WebSocket client;
    private String topic;

    private List<EventListener> listeners;
    private boolean connected;
    private StorageController storageController;

    public Client() {
        listeners = new ArrayList<>();
    }

    /**
     * Inits connection on specified topic with
     * specified IP address. If IP is not specified,
     * default URL is used...
     *
     * @param listenOnTopic
     * @param ipAddress
     */
    public void initConnection(String listenOnTopic, String ipAddress) {
        System.out.println("initiating");
        topic = listenOnTopic;
        listener = new WebSocketEcho();
        connected = false;

        OkHttpClient clientOk = new OkHttpClient.Builder()
                .readTimeout(0, TimeUnit.MILLISECONDS)
                .build();

        if (ipAddress != null && ipAddress.length() > 0) {
            URL = "ws://" + ipAddress + "/v2/broker/";
        }

        Request request = new Request.Builder()
                .url(URL + "?topics=" + topic)
                .build();
        System.out.println("Create new token");
        client = clientOk.newWebSocket(request, listener);
        System.out.println("token created");
    }

    public boolean isConnectionInitialized() {
        return client != null;
    }

    public void sendMessage(Event message) {
        JSONObject json = new JSONObject();
        JSONObject value = new JSONObject();

        try {
            if (message.getToTopic() == null) {
                json.put("topic", message.getType());
            } else {
                json.put("topic", message.getToTopic());
            }
            value.put("data", message.getValue());
            value.put("eventType", message.getType().toString());
            value.put("device", DEVICE);
            value.put("authorization", "Bearer " + storageController.getToken());
            json.put("key", topic);

            json.put("message", value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("send message:" + message.toString());
        System.out.println(json.toString());
        String str = json.toString();
        client.send(str);
    }

    public void addListener(EventListener listener) {
        listeners.add(listener);
    }

    @Override
    public void produceEvent(Event msg) {
        if (client == null) {
            String ipAddress = null;
            try {
                ipAddress = msg.getValue().getString("ipAddress");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            initConnection(msg.getUserId().toLowerCase(), ipAddress);
            while (!connected) {
                synchronized (this) {
                    try {
                        System.out.println("waiting...");
                        wait(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        sendMessage(msg);
    }

    private class WebSocketEcho extends WebSocketListener {

        @Override
        public void onOpen(WebSocket webSocket, Response response) {
            System.out.println("OTEVRENO");
            System.out.println("listening on topic " + topic);
            connected = true;
        }

        @Override
        public void onMessage(WebSocket webSocket, String text) {
            System.out.println("MESSAGE: " + text);
            System.out.println("Notifying listeners... ");
            for (EventListener listener : listeners) {
                listener.handleEvent(text);
            }
        }

        @Override
        public void onMessage(WebSocket webSocket, ByteString bytes) {
            System.out.println("MESSAGE: " + bytes.hex());
        }

        @Override
        public void onClosing(WebSocket webSocket, int code, String reason) {
            webSocket.close(1000, null);
            System.out.println("CLOSE: " + code + " " + reason);
            client = null;
        }

        @Override
        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
            System.out.println("FAILURE");
            client = null;
            t.printStackTrace();
        }

    }

    public void setStorageController(StorageController storageController) {
        this.storageController = storageController;
    }

    public String getTopic() {
        return this.topic;
    }

}
