package diploma.geoframe.quests;

/**
 * @author Lucie Kucharova
 * Base Quest class which has to be implemented by
 * all possible quest types.
 */

public class BaseQuest implements Quest {
    Long id;
    QuestType questType;

    public BaseQuest(Long id, QuestType questType) {
        this.id = id;
        this.questType = questType;
    }

    public BaseQuest() {
    }

    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return "I am base quest, I don't have a name";
    }

    @Override
    public String getDescription() {
        return "I am base quest, I don't have a description";
    }

    public QuestType getQuestType() {
        return questType;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setQuestType(QuestType questType) {
        this.questType = questType;
    }
}