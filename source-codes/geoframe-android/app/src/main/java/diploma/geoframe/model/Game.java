package diploma.geoframe.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lucie Kucharova
 * Repesentation of Game info and settings
 */

public class Game {

    private Long id;
    private String name;
    private String description;
    private List<GameObject> gameObjects;
    private Long visibleRange;

    public Game(String name, String description, List<GameObject> gameObjects) {
        this.name = name;
        this.description = description;
        this.gameObjects = gameObjects;
    }

    public Game(JSONObject json) {
        try {
            if (!json.isNull("name")) {
                this.name = json.getString("name");
            }
            if (!json.isNull("description")) {
                this.description = json.getString("description");
            }
            if (!json.isNull("id")) {
                this.id = json.getLong("id");
            }
            if (!json.isNull(("visibleRange"))) {
                this.visibleRange = json.getLong("visibleRange");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<GameObject> getGameObjects() {
        return gameObjects;
    }

    public void addGameObjects(List<GameObject> gameObjects) {
        if (this.getGameObjects() == null) this.gameObjects = new ArrayList<>();
        for (GameObject obj : gameObjects) {
            this.getGameObjects().add(obj);
        }
    }

    public Long getVisibleRange() {
        return visibleRange;
    }

    public void setVisibleRange(Long visibleRange) {
        this.visibleRange = visibleRange;
    }
}