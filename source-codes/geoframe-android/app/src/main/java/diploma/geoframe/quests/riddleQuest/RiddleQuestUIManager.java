package diploma.geoframe.quests.riddleQuest;

import android.support.v4.app.FragmentManager;
import diploma.geoframe.GlobalState;
import diploma.geoframe.quests.Quest;
import diploma.geoframe.quests.QuestResult;
import diploma.geoframe.quests.QuestUIManager;

/**
 * @author Lucie Kucharova
 * This class is responsible for managing UI components
 * of @{@link RiddleQuest}
 */

public class RiddleQuestUIManager implements QuestUIManager {

    private RiddleQuest riddleQuest;
    private FragmentManager fragmentManager;


    public RiddleQuestUIManager(FragmentManager fragManager, Quest quest) {
        fragmentManager = fragManager;
        this.riddleQuest = (RiddleQuest) quest;
    }

    public RiddleQuestUIManager(FragmentManager fragManager) {
        fragmentManager = fragManager;
    }

    public void showQuestDialog() {
        RiddleQuestDialog dialog = new RiddleQuestDialog();
        dialog.setCurrentQuest(riddleQuest);
        dialog.setRiddleQuestController(this);
        dialog.show(fragmentManager, "riddleQuestDialog");
    }

    public void resolveCompletion(QuestResult result) {
        RiddleResult riddleResult = (RiddleResult) result;
        if (riddleResult.isCorrect()) {
            showSuccesDialog(riddleResult);
        } else {
            showFailureDialog(riddleResult);
        }
    }

    public void showSuccesDialog(RiddleResult result) {
        RiddleQuestSuccessDialog dialog = new RiddleQuestSuccessDialog();
        dialog.setResult(result);
        dialog.show(fragmentManager, "riddleQuestSuccessDialog");
    }

    public void showFailureDialog(RiddleResult result) {
        RiddleQuestFailureDialog dialog = new RiddleQuestFailureDialog();
        dialog.setResult(result);
        dialog.show(fragmentManager, "riddleQuestFailureDialog");
    }
}
