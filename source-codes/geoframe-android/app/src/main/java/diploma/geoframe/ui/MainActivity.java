package diploma.geoframe.ui;

import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import diploma.geoframe.GlobalState;
import diploma.geoframe.R;
import diploma.geoframe.connection.Event;
import diploma.geoframe.connection.EventType;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.config.Configuration;

/**
 * @author Lucie Kucharova
 * Main class activity
 */

public class MainActivity extends AppCompatActivity {

    private Button goToLogIn;

    private GlobalState globalState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        globalState = (GlobalState) getApplicationContext();
        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        setContentView(R.layout.activity_main);

        goToLogIn = findViewById(R.id.goToLogIn);
        goToLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        GlobalState state = ((GlobalState) getApplicationContext());
        state.setupStorage(this);
        state.setupConnection();
        state.setupControllers();
    }

    public void onResume() {
        super.onResume();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().save(this, prefs);
        Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        JSONObject data = new JSONObject();
//        try {
//            data.put("eventType", "I_END");
//            data.put("gameId", globalState.getCurrentGame().getId());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        globalState.getClient().produceEvent(new Event(globalState.getClient().getTopic(),
//                EventType.I_END, data, null));
    }

}
