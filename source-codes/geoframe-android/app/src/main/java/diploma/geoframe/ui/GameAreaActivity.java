package diploma.geoframe.ui;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import diploma.geoframe.GlobalState;
import diploma.geoframe.R;
import diploma.geoframe.controller.GameController;
import diploma.geoframe.controller.UINotification;
import diploma.geoframe.controller.UINotificationType;
import diploma.geoframe.model.Player;
import org.osmdroid.config.Configuration;

/**
 * @author Lucie Kucharova
 * Activity handling in-game actions
 */

public class GameAreaActivity extends AppCompatActivity implements UINotificationListener {

    private GlobalState globalState;
    private GameController gameController;

    private Long currentGameId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_area);
        Log.i(getClass().toString(), "GameAreaActivity started");
        globalState = (GlobalState) getApplicationContext();
        gameController = globalState.getGameController();
        gameController.addListener(this);
        currentGameId = getIntent().getLongExtra("GAME_ID", 0);
        showMap();
    }

    private void showMap() {
        Bundle bundle = new Bundle();
        bundle.putLong("GAME_ID", currentGameId);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MapFragment fragment = new MapFragment();
        fragment.setArguments(bundle);
        fragmentTransaction.add(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
    }

    public void onResume() {
        super.onResume();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().save(this, prefs);
        Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void notification(UINotification notification) {
        UINotificationType notificationType = notification.getType();
        switch (notificationType) {
            case SET_CHARACTER:
                Log.i(this.getClass().toString(), notification.getValue().toString());
                Player player = (Player) notification.getValue();
                if (player.getUsername().equals(globalState.getClient().getTopic())) {
                    globalState.setCurrentPlayer(player);
                } else {
                    System.out.println("Spatny player.");
                }
                break;
            default:
                Log.i(this.getClass().toString(), "No message for me :( ");
        }
    }
}
