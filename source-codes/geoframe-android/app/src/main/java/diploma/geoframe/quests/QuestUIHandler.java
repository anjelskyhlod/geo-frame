package diploma.geoframe.quests;

import android.support.v4.app.FragmentManager;
import android.util.Log;
import diploma.geoframe.GlobalState;
import diploma.geoframe.quests.riddleQuest.*;

/**
 * @author Lucie Kucharova
 * Determines type of given quest and invokes
 * type specific actions in UI
 */

public class QuestUIHandler {

    private GlobalState globalState;
    private FragmentManager fragmentManager;

    public QuestUIHandler(GlobalState applicationContext, FragmentManager fragmentManager) {
        this.globalState = applicationContext;
        this.fragmentManager = fragmentManager;
    }

    public void runQuest(Quest quest) {
        QuestType type = quest.getQuestType();
        switch (type) {
            case QUEST_TYPE_RIDDLE:
                RiddleQuestUIManager ctrl = new RiddleQuestUIManager(fragmentManager, quest);
                ctrl.showQuestDialog();
                break;
            default:
                Log.w(this.getClass().toString(), "Unknown quest type");
        }
    }

    public void resolveCompletion(QuestResult result) {
        QuestType type = result.getQuestType();
        switch (type) {
            case QUEST_TYPE_RIDDLE:
                RiddleQuestUIManager ctrl = new RiddleQuestUIManager(fragmentManager);
                ctrl.resolveCompletion(result);
                break;
            default:
                Log.w(this.getClass().toString(), "Unknown quest type");
        }
    }
}
