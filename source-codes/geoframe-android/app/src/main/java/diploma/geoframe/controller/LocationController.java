package diploma.geoframe.controller;

import diploma.geoframe.connection.Event;
import diploma.geoframe.connection.EventBuilder;
import diploma.geoframe.connection.EventProducer;
import diploma.geoframe.connection.EventType;
import diploma.geoframe.model.Game;
import diploma.geoframe.model.GameObject;
import diploma.geoframe.model.Location;
import diploma.geoframe.quests.Quest;
import diploma.geoframe.quests.QuestType;
import diploma.geoframe.quests.riddleQuest.RiddleQuest;
import diploma.geoframe.ui.UINotificationListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.util.GeoPoint;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Lucie Kucharova
 * This class handles player location.
 */

public class LocationController implements UINotificator {

    public static final int ITERATION_TIME = 2000;

    private EventProducer eventProducer;

    private List<UINotificationListener> listeners;

    private volatile GeoPoint myLocation;
    private GeoPoint lastLocationOfObjCheck;
    private boolean firstTimeChecking;

    public LocationController(EventProducer producer) {
        this.eventProducer = producer;
        this.listeners = new ArrayList<>();
        firstTimeChecking = false;
    }

    @Override
    public void produceEvent(UINotification data) {
        for (UINotificationListener listener : listeners) {
            listener.notification(data);
        }
    }

    /**
     * Checks if location is in default area radius
     *
     * @param location
     * @param defaultAreaRadius
     * @return True if object is reachable, else false
     */
    public boolean isTooFarAway(Location location, Double defaultAreaRadius) {
        //if area is not specified - is accessible from anywhere
        if (defaultAreaRadius == null) return false;
        double distance = distFrom(myLocation.getLatitude(), myLocation.getLongitude(),
                location.getLattitude(), location.getLongitude());
        return distance > defaultAreaRadius;
    }

    public void addListener(UINotificationListener activity) {
        this.listeners.add(activity);
    }


    public synchronized GeoPoint getMyLocation() {
        return myLocation;
    }

    /**
     * Receives info about new location and calls
     * loadNewObjectsIfNeeded(...) method
     *
     * @param myLocation
     * @param topic
     * @param visibleRange
     * @param gameId
     * @param visibleRadius
     */
    public synchronized void setMyLocation(GeoPoint myLocation, String topic, Long visibleRange, Long gameId, Long visibleRadius) {
        System.out.println("My Location is: " + myLocation.toDoubleString());
        this.myLocation = myLocation;
        loadNewObjectsIfNeeded(myLocation, topic, visibleRange, gameId, visibleRadius);
    }

    /**
     * Checks if player is faaar faaar away from
     * last checked point. If so, system is informed
     * (system is expected to send game entities nearby)
     *
     * @param myLocation
     * @param topic
     * @param visibleRange
     * @param gameId
     * @param visibleRadius
     */
    private void loadNewObjectsIfNeeded(GeoPoint myLocation, String topic, Long visibleRange, Long gameId, Long visibleRadius) {
        //if is faar away from last checked point
        if (lastLocationOfObjCheck != null) {
            firstTimeChecking = false;
            if (visibleRadius == null) visibleRadius = new Long(0);
            if (distFrom(myLocation, lastLocationOfObjCheck) > visibleRadius / 2)
                imHere(topic, myLocation, visibleRange, gameId);
        } else {
            firstTimeChecking = true;
            imHere(topic, myLocation, visibleRange, gameId);
        }
    }

    /**
     * Produces event for system to notify it about
     * current player position
     *
     * @param topic
     * @param myLocation
     * @param visibleRange
     * @param gameId
     */
    public void imHere(String topic, GeoPoint myLocation, Long visibleRange, Long gameId) {
        lastLocationOfObjCheck = myLocation;
        JSONObject object = new JSONObject();
        try {
            object.put("gameId", gameId);
            object.put("lattitude", myLocation.getLatitude());
            object.put("longitude", myLocation.getLongitude());
            object.put("eventType", EventType.IM_HERE.toString());
            object.put("visibleRange", visibleRange);
            object.put("firstTime", firstTimeChecking);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        Event event = new Event(topic, EventType.IM_HERE, object);
        eventProducer.produceEvent(event);
    }

    private double distFrom(GeoPoint one, GeoPoint two) {
        return distFrom(one.getLatitude(), one.getLongitude(), two.getLatitude(), two.getLongitude());
    }

    /**
     * Calculates distance between two GPS coordinates
     *
     * @param lat1
     * @param lng1
     * @param lat2
     * @param lng2
     * @return distance in meters
     */
    private double distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 3958.75; // miles (or 6371.0 kilometers)
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double a = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;
        return dist * 1000; //in meters
    }
}
