package diploma.geoframe.ui.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;
import diploma.geoframe.GlobalState;
import diploma.geoframe.R;
import diploma.geoframe.controller.LocationController;
import diploma.geoframe.controller.UINotification;
import diploma.geoframe.model.GameObject;
import diploma.geoframe.ui.UINotificationListener;

/**
 * @author Lucie Kucharova
 * UI dialog shown to player when detailed
 * info about game object is needed
 */

public class GameObjectDialog extends DialogFragment implements UINotificationListener {

    private GameObject currentGameObject;

    private LocationController locationController;
    private GlobalState globalState;

    private boolean closeDialog = true;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        globalState = (GlobalState) getActivity().getApplicationContext();
        locationController = globalState.getLocationController();
        locationController.addListener(this);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder//.setView(inflater.inflate(R.layout.game_object_dialog, null))
                // Add action buttons
                .setTitle(this.currentGameObject.getName())
                .setMessage(this.currentGameObject.getDescription())
                .setNeutralButton(R.string.show_quests, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Log.i(getClass().toString(), "SHOW QUESTS");
                        if (locationController.isTooFarAway(currentGameObject.getLocation(),
                                currentGameObject.getAccessibleRadius())) {
                            closeDialog = false;
                            Toast.makeText(
                                    getActivity(),
                                    "You are too far away.", Toast.LENGTH_LONG).show();
                        } else {
                            closeDialog = true;
                            ChooseQuestDialog questDialog = new ChooseQuestDialog();
                            questDialog.setArray(currentGameObject.getBaseQuests());
                            questDialog.show(getFragmentManager(), "showQuestsDialog");
                        }
                    }
                })
                .setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        GameObjectDialog.this.getDialog().cancel();
                    }
                });
        return builder.create();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void setCurrentObject(GameObject gameObject) {
        this.currentGameObject = gameObject;
    }

    @Override
    public void notification(UINotification data) {

    }
}
