package diploma.geoframe.quests.riddleQuest;

import android.util.Log;
import diploma.geoframe.quests.BaseQuest;
import diploma.geoframe.quests.QuestType;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Lucie Kucharova
 * Repesentation of specific Riddle
 */

public class RiddleQuest extends BaseQuest {

    private String name;
    private String description;
    private String answer;
    private String hint;
    private boolean active;
    private RiddleResult result;

    public RiddleQuest(Long id, QuestType questType) {
        super(id, questType);
    }

    public RiddleQuest(JSONObject json) {
        super();
        super.setQuestType(QuestType.QUEST_TYPE_RIDDLE);
        fromJson(json);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public RiddleResult getResult() {
        return result;
    }

    public void setResult(RiddleResult result) {
        this.result = result;
    }

    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        try {
            json.put("id", super.getId());
            json.put("name", name);
            json.put("description", description);
            json.put("answer", answer);
            json.put("hint", hint);
            json.put("active", active);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    public JSONObject fromJson(JSONObject json) {
        try {
            if (!json.isNull("id")) {
                if (json.get("id") instanceof Long) {
                    super.setId(json.getLong("id"));
                }
                if (json.get("id") instanceof String) {
                    String id = json.getString("id");
                    super.setId(Long.parseLong(id.substring(id.lastIndexOf("_") + 1)));
                }
            }
            if (!json.isNull(("name"))) {
                this.name = json.getString(("name"));
            }
            if (!json.isNull("description")) {
                this.description = json.getString("description");
            }
            if (!json.isNull("answer")) {
                this.answer = json.getString("answer");
            }
            if (!json.isNull("hint")) {
                this.hint = json.getString("hint");
            }
            if (!json.isNull("active")) {
                this.active = json.getBoolean("active");
            }
        } catch (Exception e) {
            Log.e(this.getClass().toString(), e.getMessage());
        }
        return json;
    }
}