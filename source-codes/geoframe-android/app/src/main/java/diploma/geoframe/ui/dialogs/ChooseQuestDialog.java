package diploma.geoframe.ui.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import diploma.geoframe.GlobalState;
import diploma.geoframe.R;
import diploma.geoframe.quests.Quest;
import diploma.geoframe.quests.QuestUIHandler;
import diploma.geoframe.ui.helpers.QuestSelectionAdapter;

import java.util.List;

/**
 * @author Lucie Kucharova
 * UI dialog shown to player to offer all
 * possible quests for solving in certain
 * context
 */

public class ChooseQuestDialog extends DialogFragment {

    private List<Quest> array;
    GlobalState globalState;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        globalState = (GlobalState) getActivity().getApplicationContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        QuestSelectionAdapter arrayAdapter = new QuestSelectionAdapter(getContext(),
                R.layout.my_spinner_item, array,
                globalState.getCurrentPlayer().getCompletedQuestIds());
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        builder.setTitle(R.string.choose_quest)
                .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.i(this.getClass().toString(), "SHOW QUESTS");
                        QuestUIHandler manager = new QuestUIHandler((GlobalState) getActivity().getApplicationContext(),
                                getFragmentManager());
                        manager.runQuest(array.get(which));
                    }
                });
        return builder.create();
    }

    public void setArray(List<Quest> array) {
        this.array = array;
    }
}
