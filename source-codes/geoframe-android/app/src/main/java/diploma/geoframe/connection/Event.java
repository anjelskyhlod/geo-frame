package diploma.geoframe.connection;

import org.json.JSONObject;

/**
 * @author Lucie Kucharova
 * Representation of system/client event
 */

public class Event {
    private String userId;
    private EventType type;
    private JSONObject value;
    private String toTopic;

    public Event(String userId, EventType type, JSONObject value) {
        this.userId = userId;
        this.type = type;
        if (value == null) this.value = new JSONObject();
        else this.value = value;
        this.toTopic = null;
    }

    public Event(String userId, EventType type, JSONObject value, String toTopic) {
        this.userId = userId;
        this.type = type;
        if (value == null) this.value = new JSONObject();
        else this.value = value;
        this.toTopic = toTopic;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
    }

    public JSONObject getValue() {
        return value;
    }

    public void setValue(JSONObject value) {
        this.value = value;
    }

    public String getToTopic() {
        return toTopic;
    }

    public void setToTopic(String toTopic) {
        this.toTopic = toTopic;
    }
}
