package diploma.geoframe.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import diploma.geoframe.GlobalState;
import diploma.geoframe.R;
import diploma.geoframe.controller.*;
import diploma.geoframe.ui.dialogs.ChooseQuestDialog;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Lucie Kucharova
 * Activity handling login action
 */

public class LoginActivity extends Activity implements UINotificationListener {
    GlobalState globalState;

    private EditText usernameInput;
    private EditText passwordInput;
    private EditText ipAdressInput;
    private Button loginButton;
    private AlertDialog badLoginDialog;

    private LoginController loginController;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(getClass().toString(), "LoginActivity started");
        globalState = ((GlobalState) getApplicationContext());
        loginController = globalState.getLoginController();
        loginController.addListener(this);
        setContentView(R.layout.login);
        loginButton = findViewById(R.id.logInButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loginController != null) {
                    usernameInput = v.getRootView().findViewById(R.id.usernameInput);
                    passwordInput = v.getRootView().findViewById(R.id.passwordInput);
                    ipAdressInput = v.getRootView().findViewById(R.id.ipAddressInput);
                    loginController.logIn(usernameInput.getText().toString(), passwordInput.getText().toString(),
                            ipAdressInput.getText().toString());
                } else {
                    Log.e("Login", "Controller not set!");
                }
            }
        });
    }

    private void showLoginResult(boolean success) {
        if (!success) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(LoginActivity.this, "Login failed", 3000).show();
                }
            });
        }
    }

    @Override
    public void notification(UINotification notification) {
        UINotificationType notificationType = notification.getType();
        switch (notificationType) {
            case LOGIN_FAIL:
                Log.e("Login", "Login failed");
                showLoginResult(false);
                break;
            case LOGIN_SUCCESS:
                Log.e("Login", "Login succeded!");
                StorageController ctrl = globalState.getStorageController();
                ctrl.storeAuthToken((JSONObject) notification.getValue());
                redirectToWelcome();
                break;
            default:
                Log.i("LoginActivity", "No message for me :( ");
        }
    }

    private void redirectToWelcome() {
        Intent intent = new Intent(this, WelcomeActivity.class);
        this.startActivity(intent);
    }
}
