package diploma.geoframe.controller;

/**
 * @author Lucie Kucharova
 * All possible UI notifications
 */

public enum UINotificationType {
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    ALL_GAMES_BASIC,
    OBJECTS_BY_GAME,
    CURRENT_GAME,
    SOME_QUESTS_LOADED,
    RESULT_IS_HERE,
    SET_CHARACTER,
    ADD_HIDDEN_OBJECT,
    UPDATE_EXISTING_OBJECT,
    DELETE_EXISTING_OBJECT
}
