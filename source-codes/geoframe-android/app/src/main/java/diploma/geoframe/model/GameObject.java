package diploma.geoframe.model;

import android.util.Log;
import diploma.geoframe.quests.BaseQuest;
import diploma.geoframe.quests.Quest;
import diploma.geoframe.quests.QuestType;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lucie Kucharova
 * Repesentation of Game Objects
 */

public class GameObject {
    Long id;
    String name;
    String description;
    Location location;
    List<Quest> quests;
    Double accessibleRadius;

    public GameObject(String name, String description, double latitude, double longitude, List<Quest> baseQuests,
                      Double accessibleRadius) {
        this.name = name;
        this.description = description;
        this.location = new Location(latitude, longitude);
        this.quests = baseQuests;
        this.accessibleRadius = accessibleRadius;
    }

    public GameObject(JSONObject json) {
        this.quests = new ArrayList<>();
        try {
            if (!json.isNull("id")) {
                this.id = Long.parseLong(json.getString("id"));
            }
            if (!json.isNull("name")) {
                this.name = json.getString("name");
            }
            if (!json.isNull("description")) {
                this.description = json.getString("description");
            }
            if (!json.isNull("location")) {
                this.location = new Location(json.getJSONObject("location"));
            }
            if (!json.isNull("quests")) {
                JSONArray arr = json.getJSONArray("quests");
                for (int i = 0; i < arr.length(); i++) {
                    String composedId = arr.getString(i);
                    QuestType questType = null;
                    Long id = null;
                    for (QuestType qType : QuestType.values()) {
                        if (composedId.startsWith(qType.toString())) {
                            questType = qType;
                            id = Long.parseLong(composedId.substring(composedId.lastIndexOf("_") + 1));
                            break;
                        }
                    }
                    BaseQuest baseQuest = new BaseQuest(id, questType);
                    quests.add(baseQuest);
                }
            }
            if (!json.isNull("accessibleRadius")) {
                this.accessibleRadius = json.getDouble("accessibleRadius");
            }
        } catch (Exception e) {
            Log.e(this.getClass().toString(), "Game object parsing from json failed");
        }
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Location getLocation() {
        return location;
    }

    public Long getId() {
        return id;
    }

    public List<Quest> getBaseQuests() {
        return quests;
    }

    public List<Quest> getQuests() {
        return quests;
    }

    public Double getAccessibleRadius() {
        return accessibleRadius;
    }
}
