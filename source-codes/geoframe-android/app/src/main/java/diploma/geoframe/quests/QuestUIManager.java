package diploma.geoframe.quests;

/**
 * @author Lucie Kucharova
 * Every quest manager handling quest type specific
 * UI has to implement this interface in order to
 * avoid unnecessary coupling througout application.
 */

public interface QuestUIManager {
}
