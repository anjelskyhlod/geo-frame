package diploma.geoframe.connection;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

public class EventBuilder {

    public EventBuilder() {

    }

    public Event createEvent(String message) {
        String key = null;
        Event event = null;
        JSONObject jsonValue = null;
        try {
            jsonValue = new JSONObject(message);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        EventType type = null;
        try {
            JSONObject msg = new JSONObject(jsonValue.getString("message"));
            type = createEventType(msg.getString("eventType"));
            key = jsonValue.getString("topic");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (type != null) {
            event = new Event(key, type, jsonValue);
        } else {
            Log.e(this.getClass().toString(), "CreateEvent failed");
        }
        return event;
    }

    private EventType createEventType(String value) {
        EventType eventType = null;
        for (EventType type : EventType.values()) {
            if (type.name().equals(value)) {
                eventType = type;
            }
        }
        if (eventType == null) {
            Log.e(this.getClass().toString(), "UNKNOWN EVENT TYPE");
        }
        return eventType;
    }
}
