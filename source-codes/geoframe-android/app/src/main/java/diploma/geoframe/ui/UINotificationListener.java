package diploma.geoframe.ui;

import diploma.geoframe.controller.UINotification;

/**
 * @author Lucie Kucharova
 * Every UI component which needs to be updated with
 * new data has to implement this interface and has
 * to be subscribed to certain
 * @{@link diploma.geoframe.controller.UINotificator}
 */

public interface UINotificationListener {
    /**
     * This method handles new events which are directed
     * towards UI. Resolves type of event and invokes
     * correct actions
     *
     * @param data
     */
    void notification(UINotification data);
}
