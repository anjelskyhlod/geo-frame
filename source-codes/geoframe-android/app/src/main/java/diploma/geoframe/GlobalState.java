package diploma.geoframe;

import android.app.Activity;
import android.app.Application;
import diploma.geoframe.connection.*;
import diploma.geoframe.connection.EventListener;
import diploma.geoframe.controller.GameController;
import diploma.geoframe.controller.LocationController;
import diploma.geoframe.controller.LoginController;
import diploma.geoframe.controller.StorageController;
import diploma.geoframe.model.Game;
import diploma.geoframe.model.Player;
import okhttp3.*;
import okio.ByteString;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.util.GeoPoint;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author Lucie Kucharova
 * This class provides main application context. Stores all
 * needed information about current game (or knows how to
 * get it). Separates game logic from UI. UI can reach
 * this class by getApplicationContext(...) method.
 */

public class GlobalState extends Application {
    private Client client;
    private EventHandler eventHandler;

    private LoginController loginController;
    private GameController gameController;
    private StorageController storageController;
    private LocationController locationController;

    private Game currentGame;
    private Player currentPlayer;

    public void setupStorage(Activity activity) {
        storageController = new StorageController(activity);
    }

    public void setupConnection() {
        eventHandler = new EventHandler();
        client = new Client();
        client.addListener(eventHandler);
        client.setStorageController(storageController);
    }

    public void setupControllers() {
        loginController = new LoginController(client);
        gameController = new GameController(client);
        locationController = new LocationController(client);

        eventHandler.setLoginController(loginController);
        eventHandler.setGameController(gameController);
    }

    public Client getClient() {
        return client;
    }

    public EventHandler getEventHandler() {
        return eventHandler;
    }

    public LoginController getLoginController() {
        return loginController;
    }

    public GameController getGameController() {
        return gameController;
    }

    public StorageController getStorageController() {
        return storageController;
    }

    public LocationController getLocationController() {
        return locationController;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Game getCurrentGame() {
        return currentGame;
    }

    public void setCurrentGame(Game currentGame) {
        this.currentGame = currentGame;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }
}
