package diploma.geoframe.controller;

import diploma.geoframe.connection.Event;
import diploma.geoframe.connection.EventProducer;
import diploma.geoframe.connection.EventType;
import diploma.geoframe.model.Game;
import diploma.geoframe.model.GameObject;
import diploma.geoframe.model.Player;
import diploma.geoframe.quests.QuestHandler;
import diploma.geoframe.quests.QuestType;
import diploma.geoframe.quests.Quest;
import diploma.geoframe.quests.QuestResult;
import diploma.geoframe.quests.riddleQuest.RiddleQuest;
import diploma.geoframe.ui.UINotificationListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.tileprovider.MapTile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Lucie Kucharova
 * This class handles game info logic.
 */

public class GameController implements UINotificator {

    private EventProducer eventProducer;

    private List<UINotificationListener> listeners;

    public GameController(EventProducer producer) {
        this.eventProducer = producer;
        this.listeners = new ArrayList<>();
    }

    @Override
    public void produceEvent(UINotification data) {
        for (UINotificationListener listener : listeners) {
            listener.notification(data);
        }
    }

    public void addListener(UINotificationListener activity) {
        this.listeners.add(activity);
    }

    /**
     * Emits event to system for obtaining all
     * games
     *
     * @param topic
     */
    public void requestGetAllGames(String topic) {
        eventProducer.produceEvent(new Event(topic, EventType.GET_ALL_GAMES_BASIC, null));
    }

    /**
     * Parses incoming data containing basic info
     * of all games and notifies UI
     *
     * @param body
     */
    public void setAllGamesBasic(JSONObject body) {
        List<Game> gamesBasic = parseAllGames(body);
        UINotification<List<Game>> notification = new UINotification();
        notification.setType(UINotificationType.ALL_GAMES_BASIC);
        notification.setValue(gamesBasic);
        produceEvent(notification);
    }

    /**
     * Emits event to system for obtaining chosen
     * game
     *
     * @param topic
     * @param gameId
     */
    public void requestCurrentGame(String topic, Long gameId) {
        JSONObject data = new JSONObject();
        try {
            data.put("gameId", gameId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        eventProducer.produceEvent(new Event(topic, EventType.GET_GAME_BY_ID, data));
    }

    /**
     * Parses incoming data containing info
     * about chosen game, notifies UI
     *
     * @param body
     */
    public void setCurrentGame(JSONObject body) {
        Game game = new Game(getJsonGame(body));
        UINotification<Game> notification = new UINotification();
        notification.setType(UINotificationType.CURRENT_GAME);
        notification.setValue(game);
        produceEvent(notification);
    }

    /**
     * Parses incoming game objects data and
     * notifies UI
     *
     * @param body
     */
    public void setAllObjects(JSONObject body) {
        List<GameObject> gameObjects = getGameObjects(body);
        UINotification<List<GameObject>> notification = new UINotification();
        notification.setType(UINotificationType.OBJECTS_BY_GAME);
        notification.setValue(gameObjects);
        produceEvent(notification);
    }

    /**
     * Emits event for all quests contained in
     * questsBasicInfo in order to get specific
     * info about them
     *
     * @param topic
     * @param questsBasicInfo
     */
    public void getQuestsById(String topic, List<Quest> questsBasicInfo) {
        for (Map.Entry<QuestType, List<Quest>> entry : getParsedQuestMap(questsBasicInfo).entrySet()) {
            JSONObject data = new JSONObject();
            JSONArray ids = new JSONArray();
            for (Quest q : entry.getValue()) {
                ids.put(q.getId());
            }
            try {
                data.put("questIds", ids);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            eventProducer.produceEvent(new Event(topic, EventType.GET_QUESTS_BY_IDS, data, entry.getKey().toString()));
        }
    }

    /**
     * Parses quests from given string and
     * notifies UI about new data obtained
     *
     * @param body
     */
    public void setRiddleOnCurrentObject(JSONObject body) {
        List<Quest> quests = parseQuests(body);
        UINotification<List<Quest>> notification = new UINotification();
        notification.setType(UINotificationType.SOME_QUESTS_LOADED);
        notification.setValue(quests);
        produceEvent(notification);
    }

    /**
     * Receives data about completition of
     * certain quest. Quest type is detemined
     * by @{@link QuestHandler} and UI
     * notified about result
     *
     * @param body
     */
    public void setResultOfQuest(JSONObject body) {
        QuestHandler handler = new QuestHandler();
        QuestResult result = handler.resolveCompletition(body);
        UINotification<QuestResult> notification = new UINotification();
        notification.setType(UINotificationType.RESULT_IS_HERE);
        notification.setValue(result);
        produceEvent(notification);
    }

    /**
     * Receives player object and notifies UI
     *
     * @param body
     */
    public void setPlayerObject(JSONObject body) {
        System.out.println("CHARACTER SET");
        UINotification<JSONObject> notification = new UINotification();
        notification.setType(UINotificationType.SET_CHARACTER);
        notification.setValue(body);
        produceEvent(notification);
    }

    /**
     * Receives new object which was uncovered
     * and notifies UI
     *
     * @param body
     */
    public void addHiddenObject(JSONObject body) {
        GameObject object = new GameObject(body);
        UINotification<GameObject> notification = new UINotification();
        notification.setType(UINotificationType.ADD_HIDDEN_OBJECT);
        notification.setValue(object);
        produceEvent(notification);
    }

    public void addNesObject(JSONObject body) {
        GameObject object = null;
        try {
            object = new GameObject(body.getJSONObject("gameObject"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        UINotification<GameObject> notification = new UINotification();
        notification.setType(UINotificationType.ADD_HIDDEN_OBJECT);
        notification.setValue(object);
        produceEvent(notification);
    }


    public void updateObject(JSONObject body) {
        GameObject object = null;
        try {
            object = new GameObject(body.getJSONObject("gameObject"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        UINotification<GameObject> notification = new UINotification();
        notification.setType(UINotificationType.UPDATE_EXISTING_OBJECT);
        notification.setValue(object);
        produceEvent(notification);
    }

    public void deleteObject(JSONObject body) {
        Long objectId = null;
        try {
            objectId = body.getLong("gameObjectId");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        UINotification<Long> notification = new UINotification();
        notification.setType(UINotificationType.DELETE_EXISTING_OBJECT);
        notification.setValue(objectId);
        produceEvent(notification);
    }


//    ############################
//    ###### Helper methods ######
//    ############################

    private List<Game> parseAllGames(JSONObject body) {
        List<Game> gamesBasic = new ArrayList<>();
        JSONArray gamesJson = null;
        try {
            gamesJson = body.getJSONArray("allGamesBasic");
            for (int i = 0; i < gamesJson.length(); i++) {
                JSONObject jGame = gamesJson.getJSONObject(i);
                String name = jGame.getString("name");
                String description = jGame.getString("description");
                Long id = jGame.getLong("id");
                Game game = new Game(name, description, null);
                game.setId(id);
                gamesBasic.add(game);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gamesBasic;
    }

    private JSONObject getJsonGame(JSONObject body) {
        JSONObject jGame = null;
        try {
            jGame = body.getJSONObject("game");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jGame;
    }

    private List<GameObject> getGameObjects(JSONObject body) {
        List<GameObject> gameObjects = new ArrayList<>();
        JSONArray objectsJson = null;
        try {
            objectsJson = body.getJSONArray("gameObjects");
            for (int i = 0; i < objectsJson.length(); i++) {
                JSONObject jGameObject = objectsJson.getJSONObject(i);

                boolean isPlayer = findPlayerObject(jGameObject);

                if (isPlayer) continue;
                GameObject gameObject = new GameObject(jGameObject);
                gameObjects.add(gameObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return gameObjects;
    }

    /**
     * Discovers if there is player object among
     * all given objects, if it is -> notify UI
     *
     * @param value
     * @return True if object is player object,
     * else false
     */
    private boolean findPlayerObject(JSONObject value) {
        if (value.isNull("username")) {
            return false;
        } else {
            Player player = new Player(value);
            UINotification<Player> notification = new UINotification();
            notification.setType(UINotificationType.SET_CHARACTER);
            notification.setValue(player);
            produceEvent(notification);
        }
        return true;
    }

    private Map<QuestType, List<Quest>> getParsedQuestMap(List<Quest> questsBasicInfo) {
        Map<QuestType, List<Quest>> map = new HashMap<>();
        for (Quest q : questsBasicInfo) {
            if (map.containsKey(q.getQuestType())) {
                map.get(q.getQuestType()).add(q);
            } else {
                List<Quest> list = new ArrayList<>();
                list.add(q);
                map.put(q.getQuestType(), list);
            }
        }
        return map;
    }

    private List<Quest> parseQuests(JSONObject body) {
        List<Quest> quests = new ArrayList<>();
        JSONArray questsJson = null;
        QuestHandler questHandler = new QuestHandler();
        try {
            questsJson = body.getJSONArray("quests");
            for (int i = 0; i < questsJson.length(); i++) {
                JSONObject jQuest = questsJson.getJSONObject(i);
                Quest gameObject = questHandler.createQuestObject(jQuest);
                quests.add(gameObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return quests;
    }
}
