package diploma.geoframe.quests.riddleQuest;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Button;
import android.widget.EditText;
import diploma.geoframe.GlobalState;
import diploma.geoframe.R;
import diploma.geoframe.quests.Quest;
import diploma.geoframe.quests.QuestDialog;
import diploma.geoframe.ui.helpers.QuestSelectionAdapter;

import java.util.List;

/**
 * @author Lucie Kucharova
 * UI Representation of @{@link RiddleQuest} object
 */

public class RiddleQuestDialog extends DialogFragment implements QuestDialog {

    private RiddleQuest currentQuest;

    private RiddleQuestUIManager riddleQuestUIManager;
    private RiddleQuestController riddleQuestController;

    private GlobalState globalState;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        globalState = (GlobalState) getActivity().getApplicationContext();
        riddleQuestController = new RiddleQuestController(globalState.getClient());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final EditText userinput = new EditText(getContext());

        userinput.setHint("Answer");
        builder.setTitle(currentQuest.getName())
                .setMessage(currentQuest.getDescription())
                .setView(userinput)
                .setNeutralButton(R.string.check_answer, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        riddleQuestController.checkAnswer(globalState.getClient().getTopic(), userinput.getText().toString(),
                                currentQuest.getId(), globalState.getCurrentGame().getId());
                    }
                });
        return builder.create();
    }

    public void setCurrentQuest(RiddleQuest quest) {
        this.currentQuest = quest;
    }

    public void setRiddleQuestController(RiddleQuestUIManager ctrl) {
        this.riddleQuestUIManager = ctrl;
    }
}
