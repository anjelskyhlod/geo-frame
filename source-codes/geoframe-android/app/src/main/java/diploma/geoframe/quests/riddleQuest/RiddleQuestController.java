package diploma.geoframe.quests.riddleQuest;

import diploma.geoframe.connection.Event;
import diploma.geoframe.connection.EventProducer;
import diploma.geoframe.connection.EventType;
import diploma.geoframe.quests.QuestType;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Lucie Kucharova
 * Handles logic of @{@link RiddleQuest}
 */

public class RiddleQuestController {

    private EventProducer eventProducer;

    public RiddleQuestController(EventProducer eventProducer) {
        this.eventProducer = eventProducer;
    }

    public RiddleQuestController() {
        this.eventProducer = null;
    }

    public void checkAnswer(String topic, String s, Long questId, Long gameId) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("answer", s);
            obj.put("questId", questId);
            obj.put("gameId", gameId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        eventProducer.produceEvent(new Event(topic, EventType.CHECK_SOLVED, obj,
                QuestType.QUEST_TYPE_RIDDLE.toString()));
    }

    public RiddleResult resolveCompletion(JSONObject data) {
        RiddleResult result = new RiddleResult(data);
        return result;
    }
}
