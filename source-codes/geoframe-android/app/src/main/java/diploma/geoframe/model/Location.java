package diploma.geoframe.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Lucie Kucharova
 * Repesentation of player's or object's location
 */

public class Location {

    private Double lattitude;
    private Double longitude;

    public Location(Double lattitude, Double longitude) {
        this.lattitude = lattitude;
        this.longitude = longitude;
    }

    public Location() {
    }

    public Location(JSONObject json) {
        if (!json.isNull("lattitude") && !json.isNull("longitude")) {
            try {
                this.lattitude = json.getDouble("lattitude");
                this.longitude = json.getDouble("longitude");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public Double getLattitude() {
        return lattitude;
    }

    public void setLattitude(Double lattitude) {
        this.lattitude = lattitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("lattitude", Double.toString(this.lattitude));
            obj.put("longitude", Double.toString(this.longitude));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return obj;
    }
}