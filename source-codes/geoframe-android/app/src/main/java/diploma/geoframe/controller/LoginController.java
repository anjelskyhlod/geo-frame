package diploma.geoframe.controller;

import android.util.Log;
import diploma.geoframe.connection.Event;
import diploma.geoframe.connection.EventProducer;
import diploma.geoframe.connection.EventType;
import diploma.geoframe.ui.UINotificationListener;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lucie Kucharova
 * This class handles player login.
 */

public class LoginController implements UINotificator {

    EventProducer producer;
    List<UINotificationListener> listeners;

    public LoginController(EventProducer producer) {
        this.producer = producer;
        listeners = new ArrayList<>();
    }

    @Override
    public void produceEvent(UINotification data) {
        for (UINotificationListener listener : listeners) {
            listener.notification(data);
        }
    }

    public void addListener(UINotificationListener listener) {
        this.listeners.add(listener);
    }

    /**
     * Emits event to system that new user
     * wants to log in
     *
     * @param username
     * @param password
     * @param ipAddress
     */
    public void logIn(String username, String password, String ipAddress) {
        if (producer == null) {
            Log.e("Login", "Cannot login, no connection! (UINotification producer missing)");
            return;
        }
        JSONObject data = new JSONObject();
        try {
            data.put("username", username);
            data.put("password", password);
            data.put("ipAddress", ipAddress);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        producer.produceEvent(new Event(username, EventType.GET_LOGIN, data));
    }

    /**
     * Determines if authentication failed or not
     * and notifies UI
     *
     * @param token
     */
    public void resolveAuthentication(String token) {
        UINotification notification = new UINotification();
        if (token.length() <= 0) {
            notification.setType(UINotificationType.LOGIN_FAIL);
        } else {
            notification.setType(UINotificationType.LOGIN_SUCCESS);
            JSONObject data = new JSONObject();
            try {
                data.put("access_token", token);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            notification.setValue(data);
        }
        produceEvent(notification);
    }
}
