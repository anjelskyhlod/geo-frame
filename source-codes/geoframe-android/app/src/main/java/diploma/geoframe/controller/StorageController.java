package diploma.geoframe.controller;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * @author Lucie Kucharova
 * This class handles application storage.
 */

public class StorageController {

    private Activity activity;

    public StorageController(Activity activity) {
        this.activity = activity;
    }

    public void storeAuthToken(JSONObject data) {
        String token = "";
        try {
            token = data.getString("access_token");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (token.length() > 0) {
            SharedPreferences prefs = activity.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("access_token", token);
            editor.commit();
            readPreferences();
            Log.w(this.getClass().toString(), "Token stored.");
        } else {
            Log.e(this.getClass().toString(), "No token to store!");
        }
    }

    public String getToken() {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        String token = sharedPref.getString("access_token", "no token here...");
        return token;
    }

    public void readPreferences() {
        SharedPreferences sharedPref = activity.getPreferences(Context.MODE_PRIVATE);
        String token = sharedPref.getString("access_token", "no token here...");
        Log.i(this.getClass().toString(), token);
    }
}
