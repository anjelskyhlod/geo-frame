package diploma.geoframe.quests;

/**
 * @author Lucie Kucharova
 * Every quest type UI dialog has to implement this interface.
 * Interface is used for interaction with rest of the
 * application in order to avoid unnecessary coupling.
 */


public interface QuestDialog {
}
