package diploma.geoframe.quests;

/**
 * @author Lucie Kucharova
 * Base class which has to implement
 * result from every quest type
 */

public interface QuestResult {
    QuestType getQuestType();

    boolean isCorrect();
}
