----------------------------------------------------
   ____		   _____
  / ___| ___  ___ |  ___| __ __ _ _ __ ___   ___ 
 | |  _ / _ \/ _ \| |_ | '__/ _` | '_ ` _ \ / _ \
 | |_| |  __/ (_) |  _|| | | (_| | | | | | |  __/
  \____|\___|\___/|_|  |_|  \__,_|_| |_| |_|\___|
                                                                                                                                      
----------------------------------------------------

Tato dimplomová práce se zabývá návrhem a zhotovením geolokačního rámce, který umožní vy-
tvářet různé verze her založených na určování polohy hráče. Práce analyzuje stávající možnosti
her na mobilních zařízeních, jejich funkcionalitu i problémy. Z toho čerpá ve vlastním návrhu
rámce, který problémům, v již existujících hrách, předchází a poskytuje webové rozhraní pro vy-
tváření her vlastních. Tento rámec pak poskytuje jednoduchý způsob, kterým je možné stávající
funkcionalitu rozšířit pro vznik nových typů herních akcí.

Klíčová slova: GPS, geolokace, mobilní aplikace, Android, datová spotřeba, synchronizace dat,
offline režim, SOA, mikroslužby, zprostředkovatel, událostmi řízená architektura, Open Street
Map, rozšiřitelnost, AngularJS, Kafka, WebSocket, JWT, Docker

----------------------------------------------------
This thesis deals with design and implementation of geolocation framework. This framework
enables creation of different game versions based on defining current position of player. This
work analyses current game posibilities on mobile devices, their functionalities and problems.
This information is used for designing geolocation framework with focus on elimination of these
problems, offering creation of own games via web editor. This framework also offers simple way
to extend existing functionalities and allows implementation of new game actions.

Key Words: GPS, geolocation, mobile device, Android, data usage, data synchronization,
offline mode, SOA, microservices, broker, event driven architecture, Open Street Map, extensi-
bility, AngularJS, Kafka, Websocker, JWT, Docker
